[Go back](./Readme.md)

### hex2raw - 20 points

As the required string doesn't change every time you execute the program, I just used `echo -e` and piped its output to `hex2raw` like so:

```
trupples@shell-web:~$ cd /problems/bee57af2b16368039c96edaa1bd95826
trupples@shell-web:/problems/bee57af2b16368039c96edaa1bd95826$ ./hex2raw
Give me this in raw form (0x41 -> 'A'):
6a5c6fa9602a2d0f439953bcb6f4a611

You gave me:
^C
trupples@shell-web:/problems/bee57af2b16368039c96edaa1bd95826$ echo -e '\x6a\x5c\x6f\xa9\x60\x2a\x2d\x0f\x43\x99\x53\xbc\xb6\xf4\xa6\x11' | ./hex2raw
Give me this in raw form (0x41 -> 'A'):
6a5c6fa9602a2d0f439953bcb6f4a611

You gave me:
6a5c6fa9602a2d0f439953bcb6f4a611
Yay! That's what I wanted! Here be the flag:
d2ee728e47348dffdd9b654d3733a40a
```
