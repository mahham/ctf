[Go back](./Readme.md)

### What Is Web - 20 points

The flag was split into 3 parts. The first one was found in a comment at the end of the html page:

```html
<!-- Cool! Look at me! This is an HTML file. It describes what each page contains in a format your browser can understand. -->
<!-- The first part of the flag (there are 3 parts) is 4eabea7c5b1 -->
<!-- What other types of files are there in a webpage? -->
```

The second one was in `hacker.css`:

```css
/*
This is the css file. It contains information on how to graphically display
the page. It is in a seperate file so that multiple pages can all use the same 
one. This allows them all to be updated by changing just this one.
The second part of the flag is e8e0c84cc61 
*/
```

And the third in `script.js`:

```js
/* This is a javascript file. It contains code that runs locally in your
 * browser, although it has spread to a large number of other uses.
 *
 * The final part of the flag is 2a79d2dc833
 */
```

Thus the flag is `4eabea7c5b1e8e0c84cc612a79d2dc833`
