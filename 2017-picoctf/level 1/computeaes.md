[Go back](./Readme.md)

### computeAES - 50 points

```
Encrypted with AES in ECB mode. All values base64 encoded
ciphertext = I300ryGVTXJVT803Sdt/KcOGlyPStZkeIHKapRjzwWf9+p7fIWkBnCWu/IWls+5S
key = iyq1bFDkirtGqiFz7OVi4A==
```

Given that data I wrote this python program to decrypt the message:

```py
from Crypto.Cipher import AES
from base64 import b64encode, b64decode

ciphertext = b64decode("I300ryGVTXJVT803Sdt/KcOGlyPStZkeIHKapRjzwWf9+p7fIWkBnCWu/IWls+5S")
key = b64decode("iyq1bFDkirtGqiFz7OVi4A==")

print(AES.new(key, AES.MODE_ECB, "this is ignored").decrypt(ciphertext))
```

The program outputs the flag: `flag{do_not_let_machines_win_2d4975bc}__________`

This needs the pycrypto package in order to work so run this if you don't have it:
```
python -m pip install pycrypto
```
