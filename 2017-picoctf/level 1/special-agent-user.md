[Go back](./Readme.md)

### Special Agent User - 50 points

This challenge requires figuring out the user's browser and browser version. This data is sent in the `User-Agent` HTTP request field. For a starter, I filtered all non HTTP request packets:

![Screenshot with a few packets](special-user-agent.png)

All but one of the packets were sent by the `wget` utility, the last one having this `User-Agent` string:

```
User-Agent: Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.93 Safari/537.36
```

The browser is Chrome verison `40.0.2214`. The Safari and AppleWebKit parts actually refer to the webkit version (I got stuck for a minute because of this)
