[Go back](./Readme.md)

### Digital Camouflage - 50 points

![Screenshot](digital-camouflage.png)

Looking through the given packet capture I found a form submission which contained the following fields:

```
	userid="hardawayn"
	passwd="UEFwZHNqUlRhZQ=="
```

Base 64 decoding the password I got `PApdsjRTae` which is the flag.
