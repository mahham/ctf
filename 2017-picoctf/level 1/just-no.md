[Go back](./Readme.md)

### Just No. - 40 points but unsolved

In order to trick out the executable I made a fake `auth` file inside `/home/trupples/problems/ec9da1496f80c8248197ba564097cebb` that contains anything but the string "no". When running `/problems/ec9da1496f80c8248197ba564097cebb/justno`, the `auth` file used is the spoofed one, but the flag is read from the real file:

```
trupples@shell-web:~$ mkdir -p problems/ec9da1496f80c8248197ba564097cebb
trupples@shell-web:~$ echo yeah > problems/ec9da1496f80c8248197ba564097cebb/auth
trupples@shell-web:~$ cd problems/ec9da1496f80c8248197ba564097cebb/
trupples@shell-web:~/problems/ec9da1496f80c8248197ba564097cebb$ /problems/ec9da1496f80c8248197ba564097cebb/justno
Oh. Well the auth file doesn't say no anymore so... Here's the flag: e4cec8fdf76a931b03ad7ef026103d43
```
