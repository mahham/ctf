[Go back](./Readme.md)

### keyz - 20 points

First I created an ssh key:
```
$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/trupples/.ssh/id_rsa): /home/trupples/somewhere_else
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/trupples/somewhere_else.
Your public key has been saved in /home/trupples/somewhere_else.pub.
The key fingerprint is:
SHA256:V32++X90CpIBMS3o5f0bjKCb/v0PwS/KK3Izwihhb/M trupples@DESKTOP-NILJA68
The key's randomart image is:
+---[RSA 2048]----+
|       .oo       |
|      . +..  .   |
|     . o +  . . .|
|      . o oo   o |
|       .S..*o   .|
|  o   .  .+ =o  =|
| . o o o   .o+.=.|
|  . = * =o ..o. o|
|   o +E=.+=o... =|
+----[SHA256]-----+
$ cat /home/trupples/somewhere_else.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCqOzBFhLLsVOSqEYmZNEVkBM2RWyBKPfa4wfPv1WUaPdystvAfXeyFjWroz8oyLbzcjr0NGEQHlaMCeG2zc9hO0N50why++DFNFy0XcJ142SurhlWjk3a/xserzbRzMnzgYU7UUjA48ytphhqe+cvwQxfK3ZvKhx2FJC0ObHZNtPGm78ajU2J6PbdSTlt8vniGBeEyTu71nJZO4LNLW0ArAM0czwXTcQ68MVHKwwDC2fGuahADLLYhEal45Kn1ig9tSe0asVXtHZF0UiDby1m3t8Wivq3/0dqpEgKnRysVPw6C5FDLRRf6sSUeZTJ8oqJgdkaSIuDQTT1xeuskfbrt trupples@DESKTOP-NILJA68
```

Then I copied it and added it to the remote `.ssh/authorized_keys` using the web interface. After this I was able to do `ssh trupples@shell2017.picoctf.com` and was greeted with the flag: `who_needs_pwords_anyways`
