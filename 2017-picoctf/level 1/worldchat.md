[Go back](./Readme.md)

### WorldChat - 30 points

As the task description suggests, the amount of messages is overwhelming for a normal human being. To counter that, I used `grep` to only show the messages containing "flag":

```
21:43:34 noihazflag: that guy from that movie gives me hope to make a rasberry pie
21:43:34 noihazflag: my parents have demanded my presence to help me spell 'raspberry' correctly
21:43:34 ihazflag: We want to see me to help me spell 'raspberry' correctly
21:43:34 ihazflag: my homegirlz have demanded my presence to generate fusion power
21:43:34 noihazflag: Only us give me hope for what, I do not know
21:43:34 personwithflag: Several heavily mustached dolphins have demanded my presence to create a self driving car
21:43:34 personwithflag: Several heavily mustached dolphins want to see me for what, I do not know
21:43:35 personwithflag: A silly panda wants to steal my sloth to create a self driving car
21:43:35 flagperson: this is part 1/8 of the flag - 748a
21:43:35 noihazflag: You totally understands me and my pet sloth to understand me
21:43:36 whatisflag: my parents have demanded my presence to drink your milkshake
21:43:37 noihazflag: My sworn enemy would like to meet you to make a rasberry pie
21:43:37 ihazflag: Only us give me hope for what, I do not know
```

Still a lot of spam, but now I know I can search for "of the flag" to remove the useless messages:

```
21:46:12 flagperson: this is part 1/8 of the flag - 748a
21:46:13 flagperson: this is part 2/8 of the flag - 3a37
21:46:16 flagperson: this is part 3/8 of the flag - ce62
21:46:20 flagperson: this is part 4/8 of the flag - e537
21:46:20 flagperson: this is part 5/8 of the flag - 4552
21:46:20 flagperson: this is part 6/8 of the flag - c31f
21:46:24 flagperson: this is part 7/8 of the flag - 5319
21:46:26 flagperson: this is part 8/8 of the flag - 30dc
```
