[Go back](./Readme.md)

### raw2hex - 20 points

I used hexdump to take the raw otput and convert it to hex:

```
trupples@shell-web:~$ cd /problems/7ed72aec10a93d978ec3542055975d36
trupples@shell-web:/problems/7ed72aec10a93d978ec3542055975d36$ ./raw2hex | hexdump -C
00000000  54 68 65 20 66 6c 61 67  20 69 73 3a 23 3a 33 8f  |The flag is:#:3.|
00000010  30 52 fe c7 5f 00 9f 24  85 ac 53 52              |0R.._..$..SR|
```

Manually copying that I got `233a338f3052fec75f009f2485ac5352`
