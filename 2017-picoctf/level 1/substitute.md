[Go back](./Readme.md)

### Substitute - 40 points

I just let an online substitution cipher solver figure out this one:

![flag is IFONLYMODERNCRYPTOWASLIKETHIS](substitute.png)
