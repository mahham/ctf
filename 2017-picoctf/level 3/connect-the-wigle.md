[Go back](./Readme.md)

### Connect the wigle - 140 points

The file given is a sqlite database

```
$ file wigle
wigle: SQLite 3.x database
```

Opening it in SQLite Studio shows 3 tables: `android_metadata`, `network` and `location`. The first one has no interesting data, but the other two have a bunch of latlong coordinates. I exported those two tables as 2 CSV files and used this python script to create an svg file with the coordinates of one of them:

```py
import csv

file = open("wigle-location.csv", "r")
points = []
reader = csv.reader(file)

header = next(reader)
lat_i = header.index("lat")
lon_i = header.index("lon")

min_lat, min_long, max_lat, max_long = 999999, 999999, -999999, -999999
for row in reader:
    lat = float(row[lat_i])
    lon = float(row[lon_i])
    points += [(lat, lon)]
    if lat > max_lat:
        max_lat = lat
    if lat < min_lat:
        min_lat = lat
    if lon > max_long:
        max_long = lon
    if lon < min_long:
        min_long = lon

print("<html><body>")
print("<svg width=\"10000\" height=\"1000\" xmlns=\"http://www.w3.org/2000/svg\">")

for p in points:
    lat = 400 - (p[0] - min_lat) * 400
    lon = 50 + (p[1] - min_long) * 400
    print("<rect fill=\"black\" x=\"{}\" y=\"{}\" width=\"3\" height=\"3\" />".format(lon, lat))

print("</svg>")
print("</body></html>")
```

```
$ python wigle.py > wigle.html
```

Opening `wigle.html` shows this:

![FLAG{F0UND_M3_5148BBEA}](connect-the-wigle.png)
