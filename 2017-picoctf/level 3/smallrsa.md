[Go back](./Readme.md)

### smallRSA - 120 points

This is solved using [Wiener's Attack](https://en.wikipedia.org/wiki/Wiener%27s_Attack). It works by generating a bunch of possible d values and checking whether each d value would correspond to valid p and q values.

```py
from binascii import unhexlify

e = 165528674684553774754161107952508373110624366523537426971950721796143115780129435315899759675151336726943047090419484833345443949104434072639959175019000332954933802344468968633829926100061874628202284567388558408274913523076548466524630414081156553457145524778651651092522168245814433643807177041677885126141
n = 380654536359671023755976891498668045392440824270475526144618987828344270045182740160077144588766610702530210398859909208327353118643014342338185873507801667054475298636689473117890228196755174002229463306397132008619636921625801645435089242900101841738546712222819150058222758938346094596787521134065656721069
c = 225175934161594228135276539151300635650154382949275610703955895454397972800078177010645120694514429238852436900491673897049738868395450588115023690042698618062429449915158642982464013827556037197674747411586501561443167188341892522829441271789447291650951307375998344078663824671416916330711386511022057733100

# Generates continued fraction representation of numerator/denominator
def continuedFrac(numerator, denominator):
	while denominator != 0:
		integer = numerator // denominator				# integer part
		yield integer
		numerator -= denominator * integer				# reduce to fractional part
		denominator, numerator = numerator, denominator # flip fraction

# Generates convergents as (numerator, denominator) tuples
def convergents(continued_fraction):
	# First 2 convergents hardcoded as I'm bad at maths
	a0 = next(continued_fraction)
	a1 = next(continued_fraction)

	yield (a0, 1)
	yield (a1 * a0 + 1, a1)

	num1 = a1 * a0 + 1	# previous numerator
	num2 = a0			# numerator before that one
	denom1 = a1			# same as above but for denominators
	denom2 = 1

	for quotient in continued_fraction:
		num = quotient * num1 + num2
		denom = quotient * denom1 + denom2
		yield (num, denom)
		num2, num1 = num1, num
		denom2, denom1 = denom1, denom

def binary_search_sqrt(n):
	lo, hi = 1, 2
	while hi * hi <= n:
		hi *= 2
		lo *= 2

	while lo <= hi:
		mid = (lo + hi) // 2
		mid2 = mid * mid
		if mid2 < n:
			lo = mid + 1
			closest = mid
		elif mid2 > n:
			hi = mid - 1
		else:
			return mid

	return closest

for (k, d) in convergents(continuedFrac(e, n)):
	if k == 0:
		print("k = 0")
		continue

	# Check totient is an integer - most convergents fail here
	if (e * d - 1) % k == 0:
		totient = (e * d - 1) // k

		# Solving this equation gives p and q:
		# x^2 - (n - totient + 1)x + n = 0
		# x_1 and x_2 will be p, q
		# since the values of p and q are not needed, I can just check if that equation has valid solutions (p, q are integers)		
		
		# x = (-b +- sqrt(b*b - 4*a*c)) / 2*a
		# where
		# a = 1
		b = -n + totient - 1
		# c = n

		delta = b * b - 4 * n
		if delta < 0:
			print("No solution (delta < 0)")
			continue

		deltasqrt = binary_search_sqrt(delta)
		if deltasqrt * deltasqrt != delta:
			print("Delta is not a perfect square")
			continue

		if (b + deltasqrt) % 2 != 0:
			print("p, q not integers")
			continue

		print("Success:")
		print("d = {}".format(d))
		m = pow(c, d, n)
		print(unhexlify(hex(m)[2:])) # use [2:-1] for python 2 
```

```
$ python3 smallrsa.py
k = 0
Delta is not a perfect square
Delta is not a perfect square
Success:
d = 3223594586826657550897063711914171740995144768727978698812620148084071525621
b'flag{Are_any_RSA_vals_good_65199450210}'
No solution (delta < 0)
```
