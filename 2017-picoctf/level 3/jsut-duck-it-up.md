[Go back](./Readme.md)

### JSut Duck It Up - 100 points

The given file contains some weird obfuscated javascript code that uses quirks in how javascript arrays and objects add and negate to give unexpected results, thus allowing any JS script to be transformed into a long string of brackets, addition and negation operators. Just copy-pasting the 38kb file to the browser's console and running it creates an alert box with the flag: "aw_yiss_ducking_breadcrumbs_a4e5266a"
