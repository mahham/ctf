[Go back](./Readme.md)

### HashChain - 90 points

Authentication is done by repeatedly md5 hashing a value and sending the server the hash before the one given. The seed is the md5 hash of the base 10 string representation of the user id. This python script solves the task:

```py
import md5

ID = 3829 # manually insert given id here
seed = md5.new(str(ID)).hexdigest()
print "seed = " + seed

hashc = seed
while True:
    nextHash = md5.new(hashc).hexdigest()
    if nextHash == "b50d38162cd149edb6b96fb9ce40612e": # same as above but with the provided hash
        print "proof = " + hashc
        break
    hashc = nextHash
```

```
$ nc shell2017.picoctf.com 38130

*******************************************
***            FlagKeeper 1.1           ***
*  now with HASHCHAIN AUTHENTICATION! XD  *
*******************************************

Would you like to register(r) or get flag(f)?

r/f?

f
This flag only for user 3829
Please authenticate as user 3829
b50d38162cd149edb6b96fb9ce40612e
Next token?
cc90bdc16035325d554fe35d80fee30e
Hello user 3829! Here's the flag: 739045aa5b814fba124d5899a2d7d78b
```
