[Go back](./Readme.md)

### Coffee - 115 points

The .class file is compiled java, so I used [this online decompiler](http://www.javadecompilers.com/) to get readable java code out of it:

```java
import java.util.Base64.Decoder;

public class problem {
	public problem() {}

	public static String get_flag() {
		String str1 = "Hint: Don't worry about the schematics";
		String str2 = "eux_Z]\\ayiqlog`s^hvnmwr[cpftbkjd";
		String str3 = "Zf91XhR7fa=ZVH2H=QlbvdHJx5omN2xc";
		byte[] arrayOfByte1 = str2.getBytes();
		byte[] arrayOfByte2 = str3.getBytes();
		byte[] arrayOfByte3 = new byte[arrayOfByte2.length];
		for (int i = 0; i < arrayOfByte2.length; i++) {
			arrayOfByte3[i] = arrayOfByte2[(arrayOfByte1[i] - 90)];
		}
		System.out.println(java.util.Arrays.toString(java.util.Base64.getDecoder().decode(arrayOfByte3)));
		return new String(java.util.Base64.getDecoder().decode(arrayOfByte3));
	}

	public static void main(String[] paramArrayOfString) {
		System.out.println("Nothing to see here");
	}
}
```

The code in `get_flag` creates a string using characters in `str3` addressed by the ASCII values minus 90 of the chars in `str2`. The following python code decodes the flag:

```py
from base64 import b64decode

str2 = "eux_Z]\\ayiqlog`s^hvnmwr[cpftbkjd"
str3 = "Zf91XhR7fa=ZVH2H=QlbvdHJx5omN2xc"
result = ""

for i in range(len(str3)):
    result += str3[ord(str2[i]) - 90]

print(b64decode(result))
```

This script prints the flag: `flag_{pretty_cool_huh}`
