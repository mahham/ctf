<<<<<<< HEAD
[Go back](./Readme.md)

### Little School Bus - 75 points

Using this script I extracted the least significant bit of each byte in the image:
```py
output = ""

with open("littleschoolbus.bmp", "rb") as image:
    while True:
        byte = image.read(1)
        if byte == None:    # EOF
            break
        if byte % 2 == 0:
            output += '0'
        else:
            output += '1'

open("bits.txt", "w").write(output)
```

Looking at the output file I searched for an ascii-looking sequence and found this near the beginning of the file:

```
01100110 f
01101100 l
01100001 a
01100111 g
01111011 {
01110010 r
01100101 e
01101101 m
01100101 e
01101101 m
01100010 b
01100101 e
01110010 r
01011111 _
01101011 k
01101001 i
01100100 d
01110011 s
01011111 _
01110000 p
01110010 r
01101111 o
01110100 t
01100101 e
01100011 c
01110100 t
01011111 _
01111001 y
01101111 o
01110101 u
01110010 r
01011111 _
01101000 h
01100101 e
01100001 a
01100100 d
01100101 e
01110010 r
01110011 s
01011111 _
01100011 c
00110001 1
01100001 a
00110011 3
01111101 }

flag{remember_kids_protect_your_headers_c1a3}
```
=======
[Go back](./Readme.md)

### Little School Bus - 75 points

Using this script I extracted the least significant bit of each byte in the image:
```py
output = ""

with open("littleschoolbus.bmp", "rb") as image:
    while True:
        byte = image.read(1)
        if byte == None:    # EOF
            break
        if byte % 2 == 0:
            output += '0'
        else:
            output += '1'

open("bits.txt", "w").write(output)
```

Looking at the output file I searched for an ascii-looking sequence and found this near the beginning of the file:

```
01100110 f
01101100 l
01100001 a
01100111 g
01111011 {
01110010 r
01100101 e
01101101 m
01100101 e
01101101 m
01100010 b
01100101 e
01110010 r
01011111 _
01101011 k
01101001 i
01100100 d
01110011 s
01011111 _
01110000 p
01110010 r
01101111 o
01110100 t
01100101 e
01100011 c
01110100 t
01011111 _
01111001 y
01101111 o
01110101 u
01110010 r
01011111 _
01101000 h
01100101 e
01100001 a
01100100 d
01100101 e
01110010 r
01110011 s
01011111 _
01100011 c
00110001 1
01100001 a
00110011 3
01111101 }

flag{remember_kids_protect_your_headers_c1a3}
```
>>>>>>> master
