<<<<<<< HEAD
[Go back](./Readme.md)

### TW_GR_E1_ART - 100 points

#### Looking through the server files

As indicated by the hint, the `package.json` is publicly visible and mentions `server/serv.js` to be the main file. That file contains this line: `require("./game")(app, io)`. Opening `server/game.js` and searching for `flag` reveals these lines:

```js
if (entity.items[action.item].effects[i].check == 64) {
    outcome.flag = process.env["PICO_CTF_FLAG"];
}
```

inside the function `executeItemUse` which is called when the player "uses" an item in-game. So in order for the flag to be printed the flag item must have some effect object which has a check value of 64. Fast forward through the overly frustrating labyrinth to the last level which has a lot of flag items lying around:

![Flag level screenshot](tw_gr_e1_art.png)

Using a random one gives an error message on the game log and deletes all other flags. After a lot of looking around I found an array with all the player's items in `state.player.items[]`. The first flag I picked up looked like this:

```js
{
    description: "Gives you the flag... maybe.",
    effects:
    [
        {
            check: 7,
            type: "revealFlag"
        },
        {
            type: "destroyItems"
        }
    ],
    ...
}
```

If the code that prints the flag doesn't trigger, the server will go to the next effect and destroy all items. A nice thing is that the check value isn't random and increments in a left-to-right, top-to-bottom fashion. The rightmost flag of the 13th (1-indexing) row is flag #64 and when used will print the flag in the logs. Just make sure you're using the right item as they all look the same by running something like `state.player.items[0].effects[0].check === 64`.

```
Toaster used the Flag!
A soft voice on the wind speaks to you: "The secret you are looking for is at_least_the_world_wasnt_destroyed_by_a_meteor_ebb38a54b1bbe2d3fafb0460af19ea14.
Use it wisely."
```
=======
[Go back](./Readme.md)

### TW_GR_E1_ART - 100 points

#### Looking through the server files

As indicated by the hint, the `package.json` is publicly visible and mentions `server/serv.js` to be the main file. That file contains this line: `require("./game")(app, io)`. Opening `server/game.js` and searching for `flag` reveals these lines:

```js
if (entity.items[action.item].effects[i].check == 64) {
    outcome.flag = process.env["PICO_CTF_FLAG"];
}
```

inside the function `executeItemUse` which is called when the player "uses" an item in-game. So in order for the flag to be printed the flag item must have some effect object which has a check value of 64. Fast forward through the overly frustrating labyrinth to the last level which has a lot of flag items lying around:

![Flag level screenshot](tw_gr_e1_art.png)

Using a random one gives an error message on the game log and deletes all other flags. After a lot of looking around I found an array with all the player's items in `state.player.items[]`. The first flag I picked up looked like this:

```js
{
    description: "Gives you the flag... maybe.",
    effects:
    [
        {
            check: 7,
            type: "revealFlag"
        },
        {
            type: "destroyItems"
        }
    ],
    ...
}
```

If the code that prints the flag doesn't trigger, the server will go to the next effect and destroy all items. A nice thing is that the check value isn't random and increments in a left-to-right, top-to-bottom fashion. The rightmost flag of the 13th (1-indexing) row is flag #64 and when used will print the flag in the logs. Just make sure you're using the right item as they all look the same by running something like `state.player.items[0].effects[0].check === 64`.

```
Toaster used the Flag!
A soft voice on the wind speaks to you: "The secret you are looking for is at_least_the_world_wasnt_destroyed_by_a_meteor_ebb38a54b1bbe2d3fafb0460af19ea14.
Use it wisely."
```
>>>>>>> master
