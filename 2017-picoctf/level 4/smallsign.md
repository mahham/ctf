[Go back](./Readme.md)

### SmallSign - 140 points

As the challenge hint says, RSA encrypting and decrypting is multiplicative. This way, if $`\text{sig}(a)`$ and $`\text{sig}(b)`$ are known:

$`sig(a*b) = sig(a) * sig(b) \text{ mod } n`$

The signature of the challenge can be computed by multiplying the signatures of the primes it factors into. My solution first requests the signatures of the primes up to 1000 and then tries to factor the challenge. If succesful it prints out the flag:

```py
from pwn import *

# Sieve primes
# The 60 second limit should allow for a lot more primes,
# but I got random socket disconnects (on THEIR network
# using THEIR instance) and the bigger this limit the more
# probable it was that the script would fail. First time I
# tried the challenge I used 8000 but then the network
# was stable. 1000 seems to work fine (less than 30 tries)
MAXPRIME = 1000

Prime = 1
NotPrime = -1

primes = []
signatures = []

sieve = [None] * MAXPRIME
sieve[0] = sieve[1] = NotPrime
i = 2
while i * i <= MAXPRIME:
	primes.append(i)

	# Mark all of its multiples as NotPrime
	j = 2
	while i * j < MAXPRIME:
		sieve[i * j] = NotPrime
		j += 1

	# Go to the next non-NotPrime
	i += 1
	while sieve[i] == NotPrime:
		i += 1

# Only primes up to sqrt(MAXPRIME) are currently in the list. Add the rest
while i < MAXPRIME:
	primes.append(i)
	i += 1
	while (i < MAXPRIME) and (sieve[i] == NotPrime):
		i += 1

print("Done sieving! Found {} primes up to {}".format(len(primes), MAXPRIME))

# Connect to the task
nc = remote("shell2017.picoctf.com", 26523)

# Get n
nc.recvuntil("N: ")
n = int(nc.recvuntil("\n"))
print("n = {}".format(n))

# Send primes in chunks to minimise ineffifiency due to
# network latency. This doesn't really matter for small
# MAXPRIME values.
i = 0
CHUNK_SIZE = 50
while i < len(primes):
	chunk = primes[i:i+CHUNK_SIZE]
	long_request = "\n".join(map(str, chunk))
	nc.sendline(long_request)
	print("Sent {} prime signature requests. [{} ... {}]. Total request length: {}".format(len(chunk), chunk[0], chunk[-1], len(long_request)))

	# Receive the signatures
	try:
		for p in chunk:
			nc.recvuntil("Signature: ")
			resp = nc.recvuntil("\n")
			sig = int(resp)
			signatures += [(p, sig)]
	except EOFError:
		print("EOF error. Their network screwed up again")
	print("{} signatures".format(len(signatures)))

	i += CHUNK_SIZE

# Request challenge
nc.sendline("-1")

# Receive challenge
nc.recvuntil("Challenge: ")
chal = int(nc.recvuntil("\n"))

# Factor challenge and compute proof
proof = 1
for prime, sig in signatures:
	while chal % prime == 0:
		print("{} | {}".format(chal, prime))
		chal //= prime
		proof = (proof * sig) % n
	if chal < prime:
		break

# Many attempts will give challenges that factor into primes that are larger than MAXPRIME
if chal != 1:
	print("Challenge does not factor into small enough primes. Try again :/")
	exit(1)

# Send signature for challenge and receive flag
nc.sendline(str(proof))
print(nc.recvall())
```

Executing it I got a lot of numbers that don't factor nicely, but it eventually got to a challenge that does:

```
$ python2 smallsign.py
Done sieving! Found 168 primes up to 1000
[+] Opening connection to shell2017.picoctf.com on port 26523: Done
n = 23391951085135374062680325487455073992552443902866078273044326456240959088698454013263933371430562226448663357057549873507192818875100281721847867942563151428630962397446039044676133826724435985124567211593017120708710421293017263296050948658888208672239554808344863223498466471227396868655915788277828893244872439083406053981857954511007282782826274653998418135849087828636641125010318688752596015138638739074640096681568195501488495318994986897408758708352534614056495942360237376657355597018224813974484815976156450183118488877678317896256186114679584775023187724935099321501019743451316888953874041461898540606081
Sent 50 prime signature requests. [2 ... 229]. Total request length: 170
50 signatures
Sent 50 prime signature requests. [233 ... 541]. Total request length: 199
100 signatures
Sent 50 prime signature requests. [547 ... 863]. Total request length: 199
150 signatures
Sent 18 prime signature requests. [877 ... 997]. Total request length: 71
168 signatures
29420128 | 2
14710064 | 2
7355032 | 2
3677516 | 2
1838758 | 2
919379 | 23
39973 | 71
563 | 563
[+] Receiving all data: Done (100B)
[*] Closed connection to shell2017.picoctf.com port 26523
Enter the signature of the challenge: Congrats! Here is the flag: 6389ae962ef371ccd31b2da7d4e822d7
```
