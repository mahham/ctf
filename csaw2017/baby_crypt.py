import socket

nc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
nc.connect(("crypto.chal.csaw.io", 1578))
nc.recv(1024)	# Enter your username ...

def request(x):
	nc.send(x + "\n")
	resp = nc.recv(4096).split()
	cookie = resp[resp.index("is:") + 1]
	return cookie.decode("hex")

bs = 16
flagsize = 47
blocks = (flagsize + 7) // bs
a_len = blocks * bs
interval_l = a_len - bs
interval_r = a_len

printable = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#$%&\'()*+,-./:;?@[\\]^_`{|}~ \t\n\r\x0b\x0c"
flag = ""

while not flag.endswith("}"):
	should_be = request("a" * (a_len - len(flag) - 1))
	for c in printable:
		f = flag + c
		res = request("a" * (a_len - len(f)) + f)
		if res[interval_l:interval_r] == should_be[interval_l:interval_r]:
			flag += c
			break
	print flag
