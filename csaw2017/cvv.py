import random, re
random.seed()

from pwn import *

def Luhn(nstring):
	rev = nstring[::-1]
	s = 0
	for i in range(0, len(rev)):
		x = ord(rev[i]) - ord('0')
		if i % 2 == 0:
			x *= 2
		if x > 9:
			x -= 9
		s += x
	return nstring + chr(s * 9 % 10 + ord('0'))

def checkLuhn(nstring):
	rev = nstring[::-1]
	s = 0
	for i in range(0, len(rev)):
		x = ord(rev[i]) - ord('0')
		if i % 2 == 1:
			x *= 2
		if x > 9:
			x -= 9
		s += x
	return s % 10 == 0

def randnum(n):
	digits = "0123456789"
	r = ""
	for i in range(n):
		r += digits[random.randrange(10)]
	return r

def creditcard(start, length):
	return Luhn(start + randnum(length - len(start) - 1))

nc = remote("misc.chal.csaw.io", 8308)

print("Part 1 - I need a new MasterCard/Discover/American Express/Visa")
for _ in range(25):
	ineed = nc.recvuntil("I need ")
	query = nc.recvuntil("!")[:-1]
	n = "Oops"

	if query == "a new MasterCard":
		n = creditcard("51", 16)
	if query == "a new Discover":
		n = creditcard("6011", 16)
	if query == "a new American Express":
		n = creditcard("34", 15)
	if query == "a new Visa":
		n = creditcard("4", 16)

	nc.sendline(n)


print("Part 2 - I need a new card that starts with...")
for _ in range(25):
	ineed = nc.recvuntil("I need ")
	query = nc.recvuntil("!")[:-1]
	n = "Oops"

	if "a new card that starts with" in query:
		start = re.search(r"(\d+)$", query).group(1)
		n = creditcard(start, 16)

	nc.sendline(n)

print("Part 3 - I need a new card which ends with...")
for _ in range(50):
	ineed = nc.recvuntil("I need ")
	query = nc.recvuntil("!")[:-1]
	n = "Oops"

	if "a new card which ends with" in query:
		end = re.search(r"(\d+)$", query).group(1)
		while not n.endswith(end):
			n = creditcard("4", 16)

	nc.sendline(n)

print("Part 4 - I need to know if ... is valid")
for _ in range(25):
	ineed = nc.recvuntil("I need ")
	query = nc.recvuntil("!")[:-1]
	n = "Oops"

	if "is valid" in query:
		number = re.search(r"\s(\d+)\s", query).group(1)
		if checkLuhn(number) and len(number) == 16:
			n = "1"
		else:
			n = "0"

	nc.sendline(n)

print(nc.recvall())
