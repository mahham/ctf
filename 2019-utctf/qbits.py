import random
import math
import sys

from z3 import *


class QBit:
	def __init__(self, s0=random.random() + random.random()*1j, s1=random.random() + random.random()*1j):
		self.s0 = s0
		self.s1 = s1

	def transform(self, mat):
		self.s0 = mat[0][0] * self.s0 + mat[1][0] * self.s1
		self.s1 = mat[0][1] * self.s0 + mat[1][1] * self.s1
		return self

	def collapse(self):
		p0 = abs(self.s0)**2
		if random.random() < p0:
			self.s0 = 1
			self.s1 = 0
		else:
			self.s0 = 0
			self.s1 = 1
		return self

	def __repr__(self):
		return "{} |0> + {} |1>".format(self.s0, self.s1)

def rotate(theta):
	return [
		[math.cos(theta), -math.sin(theta)],
		[math.sin(theta), math.cos(theta)]
	]

sq2 = 1.4142
hadamard = [[1/sq2, 1/sq2], [1/sq2, -1/sq2]]

#(-0.61980665532+0.304981553316j) |0> + (0.0211630820626+0.0964136484638j) |1>
b = QBit(-0.61980665532+0.304981553316j, 0.0211630820626+0.0964136484638j)

"""
1. maximize 0
2. repeat rotations



a b    s0
c d    s1


a*s0 + b*s1 maxim
c*s0 + d*s1 minim

c = s1 / s0
d = -1
a = 1

1 + bc = -1


b = -2*(s1/s0)

"""

"""
b.transform([[1,0],[0,0]])

print b

bomb = "y" in raw_input("bomb? (y/n)")

for i in range(50):
	b.transform(rotate(0.0314))
	if bomb:
		b.collapse()
	print b

b.collapse()
print b

print rotate(0.0314)
"""

from pwn import *

r = remote("quantumbomb.live", 1337)

"""
There are 35 suspected bombs; however we can only query in superposition. Can you tell us which are which?
This is bomb 1
Here's the qubit: (-0.828754351676794+0.40937763649913567j) |0> + (0.0906167078342443+0.11393434547803544j) |1>
"""

try:
	for i in range(35):
		print( r.recvuntil("Here's the qubit: ", timeout=10))
		s0a = r.recvuntil(")")[1:-1]
		s0 = complex(s0a)
		r.recvuntil("|0> + ")
		s1 = complex(r.recvuntil(")")[1:-1])

		b = QBit(s0, s1)
		print(abs(b.s0)**2 + abs(b.s1)**2)
		print (b)

		for i in range(2):
			tr = [[100j,100j],[-100j,-100j]]
			print b.transform(tr)
			r.recvuntil("decide\n")
			r.sendline("1")
			r.sendline("{},{},{},{}".format(tr[0][0], tr[0][1], tr[1][0], tr[1][1]))

		r.recvuntil("decide\n")
		r.sendline("2")
		r.recvuntil("Here's the qubit: ")
		l = r.recvline()
		print (l)
		if "[[1.]] |1>" in l:
			r.sendline("n")
		else:
			r.sendline("y")
except EOFError:
	r.interactive()

print r.recvall()
