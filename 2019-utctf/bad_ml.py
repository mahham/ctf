from keras.models import load_model
import numpy as np
import random
from PIL import Image

m = load_model("model.model")

img = np.zeros((32, 32, 3), np.int8)
for i in range(32):
	for j in range(32):
		for k in range(3):
			img[i][j][k] = random.random()*255

im = Image.fromarray(np.array(img), 'RGB')
im.show()

bestdeer = 1
rate = 10

it = 0
im = None
while True:
	# Pick a random pixel and channel and change it

	newimg = [
		[[img[j][i][0], img[j][i][1], img[j][i][2]] for i in range(32)] for j in range(32)
	]

	for _ in range(1000):
		i, j, c = random.randrange(0, 32), random.randrange(0, 32), random.randrange(0, 3)
		newimg[i][j][c] += random.uniform(-rate, +rate)

	labels = m.predict(np.array([newimg]))[0]
	deer = labels[2]
	if deer < bestdeer:
		bestdeer = deer
		img[i][j][c] = newimg[i][j][c]
	it+=1
	if it % 1000 == 0:
		# build image and show it
		im = Image.fromarray(np.array(img), 'RGB')
		im.show()
		im.save("best.png")
		print (bestdeer)
		print (labels)