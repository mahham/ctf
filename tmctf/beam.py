#!/usr/bin/env python

from sys import argv
from time import sleep
import socket

##############  COMMAND-LINE ARGUMENTS #################

# Check if there is a correct number of arguments

if len(argv)<2 or len(argv)>3:
    print "Usage: beam.py file [COUNT]"
    print ""
    print "If COUNT is specified, the file will be beamed COUNT times (for multiple devices)"
    print ""
    quit()

filename = str(argv[1])
file_content = open(filename, "r").read()

beam_count = 1
if len(argv)==3:
    beam_count = int(argv[2])

############# INITIALISATION STUFF ###############

PORT = 8343
HOST = ""

HTML = "<html><head><title>beam.py - ${FILENAME}</title></head><body><center><h3>Beaming <code>${FILENAME}</code> from <code>${SOURCE}</code></h3></center></body></html>"
HTML = HTML.replace("${FILENAME}", filename)
HTML = HTML.replace("${SOURCE}", socket.getfqdn())

HEADER=\
        "HTTP/1.1 200 OK\r\n"\
        "MIME-Version: 1.0\r\n"\
        "Content-Type: multipart/mixed; boundary=\"BOUNDS\"\r\n"\
        "\r\n"\
        "--BOUNDS\r\n"\
        "Content-Type: text/html\r\n"\
        "\r\n"\
        ""+HTML+"\r\n"\
        "--BOUNDS\r\n"\
        "Content-Type: application/octet-stream\r\n"\
        "Content-Disposition: attachement; filename=\""+filename+"\r\n"\
        "\r\n"\
        ""+str(file_content)+"\r\n"\
        "--BOUNDS--\r\n"

############## IP LOG STUFF ########################

# Find out my IP
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("google.com",80))
myIP = s.getsockname()[0]
s.close()

print "Beaming %s to %s:%d" % (filename, myIP, PORT)

##################### ACTUAL WORK ###################

# Make a server socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = (HOST, PORT)
sock.bind(server_address)

# Listen for incoming connections
sock.listen(1)

for beam_number in range(1, beam_count+1):    # Beam the file beam_count times
    print "Waiting for a connection... (%d/%d)" % (beam_number, beam_count)
    
    connection, client_address = sock.accept()  # Client connected
    print "%s:%s connected" % client_address
    
    connection.recv(8192)

    connection.sendall(HEADER)
    
    sleep(1)
    
    print "Closing connection"
    connection.close() # Close current connection

################# DONE #######################

sock.close()
