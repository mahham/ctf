from pwn import *

r = [0]* 37

def hmm(i, c):
  print "Bag {} la {}..{}".format(repr(c), i, i+4)
  for j in range(i, i+4):
    r[j] = c[j-i]


ct = [22, 10, 56, 15, 168, 162, 175, 169, 175, 214, 170, 171,215, 175, 215, 171, 195, 175, 220, 175, 219, 219, 223, 175, 216, 218, 219, 214, 216, 173, 173, 217, 220, 220,218, 218, 216, 170, 0xd8, 0xdd]


hmm(0, ct[4:8])
hmm(8, ct[20:24])
hmm(4, ct[16:16+4])
hmm(16, ct[8:8+4])
hmm(12, ct[24:24+4])
hmm(24, ct[28:28+4])
hmm(20, ct[32:32+4])
hmm(32, ct[12:12+4])
hmm(28, ct[36:36+4])
r[36] = 0xd7

for i in range(256):
  print ''.join(chr(x ^ i) for x in r)
