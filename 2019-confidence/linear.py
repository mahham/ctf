from keras.models import load_model
import numpy as np
from PIL import Image

m = load_model("model2.h5")

print (m.summary())

d1 = m.get_layer("dense_1")
d2 = m.get_layer("dense_2")
d3 = m.get_layer("dense_3")

d1w = d1.get_weights()[0]
d1wT = np.transpose(d1w)

d2w = d2.get_weights()[0]
d2wT = np.transpose(d2w)

d3w = d3.get_weights()[0]
d3wT = np.transpose(d3w)

mat = d3wT @ d2wT @ d1wT

# normalize
mat = (mat - mat.min()) / (mat.max() - mat.min())

print (mat)

# Get ok matrix and notok matrix
mat0, mat1 = mat


im0 = Image.fromarray(np.reshape((mat0) * 255, (11, 50)).astype(np.uint8), "L")
im0.show()
im1 = Image.fromarray(np.reshape((mat1) * 255, (11, 50)).astype(np.uint8), "L")
im1.show()
