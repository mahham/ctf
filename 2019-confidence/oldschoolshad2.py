matr_map = """        4 {4pp   
       p {k4{ E  
      p 44p{ p   
       4 p       
        S        
                 
                 
                 
                 
"""

alphabet = "abcdefghijklmnopqrstuvwxyz0123456789"
flag_alph = " p4{krule_ctf}"
dictionary = []

# 00 = scade linia, scade coloana ;
# 01 = scade linia, creste coloana;
# 10 = creste linia, scade coloana;
# 11 = creste linia, creste coloana


#"0f0000000000000000"

def print_flg(s):
    bin_str = bin(int(str("p4{" + s + "}").encode('hex'), 16))[2:]

    bin_str = bin_str.zfill(72)

    max_len = len(bin_str) // 2
    bucati_de_2 = [bin_str[j:j+2] for j in range(0, len(bin_str), 2)]

    for i in range(9):
        tmp = bucati_de_2[i*4]
        bucati_de_2[i*4] = bucati_de_2[i*4+3]
        bucati_de_2[i*4+3] = tmp
        tmp = bucati_de_2[i*4+1]
        bucati_de_2[i*4+1] = bucati_de_2[i*4+2]
        bucati_de_2[i*4+2] = tmp

    tup_arr = bucati_de_2

    #print bin_str
    #print bucati_de_2

    zero_list = [0] * 162 # 9 * 18
    lin = 4
    col = 8

    for tmp in tup_arr:
        #print tmp, lin, col
        if(tmp == '00'):
            if(lin > 0):
                lin -= 1

            if(col > 0):
                col -= 1
        elif(tmp == '01'):
            if(lin > 0):
                lin -= 1

            if(col < 16):
                col += 1
        elif(tmp == '10'):
            if(lin < 8):
                lin += 1

            if(col > 0):
                col -= 1
        elif(tmp == '11'):
            if(lin < 8):
                lin += 1

            if(col < 16):
                col += 1

        zero_list[lin * 18 + col] += 1
        if(zero_list[lin * 18 + col] >= len(flag_alph)):
            zero_list[lin * 18 + col] = len(flag_alph) - 1

    for i in range(len(zero_list)):
        zero_list[i] = flag_alph[zero_list[i]]

        if(i == 80):
            zero_list[80] = 'S'

    #print lin, col
    zero_list[lin * 18 + col] = 'E'

    new_str = ''
    for i in range(9):
        zero_list[i*18 + 17] = '\n'
        new_str += ''.join(zero_list[i * 18:(i + 1) * 18])

    if(new_str[:40] == matr_map[:40]):
        print("FOUND FLAG!")
        print(s)
        exit(1)

    #print len(new_str), len(matr_map)
    #print(str("p4{" + s + "}").encode('hex'))
    #print(new_str)

for i in alphabet:
    for j in alphabet:
        for k in alphabet:
            for l in alphabet:
                for m in alphabet:
                    print str(i+j+k+l+m)
                    print_flg(str(i+j+k+l+m))