from keras.models import load_model
import numpy as np
import random
from PIL import Image

m = load_model("model2.h5")

img = np.zeros((11, 50))
for i in range(11):
	for j in range(50):
		img[i][j] = 0.5

print(img)

im = Image.open("aia_buna.png")
im.show()

img = np.asarray(im) / 255.0

print (img)

bestdeer = 0
rate = 0.1

it = 0
im = None
while True:
	# Pick a random pixel and channel and change it

	newimg = [
		[img[i][j] for j in range(50)] for i in range(11)
	]

	for _ in range(10):
		i, j = random.randrange(0, 11), random.randrange(0, 50)
		newimg[i][j] += random.uniform(-rate, +rate)

	labels = m.predict(np.array([newimg]))[0]
	deer = labels[1]
	#print(deer)
	if deer > bestdeer:
		bestdeer = deer
		img = newimg
	it+=1
	if it % 1000 == 0:
		# build image and show it
		im = Image.fromarray(np.array(img*256, np.uint8), 'L')
		im.show()
		im.save("best.png")
		print (bestdeer)
		print (labels)