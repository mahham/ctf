#include <stdio.h>

unsigned int checksum(unsigned char *inp, unsigned int a2) {
	unsigned int v4;
	unsigned int j;
	j=0;
	v4 = 0xFFFFFFFF;
	printf("Before: v4 = %x\n", v4);
	while(j < a2) {
		v4 ^= inp[j];
		printf("inp[%d] = %d = '%c'\n", j, inp[j], inp[j]);
		printf("Mid: v4 = %x\n", v4);
		v4 = (v4 >> 1) ^ ((-(v4 & 1)) & 0xEDB88320);
		v4 = (v4 >> 1) ^ ((-(v4 & 1)) & 0xEDB88320);
		v4 = (v4 >> 1) ^ ((-(v4 & 1)) & 0xEDB88320);
		v4 = (v4 >> 1) ^ ((-(v4 & 1)) & 0xEDB88320);
		v4 = (v4 >> 1) ^ ((-(v4 & 1)) & 0xEDB88320);
		v4 = (v4 >> 1) ^ ((-(v4 & 1)) & 0xEDB88320);
		v4 = (v4 >> 1) ^ ((-(v4 & 1)) & 0xEDB88320);
		v4 = (v4 >> 1) ^ ((-(v4 & 1)) & 0xEDB88320);
		++j;
	}
	printf("After: v4 = %x\n", v4);
	printf("After: ~v4 = %x\n", ~v4);
	return ~v4;
}

int main() {
	unsigned char s[] = "ace";
	unsigned int l = 3;
	unsigned long long cs = checksum(s, l);
	printf("%llu %llx\n", cs, cs);


	unsigned int v4 = 0xFFFFFFFF;
	v4 = v4 ^ 97;
	for(int i=7; i>=0; i--) {
		v4 = (v4 >> 1) ^ ((-(v4 & 1)) & 0xEDB88320);
	}
	v4 = v4 ^ 99;
	for(int i=7; i>=0; i--) {
		v4 = (v4 >> 1) ^ ((-(v4 & 1)) & 0xEDB88320);
	}
	v4 = v4 ^ 101;
	for(int i=7; i>=0; i--) {
		v4 = (v4 >> 1) ^ ((-(v4 & 1)) & 0xEDB88320);
	}
	if(~v4 == (0xB6 | (0xD5<<8) | (0x5C<<16) | (0xC5<<24)))
		printf("<\n");
	return 0;
}