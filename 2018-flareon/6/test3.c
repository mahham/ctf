#include <stdio.h>
int main() {
	char data[2] = {0xAF,'&'};
	unsigned char xorkey[256], x=0, v8=0,v9=0;

	for(int i=0; i<256; i++) xorkey[i]=i;
	for(int i=0; i<256; i++) {
		x += xorkey[i] + "Tis but a scratch."[i%18];
		xorkey[i] ^= xorkey[x];
		xorkey[x] ^= xorkey[i];
		xorkey[i] ^= xorkey[x];
	}
	for(int i=0; i<2; i++) {
		v8+=xorkey[++v9];
		xorkey[v8] ^= xorkey[v9];
		xorkey[v9] ^= xorkey[v8];
		xorkey[v8] ^= xorkey[v9];
		x = xorkey[v8] + xorkey[v9];
		printf("%c", data[i] ^ xorkey[x]);
	}

	printf("\n");
	return 0;
}