from pwn import *
import subprocess
import time

p = process("./b")

def step():
	k = subprocess.check_output("python3 solve1.py ./b")
	print(k)
	p.sendline(k)
	print(p.recvuntil("Enter key: "))

for i in range(666):
	print(i)
	time.sleep(0.3)
	step()
