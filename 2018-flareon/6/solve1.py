def handlePlaintext(correct, input_offset, num_bytes, data):
	for i in range(num_bytes):
		if correct[input_offset+i] == 0:
			correct[input_offset+i] = data[i]
		else:
			assert(correct[input_offset+i] == data[i])


def handleXor2A(correct, input_offset, num_bytes, data):
	for i in range(num_bytes):
		if correct[input_offset+i] == 0:
			correct[input_offset+i] = data[i] ^ 0x2A
		else:
			assert(correct[input_offset+i] == data[i] ^ 0x2A)


def handleFaux13(correct, input_offset, num_bytes, data):
	for i in range(num_bytes):
		if correct[input_offset+i] == 0:
			correct[input_offset+i] = data[i] - 13
		else:
			assert(correct[input_offset+i] == data[i] - 13)


def handleB64(correct, input_offset, num_bytes, data):
	alphabet = b"*9_d\xc2\xa7F#SktG(MpBI%Rjb8@JiEDY-1$PgyT!Lvqf+chmQWO0eNZ4un3l7H&2wazKV"
	bits = ""
	for c in data[:num_bytes+num_bytes//3]:
		bits += bin(alphabet.find(c) + 64)[-6:]
	bits += "00000000"
	res = []
	for i in range(0, len(bits)-7, 8):
		res.append(int(bits[i:i+8], 2))
	for i in range(num_bytes):
		if correct[input_offset+i] == 0:
			correct[input_offset+i] = res[i]
		else:
			assert(correct[input_offset+i] == res[i])


def handleFibo(correct, input_offset, num_bytes, data):
	for j in range(num_bytes):
		for i in range(256):
			v6 = 0
			v5 = 1
			v4 = 0
			ci = i
			while ci != 0:
				v6 = (v5 + v4)
				v4 = v5
				v5 = v6
				ci -= 1

			if v6 & 0xffffffffffffffff == struct.unpack("L", data[j*8:j*8+8])[0]:
				if correct[input_offset+j] == 0:
					correct[input_offset+j] = i
				else:
					assert(correct[input_offset+j] == i)
				break


def handleLongXorThing(correct, input_offset, num_bytes, data):
	xorkey = [i for i in range(256)]
	x = 0
	for i in range(256):
		x = (x + xorkey[i] + (b"Tis but a scratch.")[i%18]) % 256;
		xorkey[i] ^= xorkey[x];
		xorkey[x] ^= xorkey[i];
		xorkey[i] ^= xorkey[x];

	v8 = 0
	v9 = 0
	for i in range(num_bytes):
		v9 = (v9 + 1) % 256
		v8 = (v8 + xorkey[v9]) % 256
		xorkey[v8] ^= xorkey[v9]
		xorkey[v9] ^= xorkey[v8]
		xorkey[v8] ^= xorkey[v9]
		x = (xorkey[v8] + xorkey[v9]) % 256
		c = data[i] ^ xorkey[x]
		if correct[input_offset+i] == 0:
			correct[input_offset+i] = c
		else:
			assert(correct[input_offset+i] == c)

def handleBitmagic(correct, input_offset, num_bytes, data):
	if num_bytes == 1:
		if correct[input_offset] == 0:
			correct[input_offset] = 46
		else:
			assert(correct[input_offset] == 46)
	elif num_bytes == 3:
		sol = [ord(c) for c in "ace"]
		for i in range(3):
			if correct[input_offset+i] == 0:
				correct[input_offset+i] = sol[i]
			else:
				assert(correct[input_offset+i] == sol[i])
	else:
		raise Exception('I only know 2 solutions for Bitmagic :/')

def handleEntry(correct, fsize, input_offset, num_bytes, data):
	{
		0x7c: handlePlaintext,
		0x84: handleXor2A,
		0x8F: handleFaux13,
		0xB3: handleBitmagic,
		0x147: handleFibo,
		0x2fe: handleLongXorThing,
		0x326: handleB64,
	}[fsize](correct, input_offset, num_bytes, data)

import r2pipe
import struct
import os
import sys
from pwn import process

assert(len(sys.argv) == 2)
folder = sys.argv[1]

r = r2pipe.open()

def getKeyFor(filename):
	r.cmd("o " + filename)
	r.cmd("s 0x605100");
	table = bytes(r.cmdj("pxj 288*33"))
	entries = []
	for i in range(33):
		entry = table[i*288:i*288+288]
		beg_ptr, fsize, input_offset, num_bytes, out_offset, key_ptr = struct.unpack("LIIIIL", entry[0:32])
		data = entry[32:]
		entries.append({
			"beg_ptr": beg_ptr,
			"fsize": fsize,
			"input_offset": input_offset,
			"num_bytes": num_bytes,
			"out_offset": out_offset,
			"key_ptr": key_ptr,
			"data": data,
		})
	entries.sort(key=lambda x: x["input_offset"])
	correct = [0]*69	# xddd
	for e in entries:
		handleEntry(correct, e["fsize"], e["input_offset"], e["num_bytes"], e["data"])
	return bytes(correct)

print("--- Creating folder {} ---".format(folder))
os.mkdir(folder)

print("--- Copying fresh magic to {} ---".format(folder))
os.system("cp untouched_magic {}/_magic".format(folder))

print("--- Calculating passwords ---")
magic_path = "{}/_magic".format(folder)
p = process(magic_path)
for i in range(667):
	key = getKeyFor(magic_path)
	print(1, key)
	print(process.recvuntil("Enter key: "))
	process.sendline(key)

p.interactive()
