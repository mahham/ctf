import sys

def sumcs(a):
	s = 0
	for c in a:
		s += ord(c)
	return s

with open(sys.argv[1], "r") as f:
	lines = f.read().split("\n")
	for l in lines:
		if sumcs(l.strip()) == 0x8b1:
			print(l)
