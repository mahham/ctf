#include <idc.idc>

static dexor(fcn, size) {
  auto i;
  for(i=0; i<size; i=i+4) {
    PatchByte(fcn+i, Byte(fcn+i) ^ 0xEB);
    PatchByte(fcn+i+1, Byte(fcn+i+1) ^ 0xDE);
    PatchByte(fcn+i+2, Byte(fcn+i+2) ^ 0xEE);
    PatchByte(fcn+i+3, Byte(fcn+i+3) ^ 0x0D);
  }
}
 