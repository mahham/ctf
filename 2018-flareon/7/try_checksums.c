#include<stdio.h>

int checksumStep(int checksum, int nextitem) {
	return nextitem + ((checksum << 16) | (checksum >> 16)) + 10;
}

int checksum(unsigned char *a, unsigned int len) {
	int checksum = 0;
	for(unsigned int i=0; i<len; i++) {
		checksum = checksumStep(checksum, a[i]);
		checksum = checksumStep(checksum, 0);
	}
	return checksum;
}

void test(char *s, int cs) {
	int len = 0;
	char *p=s;while(p[len])len++;
	/*if(checksum(s, len) == cs)*/printf("%s - %d %x\n", s, checksum(s, len), checksum(s, len));
}

int main() {
	int x[]={65142874, 2621872, 48365658};
	for(int i=0; i<1; i++) {
		test("WS2_32", x[i]);
		test("ws2_32",  x[i]);
		test("USER32",  x[i]);
		test("user32",  x[i]);
		test("WS2_32.dll",  x[i]);
		test("ws2_32.dll",  x[i]);
		test("USER32.dll",  x[i]);
		test("user32.dll",  x[i]);
		test("kernel32.dll",  x[i]);
		test("KERNEL32.dll",  x[i]);
		test("kernel32",  x[i]);
		test("KERNEL32",  x[i]);
		test("worldofworcraft",  x[i]);
		test("worldofworcraft.exe",  x[i]);
		test("WorldOfWorcraft",  x[i]);
		test("WorldOfWorcraft.exe",  x[i]);
		test("wow",  x[i]);
		test("wow.exe",  x[i]);
		test("WOW",  x[i]);
		test("WOW.exe",  x[i]);
		test("ntdll.dll",  x[i]);
		test("kernelbase.dll",  x[i]);
		test("msvcrt.dll",  x[i]);
	}
	return 0;
}
