At first I tried brute forcing the tile positions and decrypting the ciphertext
in `getKey()` but that proved to be really slow. After that I actually took a
look at the bloody code and found these snippets in `MainForm.cs` which will
help us get the flag:

```cs
private static uint VALLOC_NODE_LIMIT = 30;						
private static uint VALLOC_TYPE_HEADER_PAGE = 4294966400;		
private static uint VALLOC_TYPE_HEADER_POOL = 4294966657;		
private static uint VALLOC_TYPE_HEADER_RESERVED = 4294967026;	
private uint[] VALLOC_TYPES = new uint[3]						
{
  MainForm.VALLOC_TYPE_HEADER_PAGE,
  MainForm.VALLOC_TYPE_HEADER_POOL,
  MainForm.VALLOC_TYPE_HEADER_RESERVED
};
```

```cs
private void AllocateMemory(MineField mf)
{
  for (uint index1 = 0; index1 < MainForm.VALLOC_NODE_LIMIT; ++index1)
  {
    for (uint index2 = 0; index2 < MainForm.VALLOC_NODE_LIMIT; ++index2)
    {
      bool flag = true;
      if (((IEnumerable<uint>) this.VALLOC_TYPES).Contains<uint>(this.DeriveVallocType(index1 + 1U, index2 + 1U)))
        flag = false;
      mf.GarbageCollect[(int) index2, (int) index1] = flag;
    }
  }
}
```

```cs
private uint DeriveVallocType(uint r, uint c)
{
  return (uint) ~((int) r * (int) MainForm.VALLOC_NODE_LIMIT + (int) c);
}
```

The variable and function names are intentionally misleading and the thought
that these snippets might only have to do with memory management made me
initially ignore them.

The first snippet sets up the board size (30x30) and three constants which are
related to the winning positions.

The third snippet does some bit flip magic on a given coordinate pair.

The second snippet lays out the board and sets the winning positions based on
the 3 constants from earlier. Basically if you pass the current coordinates to
the function in the second snippet and get as a result one of the 3 constants
then this is a winning position.

Reversing the bit flip is very easy and we can extract the winning positions
with a script like this:

```py
# Out 3 constants
magic = [4294966400, 4294966657, 4294967026]

for x in magic:
	y = (~x) % (2**32) - 1
	print("Winning position at line {}, column {}".format(y//30-1, y%30))

# Outputs:
# Winning position at line 28, column 24
# Winning position at line 20, column 7
# Winning position at line 7, column 28
```

Knowing the winning positions we can start the game and click the corresponding
tiles to get the flag:
![Ch3aters_Alw4ys_W1n@flare-on.com](./solved.jpg)

