import sys

with open(sys.argv[1], "rb") as f:
	sys.stdout.write(f.read().decode("ascii", "backslashreplace").replace("\\x", "x").replace("\\u", "u"))
