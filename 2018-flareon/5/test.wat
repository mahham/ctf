(module
  (type $t0 (func (param i32 i32 i32 i32) (result i32)))
  (type $t1 (func (param i32)))
  (type $t2 (func))
  (type $t3 (func (param i32 i32 i32 i32 i32) (result i32)))
  (type $t4 (func (param i32 i32 i32) (result i32)))
  (import "env" "putc_js" (func $env.putc_js (type $t1)))
  (func $f1 (type $t2))
  (func $f2 (type $t0) (param $p0 i32) (param $p1 i32) (param $p2 i32) (param $p3 i32) (result i32)
    (local $l0 i32) (local $l1 i32) (local $l2 i32) (local $l3 i32) (local $l4 i32) (local $l5 i32) (local $l6 i32) (local $l7 i32) (local $l8 i32) (local $l9 i32) (local $l10 i32) (local $l11 i32) (local $l12 i32) (local $l13 i32) (local $l14 i32) (local $l15 i32) (local $l16 i32) (local $l17 i32) (local $l18 i32) (local $l19 i32) (local $l20 i32) (local $l21 i32) (local $l22 i32) (local $l23 i32) (local $l24 i32) (local $l25 i32) (local $l26 i32) (local $l27 i32) (local $l28 i32) (local $l29 i32) (local $l30 i32) (local $l31 i32)
    get_global $g0
    set_local $l0
    i32.const 32
    set_local $l1
    get_local $l0
    get_local $l1
    i32.sub
    set_local $l2
    i32.const 2
    set_local $l3
    get_local $l2
    get_local $p0
    i32.store offset=20
    get_local $l2
    get_local $p1
    i32.store offset=16
    get_local $l2
    get_local $p2
    i32.store offset=12
    get_local $l2
    get_local $p3
    i32.store offset=8
    get_local $l2
    i32.load offset=16
    set_local $l4
    get_local $l3
    set_local $l5
    get_local $l4
    set_local $l6
    get_local $l5
    get_local $l6
    i32.gt_u
    set_local $l7
    get_local $l7
    set_local $l8
    block $B0
      block $B1
        get_local $l8
        i32.eqz
        br_if $B1
        i32.const 105
        set_local $l9
        get_local $l2
        get_local $l9
        i32.store offset=24
        br $B0
      end
      i32.const 0
      set_local $l10
      get_local $l2
      i32.load offset=20
      set_local $l11
      get_local $l11
      i32.load8_u
      set_local $l12
      get_local $l2
      get_local $l12
      i32.store8 offset=31
      get_local $l2
      i32.load8_u offset=31
      set_local $l13
      i32.const 255
      set_local $l14
      get_local $l13
      get_local $l14
      i32.and
      set_local $l15
      i32.const 15
      set_local $l16
      get_local $l15
      get_local $l16
      i32.and
      set_local $l17
      i32.const 255
      set_local $l18
      get_local $l17
      get_local $l18
      i32.and
      set_local $l19
      get_local $l10
      set_local $l20
      get_local $l19
      set_local $l21
      get_local $l20
      get_local $l21
      i32.ne
      set_local $l22
      get_local $l22
      set_local $l23
      block $B2
        get_local $l23
        i32.eqz
        br_if $B2
        i32.const 112
        set_local $l24
        get_local $l2
        get_local $l24
        i32.store offset=24
        br $B0
      end
      i32.const 0
      set_local $l25
      i32.const 2
      set_local $l26
      get_local $l2
      i32.load offset=20
      set_local $l27
      get_local $l27
      i32.load8_u offset=1
      set_local $l28
      get_local $l2
      i32.load offset=12
      set_local $l29
      get_local $l29
      get_local $l28
      i32.store8
      get_local $l2
      i32.load offset=8
      set_local $l30
      get_local $l30
      get_local $l26
      i32.store
      get_local $l2
      get_local $l25
      i32.store offset=24
    end
    get_local $l2
    i32.load offset=24
    set_local $l31
    get_local $l31
    return)
  (func $f3 (type $t0) (param $p0 i32) (param $p1 i32) (param $p2 i32) (param $p3 i32) (result i32)
    (local $l0 i32) (local $l1 i32) (local $l2 i32) (local $l3 i32) (local $l4 i32) (local $l5 i32) (local $l6 i32) (local $l7 i32) (local $l8 i32) (local $l9 i32) (local $l10 i32) (local $l11 i32) (local $l12 i32) (local $l13 i32) (local $l14 i32) (local $l15 i32) (local $l16 i32) (local $l17 i32) (local $l18 i32) (local $l19 i32) (local $l20 i32) (local $l21 i32) (local $l22 i32) (local $l23 i32) (local $l24 i32) (local $l25 i32) (local $l26 i32) (local $l27 i32) (local $l28 i32) (local $l29 i32) (local $l30 i32) (local $l31 i32) (local $l32 i32) (local $l33 i32) (local $l34 i32) (local $l35 i32)
    get_global $g0
    set_local $l0
    i32.const 32
    set_local $l1
    get_local $l0
    get_local $l1
    i32.sub
    set_local $l2
    i32.const 2
    set_local $l3
    get_local $l2
    get_local $p0
    i32.store offset=20
    get_local $l2
    get_local $p1
    i32.store offset=16
    get_local $l2
    get_local $p2
    i32.store offset=12
    get_local $l2
    get_local $p3
    i32.store offset=8
    get_local $l2
    i32.load offset=16
    set_local $l4
    get_local $l3
    set_local $l5
    get_local $l4
    set_local $l6
    get_local $l5
    get_local $l6
    i32.gt_u
    set_local $l7
    get_local $l7
    set_local $l8
    block $B0
      block $B1
        get_local $l8
        i32.eqz
        br_if $B1
        i32.const 105
        set_local $l9
        get_local $l2
        get_local $l9
        i32.store offset=24
        br $B0
      end
      i32.const 1
      set_local $l10
      get_local $l2
      i32.load offset=20
      set_local $l11
      get_local $l11
      i32.load8_u
      set_local $l12
      get_local $l2
      get_local $l12
      i32.store8 offset=31
      get_local $l2
      i32.load8_u offset=31
      set_local $l13
      i32.const 255
      set_local $l14
      get_local $l13
      get_local $l14
      i32.and
      set_local $l15
      i32.const 15
      set_local $l16
      get_local $l15
      get_local $l16
      i32.and
      set_local $l17
      i32.const 255
      set_local $l18
      get_local $l17
      get_local $l18
      i32.and
      set_local $l19
      get_local $l10
      set_local $l20
      get_local $l19
      set_local $l21
      get_local $l20
      get_local $l21
      i32.ne
      set_local $l22
      get_local $l22
      set_local $l23
      block $B2
        get_local $l23
        i32.eqz
        br_if $B2
        i32.const 112
        set_local $l24
        get_local $l2
        get_local $l24
        i32.store offset=24
        br $B0
      end
      i32.const 0
      set_local $l25
      i32.const 2
      set_local $l26
      get_local $l2
      i32.load offset=20
      set_local $l27
      get_local $l27
      i32.load8_u offset=1
      set_local $l28
      i32.const 255
      set_local $l29
      get_local $l28
      get_local $l29
      i32.and
      set_local $l30
      i32.const -1
      set_local $l31
      get_local $l30
      get_local $l31
      i32.xor
      set_local $l32
      get_local $l2
      i32.load offset=12
      set_local $l33
      get_local $l33
      get_local $l32
      i32.store8
      get_local $l2
      i32.load offset=8
      set_local $l34
      get_local $l34
      get_local $l26
      i32.store
      get_local $l2
      get_local $l25
      i32.store offset=24
    end
    get_local $l2
    i32.load offset=24
    set_local $l35
    get_local $l35
    return)
  (func $f4 (type $t0) (param $p0 i32) (param $p1 i32) (param $p2 i32) (param $p3 i32) (result i32)
    (local $l0 i32) (local $l1 i32) (local $l2 i32) (local $l3 i32) (local $l4 i32) (local $l5 i32) (local $l6 i32) (local $l7 i32) (local $l8 i32) (local $l9 i32) (local $l10 i32) (local $l11 i32) (local $l12 i32) (local $l13 i32) (local $l14 i32) (local $l15 i32) (local $l16 i32) (local $l17 i32) (local $l18 i32) (local $l19 i32) (local $l20 i32) (local $l21 i32) (local $l22 i32) (local $l23 i32) (local $l24 i32) (local $l25 i32) (local $l26 i32) (local $l27 i32) (local $l28 i32) (local $l29 i32) (local $l30 i32) (local $l31 i32) (local $l32 i32) (local $l33 i32) (local $l34 i32) (local $l35 i32) (local $l36 i32) (local $l37 i32) (local $l38 i32)
    get_global $g0
    set_local $l0
    i32.const 32
    set_local $l1
    get_local $l0
    get_local $l1
    i32.sub
    set_local $l2
    i32.const 3
    set_local $l3
    get_local $l2
    get_local $p0
    i32.store offset=20
    get_local $l2
    get_local $p1
    i32.store offset=16
    get_local $l2
    get_local $p2
    i32.store offset=12
    get_local $l2
    get_local $p3
    i32.store offset=8
    get_local $l2
    i32.load offset=16
    set_local $l4
    get_local $l3
    set_local $l5
    get_local $l4
    set_local $l6
    get_local $l5
    get_local $l6
    i32.gt_u
    set_local $l7
    get_local $l7
    set_local $l8
    block $B0
      block $B1
        get_local $l8
        i32.eqz
        br_if $B1
        i32.const 105
        set_local $l9
        get_local $l2
        get_local $l9
        i32.store offset=24
        br $B0
      end
      i32.const 2
      set_local $l10
      get_local $l2
      i32.load offset=20
      set_local $l11
      get_local $l11
      i32.load8_u
      set_local $l12
      get_local $l2
      get_local $l12
      i32.store8 offset=31
      get_local $l2
      i32.load8_u offset=31
      set_local $l13
      i32.const 255
      set_local $l14
      get_local $l13
      get_local $l14
      i32.and
      set_local $l15
      i32.const 15
      set_local $l16
      get_local $l15
      get_local $l16
      i32.and
      set_local $l17
      i32.const 255
      set_local $l18
      get_local $l17
      get_local $l18
      i32.and
      set_local $l19
      get_local $l10
      set_local $l20
      get_local $l19
      set_local $l21
      get_local $l20
      get_local $l21
      i32.ne
      set_local $l22
      get_local $l22
      set_local $l23
      block $B2
        get_local $l23
        i32.eqz
        br_if $B2
        i32.const 112
        set_local $l24
        get_local $l2
        get_local $l24
        i32.store offset=24
        br $B0
      end
      i32.const 0
      set_local $l25
      i32.const 3
      set_local $l26
      get_local $l2
      i32.load offset=20
      set_local $l27
      get_local $l27
      i32.load8_u offset=1
      set_local $l28
      i32.const 255
      set_local $l29
      get_local $l28
      get_local $l29
      i32.and
      set_local $l30
      get_local $l2
      i32.load offset=20
      set_local $l31
      get_local $l31
      i32.load8_u offset=2
      set_local $l32
      i32.const 255
      set_local $l33
      get_local $l32
      get_local $l33
      i32.and
      set_local $l34
      get_local $l30
      get_local $l34
      i32.xor
      set_local $l35
      get_local $l2
      i32.load offset=12
      set_local $l36
      get_local $l36
      get_local $l35
      i32.store8
      get_local $l2
      i32.load offset=8
      set_local $l37
      get_local $l37
      get_local $l26
      i32.store
      get_local $l2
      get_local $l25
      i32.store offset=24
    end
    get_local $l2
    i32.load offset=24
    set_local $l38
    get_local $l38
    return)
  (func $f5 (type $t0) (param $p0 i32) (param $p1 i32) (param $p2 i32) (param $p3 i32) (result i32)
    (local $l0 i32) (local $l1 i32) (local $l2 i32) (local $l3 i32) (local $l4 i32) (local $l5 i32) (local $l6 i32) (local $l7 i32) (local $l8 i32) (local $l9 i32) (local $l10 i32) (local $l11 i32) (local $l12 i32) (local $l13 i32) (local $l14 i32) (local $l15 i32) (local $l16 i32) (local $l17 i32) (local $l18 i32) (local $l19 i32) (local $l20 i32) (local $l21 i32) (local $l22 i32) (local $l23 i32) (local $l24 i32) (local $l25 i32) (local $l26 i32) (local $l27 i32) (local $l28 i32) (local $l29 i32) (local $l30 i32) (local $l31 i32) (local $l32 i32) (local $l33 i32) (local $l34 i32) (local $l35 i32) (local $l36 i32) (local $l37 i32) (local $l38 i32)
    get_global $g0
    set_local $l0
    i32.const 32
    set_local $l1
    get_local $l0
    get_local $l1
    i32.sub
    set_local $l2
    i32.const 3
    set_local $l3
    get_local $l2
    get_local $p0
    i32.store offset=20
    get_local $l2
    get_local $p1
    i32.store offset=16
    get_local $l2
    get_local $p2
    i32.store offset=12
    get_local $l2
    get_local $p3
    i32.store offset=8
    get_local $l2
    i32.load offset=16
    set_local $l4
    get_local $l3
    set_local $l5
    get_local $l4
    set_local $l6
    get_local $l5
    get_local $l6
    i32.gt_u
    set_local $l7
    get_local $l7
    set_local $l8
    block $B0
      block $B1
        get_local $l8
        i32.eqz
        br_if $B1
        i32.const 105
        set_local $l9
        get_local $l2
        get_local $l9
        i32.store offset=24
        br $B0
      end
      i32.const 3
      set_local $l10
      get_local $l2
      i32.load offset=20
      set_local $l11
      get_local $l11
      i32.load8_u
      set_local $l12
      get_local $l2
      get_local $l12
      i32.store8 offset=31
      get_local $l2
      i32.load8_u offset=31
      set_local $l13
      i32.const 255
      set_local $l14
      get_local $l13
      get_local $l14
      i32.and
      set_local $l15
      i32.const 15
      set_local $l16
      get_local $l15
      get_local $l16
      i32.and
      set_local $l17
      i32.const 255
      set_local $l18
      get_local $l17
      get_local $l18
      i32.and
      set_local $l19
      get_local $l10
      set_local $l20
      get_local $l19
      set_local $l21
      get_local $l20
      get_local $l21
      i32.ne
      set_local $l22
      get_local $l22
      set_local $l23
      block $B2
        get_local $l23
        i32.eqz
        br_if $B2
        i32.const 112
        set_local $l24
        get_local $l2
        get_local $l24
        i32.store offset=24
        br $B0
      end
      i32.const 0
      set_local $l25
      i32.const 3
      set_local $l26
      get_local $l2
      i32.load offset=20
      set_local $l27
      get_local $l27
      i32.load8_u offset=1
      set_local $l28
      i32.const 255
      set_local $l29
      get_local $l28
      get_local $l29
      i32.and
      set_local $l30
      get_local $l2
      i32.load offset=20
      set_local $l31
      get_local $l31
      i32.load8_u offset=2
      set_local $l32
      i32.const 255
      set_local $l33
      get_local $l32
      get_local $l33
      i32.and
      set_local $l34
      get_local $l30
      get_local $l34
      i32.and
      set_local $l35
      get_local $l2
      i32.load offset=12
      set_local $l36
      get_local $l36
      get_local $l35
      i32.store8
      get_local $l2
      i32.load offset=8
      set_local $l37
      get_local $l37
      get_local $l26
      i32.store
      get_local $l2
      get_local $l25
      i32.store offset=24
    end
    get_local $l2
    i32.load offset=24
    set_local $l38
    get_local $l38
    return)
  (func $f6 (type $t0) (param $p0 i32) (param $p1 i32) (param $p2 i32) (param $p3 i32) (result i32)
    (local $l0 i32) (local $l1 i32) (local $l2 i32) (local $l3 i32) (local $l4 i32) (local $l5 i32) (local $l6 i32) (local $l7 i32) (local $l8 i32) (local $l9 i32) (local $l10 i32) (local $l11 i32) (local $l12 i32) (local $l13 i32) (local $l14 i32) (local $l15 i32) (local $l16 i32) (local $l17 i32) (local $l18 i32) (local $l19 i32) (local $l20 i32) (local $l21 i32) (local $l22 i32) (local $l23 i32) (local $l24 i32) (local $l25 i32) (local $l26 i32) (local $l27 i32) (local $l28 i32) (local $l29 i32) (local $l30 i32) (local $l31 i32) (local $l32 i32) (local $l33 i32) (local $l34 i32) (local $l35 i32) (local $l36 i32) (local $l37 i32) (local $l38 i32)
    get_global $g0
    set_local $l0
    i32.const 32
    set_local $l1
    get_local $l0
    get_local $l1
    i32.sub
    set_local $l2
    i32.const 3
    set_local $l3
    get_local $l2
    get_local $p0
    i32.store offset=20
    get_local $l2
    get_local $p1
    i32.store offset=16
    get_local $l2
    get_local $p2
    i32.store offset=12
    get_local $l2
    get_local $p3
    i32.store offset=8
    get_local $l2
    i32.load offset=16
    set_local $l4
    get_local $l3
    set_local $l5
    get_local $l4
    set_local $l6
    get_local $l5
    get_local $l6
    i32.gt_u
    set_local $l7
    get_local $l7
    set_local $l8
    block $B0
      block $B1
        get_local $l8
        i32.eqz
        br_if $B1
        i32.const 105
        set_local $l9
        get_local $l2
        get_local $l9
        i32.store offset=24
        br $B0
      end
      i32.const 4
      set_local $l10
      get_local $l2
      i32.load offset=20
      set_local $l11
      get_local $l11
      i32.load8_u
      set_local $l12
      get_local $l2
      get_local $l12
      i32.store8 offset=31
      get_local $l2
      i32.load8_u offset=31
      set_local $l13
      i32.const 255
      set_local $l14
      get_local $l13
      get_local $l14
      i32.and
      set_local $l15
      i32.const 15
      set_local $l16
      get_local $l15
      get_local $l16
      i32.and
      set_local $l17
      i32.const 255
      set_local $l18
      get_local $l17
      get_local $l18
      i32.and
      set_local $l19
      get_local $l10
      set_local $l20
      get_local $l19
      set_local $l21
      get_local $l20
      get_local $l21
      i32.ne
      set_local $l22
      get_local $l22
      set_local $l23
      block $B2
        get_local $l23
        i32.eqz
        br_if $B2
        i32.const 112
        set_local $l24
        get_local $l2
        get_local $l24
        i32.store offset=24
        br $B0
      end
      i32.const 0
      set_local $l25
      i32.const 3
      set_local $l26
      get_local $l2
      i32.load offset=20
      set_local $l27
      get_local $l27
      i32.load8_u offset=1
      set_local $l28
      i32.const 255
      set_local $l29
      get_local $l28
      get_local $l29
      i32.and
      set_local $l30
      get_local $l2
      i32.load offset=20
      set_local $l31
      get_local $l31
      i32.load8_u offset=2
      set_local $l32
      i32.const 255
      set_local $l33
      get_local $l32
      get_local $l33
      i32.and
      set_local $l34
      get_local $l30
      get_local $l34
      i32.or
      set_local $l35
      get_local $l2
      i32.load offset=12
      set_local $l36
      get_local $l36
      get_local $l35
      i32.store8
      get_local $l2
      i32.load offset=8
      set_local $l37
      get_local $l37
      get_local $l26
      i32.store
      get_local $l2
      get_local $l25
      i32.store offset=24
    end
    get_local $l2
    i32.load offset=24
    set_local $l38
    get_local $l38
    return)
  (func $f7 (type $t0) (param $p0 i32) (param $p1 i32) (param $p2 i32) (param $p3 i32) (result i32)
    (local $l0 i32) (local $l1 i32) (local $l2 i32) (local $l3 i32) (local $l4 i32) (local $l5 i32) (local $l6 i32) (local $l7 i32) (local $l8 i32) (local $l9 i32) (local $l10 i32) (local $l11 i32) (local $l12 i32) (local $l13 i32) (local $l14 i32) (local $l15 i32) (local $l16 i32) (local $l17 i32) (local $l18 i32) (local $l19 i32) (local $l20 i32) (local $l21 i32) (local $l22 i32) (local $l23 i32) (local $l24 i32) (local $l25 i32) (local $l26 i32) (local $l27 i32) (local $l28 i32) (local $l29 i32) (local $l30 i32) (local $l31 i32) (local $l32 i32) (local $l33 i32) (local $l34 i32) (local $l35 i32) (local $l36 i32) (local $l37 i32) (local $l38 i32)
    get_global $g0
    set_local $l0
    i32.const 32
    set_local $l1
    get_local $l0
    get_local $l1
    i32.sub
    set_local $l2
    i32.const 3
    set_local $l3
    get_local $l2
    get_local $p0
    i32.store offset=20
    get_local $l2
    get_local $p1
    i32.store offset=16
    get_local $l2
    get_local $p2
    i32.store offset=12
    get_local $l2
    get_local $p3
    i32.store offset=8
    get_local $l2
    i32.load offset=16
    set_local $l4
    get_local $l3
    set_local $l5
    get_local $l4
    set_local $l6
    get_local $l5
    get_local $l6
    i32.gt_u
    set_local $l7
    get_local $l7
    set_local $l8
    block $B0
      block $B1
        get_local $l8
        i32.eqz
        br_if $B1
        i32.const 105
        set_local $l9
        get_local $l2
        get_local $l9
        i32.store offset=24
        br $B0
      end
      i32.const 5
      set_local $l10
      get_local $l2
      i32.load offset=20
      set_local $l11
      get_local $l11
      i32.load8_u
      set_local $l12
      get_local $l2
      get_local $l12
      i32.store8 offset=31
      get_local $l2
      i32.load8_u offset=31
      set_local $l13
      i32.const 255
      set_local $l14
      get_local $l13
      get_local $l14
      i32.and
      set_local $l15
      i32.const 15
      set_local $l16
      get_local $l15
      get_local $l16
      i32.and
      set_local $l17
      i32.const 255
      set_local $l18
      get_local $l17
      get_local $l18
      i32.and
      set_local $l19
      get_local $l10
      set_local $l20
      get_local $l19
      set_local $l21
      get_local $l20
      get_local $l21
      i32.ne
      set_local $l22
      get_local $l22
      set_local $l23
      block $B2
        get_local $l23
        i32.eqz
        br_if $B2
        i32.const 112
        set_local $l24
        get_local $l2
        get_local $l24
        i32.store offset=24
        br $B0
      end
      i32.const 0
      set_local $l25
      i32.const 3
      set_local $l26
      get_local $l2
      i32.load offset=20
      set_local $l27
      get_local $l27
      i32.load8_u offset=1
      set_local $l28
      i32.const 255
      set_local $l29
      get_local $l28
      get_local $l29
      i32.and
      set_local $l30
      get_local $l2
      i32.load offset=20
      set_local $l31
      get_local $l31
      i32.load8_u offset=2
      set_local $l32
      i32.const 255
      set_local $l33
      get_local $l32
      get_local $l33
      i32.and
      set_local $l34
      get_local $l30
      get_local $l34
      i32.add
      set_local $l35
      get_local $l2
      i32.load offset=12
      set_local $l36
      get_local $l36
      get_local $l35
      i32.store8
      get_local $l2
      i32.load offset=8
      set_local $l37
      get_local $l37
      get_local $l26
      i32.store
      get_local $l2
      get_local $l25
      i32.store offset=24
    end
    get_local $l2
    i32.load offset=24
    set_local $l38
    get_local $l38
    return)
  (func $f8 (type $t0) (param $p0 i32) (param $p1 i32) (param $p2 i32) (param $p3 i32) (result i32)
    (local $l0 i32) (local $l1 i32) (local $l2 i32) (local $l3 i32) (local $l4 i32) (local $l5 i32) (local $l6 i32) (local $l7 i32) (local $l8 i32) (local $l9 i32) (local $l10 i32) (local $l11 i32) (local $l12 i32) (local $l13 i32) (local $l14 i32) (local $l15 i32) (local $l16 i32) (local $l17 i32) (local $l18 i32) (local $l19 i32) (local $l20 i32) (local $l21 i32) (local $l22 i32) (local $l23 i32) (local $l24 i32) (local $l25 i32) (local $l26 i32) (local $l27 i32) (local $l28 i32) (local $l29 i32) (local $l30 i32) (local $l31 i32) (local $l32 i32) (local $l33 i32) (local $l34 i32) (local $l35 i32) (local $l36 i32) (local $l37 i32) (local $l38 i32) (local $l39 i32) (local $l40 i32)
    get_global $g0
    set_local $l0
    i32.const 32
    set_local $l1
    get_local $l0
    get_local $l1
    i32.sub
    set_local $l2
    i32.const 3
    set_local $l3
    get_local $l2
    get_local $p0
    i32.store offset=20
    get_local $l2
    get_local $p1
    i32.store offset=16
    get_local $l2
    get_local $p2
    i32.store offset=12
    get_local $l2
    get_local $p3
    i32.store offset=8
    get_local $l2
    i32.load offset=16
    set_local $l4
    get_local $l3
    set_local $l5
    get_local $l4
    set_local $l6
    get_local $l5
    get_local $l6
    i32.gt_u
    set_local $l7
    get_local $l7
    set_local $l8
    block $B0
      block $B1
        get_local $l8
        i32.eqz
        br_if $B1
        i32.const 105
        set_local $l9
        get_local $l2
        get_local $l9
        i32.store offset=24
        br $B0
      end
      i32.const 6
      set_local $l10
      get_local $l2
      i32.load offset=20
      set_local $l11
      get_local $l11
      i32.load8_u
      set_local $l12
      get_local $l2
      get_local $l12
      i32.store8 offset=31
      get_local $l2
      i32.load8_u offset=31
      set_local $l13
      i32.const 255
      set_local $l14
      get_local $l13
      get_local $l14
      i32.and
      set_local $l15
      i32.const 15
      set_local $l16
      get_local $l15
      get_local $l16
      i32.and
      set_local $l17
      i32.const 255
      set_local $l18
      get_local $l17
      get_local $l18
      i32.and
      set_local $l19
      get_local $l10
      set_local $l20
      get_local $l19
      set_local $l21
      get_local $l20
      get_local $l21
      i32.ne
      set_local $l22
      get_local $l22
      set_local $l23
      block $B2
        get_local $l23
        i32.eqz
        br_if $B2
        i32.const 112
        set_local $l24
        get_local $l2
        get_local $l24
        i32.store offset=24
        br $B0
      end
      i32.const 0
      set_local $l25
      i32.const 3
      set_local $l26
      get_local $l2
      i32.load offset=20
      set_local $l27
      get_local $l27
      i32.load8_u offset=2
      set_local $l28
      i32.const 255
      set_local $l29
      get_local $l28
      get_local $l29
      i32.and
      set_local $l30
      get_local $l2
      i32.load offset=20
      set_local $l31
      get_local $l31
      i32.load8_u offset=1
      set_local $l32
      i32.const 255
      set_local $l33
      get_local $l32
      get_local $l33
      i32.and
      set_local $l34
      get_local $l30
      get_local $l34
      i32.sub
      set_local $l35
      i32.const 255
      set_local $l36
      get_local $l35
      get_local $l36
      i32.and
      set_local $l37
      get_local $l2
      i32.load offset=12
      set_local $l38
      get_local $l38
      get_local $l37
      i32.store8
      get_local $l2
      i32.load offset=8
      set_local $l39
      get_local $l39
      get_local $l26
      i32.store
      get_local $l2
      get_local $l25
      i32.store offset=24
    end
    get_local $l2
    i32.load offset=24
    set_local $l40
    get_local $l40
    return)
  (func $f9 (type $t3) (param $p0 i32) (param $p1 i32) (param $p2 i32) (param $p3 i32) (param $p4 i32) (result i32)
    (local $l0 i32) (local $l1 i32) (local $l2 i32) (local $l3 i32) (local $l4 i32) (local $l5 i32) (local $l6 i32) (local $l7 i32) (local $l8 i32) (local $l9 i32) (local $l10 i32) (local $l11 i32) (local $l12 i32) (local $l13 i32) (local $l14 i32) (local $l15 i32) (local $l16 i32) (local $l17 i32) (local $l18 i32) (local $l19 i32) (local $l20 i32) (local $l21 i32) (local $l22 i32) (local $l23 i32) (local $l24 i32) (local $l25 i32) (local $l26 i32) (local $l27 i32) (local $l28 i32) (local $l29 i32) (local $l30 i32) (local $l31 i32) (local $l32 i32) (local $l33 i32) (local $l34 i32) (local $l35 i32) (local $l36 i32) (local $l37 i32) (local $l38 i32) (local $l39 i32) (local $l40 i32) (local $l41 i32) (local $l42 i32) (local $l43 i32) (local $l44 i32) (local $l45 i32) (local $l46 i32) (local $l47 i32) (local $l48 i32) (local $l49 i32) (local $l50 i32) (local $l51 i32) (local $l52 i32) (local $l53 i32) (local $l54 i32) (local $l55 i32) (local $l56 i32) (local $l57 i32) (local $l58 i32) (local $l59 i32) (local $l60 i32) (local $l61 i32) (local $l62 i32) (local $l63 i32) (local $l64 i32) (local $l65 i32) (local $l66 i32) (local $l67 i32) (local $l68 i32) (local $l69 i32) (local $l70 i32) (local $l71 i32) (local $l72 i32) (local $l73 i32) (local $l74 i32) (local $l75 i32) (local $l76 i32) (local $l77 i32) (local $l78 i32) (local $l79 i32) (local $l80 i32) (local $l81 i32) (local $l82 i32) (local $l83 i32) (local $l84 i32) (local $l85 i32) (local $l86 i32) (local $l87 i32) (local $l88 i32) (local $l89 i32) (local $l90 i32) (local $l91 i32) (local $l92 i32) (local $l93 i32) (local $l94 i32) (local $l95 i32) (local $l96 i32) (local $l97 i32) (local $l98 i32) (local $l99 i32) (local $l100 i32) (local $l101 i32) (local $l102 i32) (local $l103 i32) (local $l104 i32) (local $l105 i32) (local $l106 i32) (local $l107 i32) (local $l108 i32) (local $l109 i32) (local $l110 i32) (local $l111 i32) (local $l112 i32) (local $l113 i32)
    get_global $g0
    set_local $l0
    i32.const 64
    set_local $l1
    get_local $l0
    get_local $l1
    i32.sub
    set_local $l2
    get_local $l2
    set_global $g0
    i32.const 0
    set_local $l3
    get_local $l2
    get_local $p0
    i32.store offset=52
    get_local $l2
    get_local $p1
    i32.store offset=48
    get_local $l2
    get_local $p2
    i32.store offset=44
    get_local $l2
    get_local $p3
    i32.store offset=40
    get_local $l2
    get_local $p4
    i32.store offset=36
    get_local $l2
    get_local $l3
    i32.store offset=32
    get_local $l2
    i32.load offset=52
    set_local $l4
    get_local $l2
    get_local $l4
    i32.store offset=28
    get_local $l2
    get_local $l3
    i32.store offset=24
    block $B0
      loop $L1
        i32.const 0
        set_local $l5
        get_local $l2
        i32.load offset=28
        set_local $l6
        get_local $l2
        i32.load offset=52
        set_local $l7
        get_local $l2
        i32.load offset=48
        set_local $l8
        get_local $l7
        get_local $l8
        i32.add
        set_local $l9
        get_local $l6
        set_local $l10
        get_local $l9
        set_local $l11
        get_local $l10
        get_local $l11
        i32.lt_u
        set_local $l12
        get_local $l12
        set_local $l13
        get_local $l5
        set_local $l14
        block $B2
          get_local $l13
          i32.eqz
          br_if $B2
          get_local $l2
          i32.load offset=24
          set_local $l15
          get_local $l2
          i32.load offset=40
          set_local $l16
          get_local $l15
          set_local $l17
          get_local $l16
          set_local $l18
          get_local $l17
          get_local $l18
          i32.lt_u
          set_local $l19
          get_local $l19
          set_local $l14
        end
        block $B3
          get_local $l14
          set_local $l20
          i32.const 1
          set_local $l21
          get_local $l20
          get_local $l21
          i32.and
          set_local $l22
          get_local $l22
          i32.eqz
          br_if $B3
          i32.const 7
          set_local $l23
          get_local $l2
          i32.load offset=28
          set_local $l24
          get_local $l24
          i32.load8_u
          set_local $l25
          get_local $l2
          get_local $l25
          i32.store8 offset=63
          get_local $l2
          i32.load8_u offset=63
          set_local $l26
          i32.const 255
          set_local $l27
          get_local $l26
          get_local $l27
          i32.and
          set_local $l28
          i32.const 15
          set_local $l29
          get_local $l28
          get_local $l29
          i32.and
          set_local $l30
          get_local $l2
          get_local $l30
          i32.store8 offset=23
          get_local $l2
          i32.load8_u offset=23
          set_local $l31
          i32.const 255
          set_local $l32
          get_local $l31
          get_local $l32
          i32.and
          set_local $l33
          get_local $l23
          set_local $l34
          get_local $l33
          set_local $l35
          get_local $l34
          get_local $l35
          i32.le_s
          set_local $l36
          get_local $l36
          set_local $l37
          block $B4
            get_local $l37
            i32.eqz
            br_if $B4
            i32.const 112
            set_local $l38
            get_local $l2
            get_local $l38
            i32.store offset=56
            br $B0
          end
          i32.const 0
          set_local $l39
          i32.const 15
          set_local $l40
          get_local $l2
          get_local $l40
          i32.add
          set_local $l41
          get_local $l41
          set_local $l42
          i32.const 8
          set_local $l43
          get_local $l2
          get_local $l43
          i32.add
          set_local $l44
          get_local $l44
          set_local $l45
          i32.const 0
          set_local $l46
          i32.const 1024
          set_local $l47
          get_local $l2
          i32.load8_u offset=23
          set_local $l48
          i32.const 255
          set_local $l49
          get_local $l48
          get_local $l49
          i32.and
          set_local $l50
          i32.const 2
          set_local $l51
          get_local $l50
          get_local $l51
          i32.shl
          set_local $l52
          get_local $l47
          get_local $l52
          i32.add
          set_local $l53
          get_local $l53
          i32.load
          set_local $l54
          get_local $l2
          get_local $l54
          i32.store offset=16
          get_local $l2
          get_local $l46
          i32.store8 offset=15
          get_local $l2
          get_local $l39
          i32.store offset=8
          get_local $l2
          i32.load offset=16
          set_local $l55
          get_local $l2
          i32.load offset=28
          set_local $l56
          get_local $l2
          i32.load offset=48
          set_local $l57
          get_local $l2
          i32.load offset=28
          set_local $l58
          get_local $l2
          i32.load offset=52
          set_local $l59
          get_local $l58
          get_local $l59
          i32.sub
          set_local $l60
          get_local $l57
          get_local $l60
          i32.sub
          set_local $l61
          get_local $l56
          get_local $l61
          get_local $l42
          get_local $l45
          get_local $l55
          call_indirect (type $t0)
          set_local $l62
          get_local $l39
          set_local $l63
          get_local $l62
          set_local $l64
          get_local $l63
          get_local $l64
          i32.ne
          set_local $l65
          get_local $l65
          set_local $l66
          block $B5
            get_local $l66
            i32.eqz
            br_if $B5
            br $B3
          end
          get_local $l2
          i32.load8_u offset=15
          set_local $l67
          i32.const 255
          set_local $l68
          get_local $l67
          get_local $l68
          i32.and
          set_local $l69
          get_local $l2
          i32.load offset=44
          set_local $l70
          get_local $l2
          i32.load offset=24
          set_local $l71
          get_local $l70
          get_local $l71
          i32.add
          set_local $l72
          get_local $l72
          i32.load8_u
          set_local $l73
          i32.const 24
          set_local $l74
          get_local $l73
          get_local $l74
          i32.shl
          set_local $l75
          get_local $l75
          get_local $l74
          i32.shr_s
          set_local $l76
          get_local $l69
          set_local $l77
          get_local $l76
          set_local $l78
          get_local $l77
          get_local $l78
          i32.eq
          set_local $l79
          get_local $l79
          set_local $l80
          block $B6
            get_local $l80
            i32.eqz
            br_if $B6
            get_local $l2
            i32.load offset=32
            set_local $l81
            i32.const 1
            set_local $l82
            get_local $l81
            get_local $l82
            i32.add
            set_local $l83
            get_local $l2
            get_local $l83
            i32.store offset=32
          end
          get_local $l2
          i32.load offset=8
          set_local $l84
          get_local $l2
          i32.load offset=28
          set_local $l85
          get_local $l85
          get_local $l84
          i32.add
          set_local $l86
          get_local $l2
          get_local $l86
          i32.store offset=28
          get_local $l2
          i32.load offset=24
          set_local $l87
          i32.const 1
          set_local $l88
          get_local $l87
          get_local $l88
          i32.add
          set_local $l89
          get_local $l2
          get_local $l89
          i32.store offset=24
          br $L1
        end
      end
      get_local $l2
      i32.load offset=28
      set_local $l90
      get_local $l2
      i32.load offset=52
      set_local $l91
      get_local $l2
      i32.load offset=48
      set_local $l92
      get_local $l91
      get_local $l92
      i32.add
      set_local $l93
      get_local $l90
      set_local $l94
      get_local $l93
      set_local $l95
      get_local $l94
      get_local $l95
      i32.ne
      set_local $l96
      get_local $l96
      set_local $l97
      block $B7
        block $B8
          get_local $l97
          i32.eqz
          br_if $B8
          i32.const 0
          set_local $l98
          get_local $l2
          i32.load offset=36
          set_local $l99
          get_local $l99
          get_local $l98
          i32.store8
          br $B7
        end
        get_local $l2
        i32.load offset=32
        set_local $l100
        get_local $l2
        i32.load offset=40
        set_local $l101
        get_local $l100
        set_local $l102
        get_local $l101
        set_local $l103
        get_local $l102
        get_local $l103
        i32.ne
        set_local $l104
        get_local $l104
        set_local $l105
        block $B9
          block $B10
            get_local $l105
            i32.eqz
            br_if $B10
            i32.const 0
            set_local $l106
            get_local $l2
            i32.load offset=36
            set_local $l107
            get_local $l107
            get_local $l106
            i32.store8
            br $B9
          end
          i32.const 1
          set_local $l108
          get_local $l2
          i32.load offset=36
          set_local $l109
          get_local $l109
          get_local $l108
          i32.store8
        end
      end
      i32.const 0
      set_local $l110
      get_local $l2
      get_local $l110
      i32.store offset=56
    end
    get_local $l2
    i32.load offset=56
    set_local $l111
    i32.const 64
    set_local $l112
    get_local $l2
    get_local $l112
    i32.add
    set_local $l113
    get_local $l113
    set_global $g0
    get_local $l111
    return)
  (func $Match (type $t0) (param $p0 i32) (param $p1 i32) (param $p2 i32) (param $p3 i32) (result i32)
    (local $l0 i32) (local $l1 i32) (local $l2 i32) (local $l3 i32) (local $l4 i32) (local $l5 i32) (local $l6 i32) (local $l7 i32) (local $l8 i32) (local $l9 i32) (local $l10 i32) (local $l11 i32) (local $l12 i32) (local $l13 i32) (local $l14 i32) (local $l15 i32) (local $l16 i32) (local $l17 i32) (local $l18 i32) (local $l19 i32) (local $l20 i32) (local $l21 i32) (local $l22 i32) (local $l23 i32)
    get_global $g0
    set_local $l0
    i32.const 32
    set_local $l1
    get_local $l0
    get_local $l1
    i32.sub
    set_local $l2
    get_local $l2
    set_global $g0
    i32.const 0
    set_local $l3
    i32.const 11
    set_local $l4
    get_local $l2
    get_local $l4
    i32.add
    set_local $l5
    get_local $l5
    set_local $l6
    i32.const 0
    set_local $l7
    get_local $l2
    get_local $p0
    i32.store offset=24
    get_local $l2
    get_local $p1
    i32.store offset=20
    get_local $l2
    get_local $p2
    i32.store offset=16
    get_local $l2
    get_local $p3
    i32.store offset=12
    get_local $l2
    get_local $l7
    i32.store8 offset=11
    get_local $l2
    i32.load offset=24
    set_local $l8
    get_local $l2
    i32.load offset=20
    set_local $l9
    get_local $l2
    i32.load offset=16
    set_local $l10
    get_local $l2
    i32.load offset=12
    set_local $l11
    get_local $l8
    get_local $l9
    get_local $l10
    get_local $l11
    get_local $l6
    call $f9
    set_local $l12
    get_local $l3
    set_local $l13
    get_local $l12
    set_local $l14
    get_local $l13
    get_local $l14
    i32.ne
    set_local $l15
    get_local $l15
    set_local $l16
    block $B0
      block $B1
        get_local $l16
        i32.eqz
        br_if $B1
        i32.const 0
        set_local $l17
        get_local $l2
        get_local $l17
        i32.store offset=28
        br $B0
      end
      get_local $l2
      i32.load8_u offset=11
      set_local $l18
      i32.const 1
      set_local $l19
      get_local $l18
      get_local $l19
      i32.and
      set_local $l20
      get_local $l2
      get_local $l20
      i32.store offset=28
    end
    get_local $l2
    i32.load offset=28
    set_local $l21
    i32.const 32
    set_local $l22
    get_local $l2
    get_local $l22
    i32.add
    set_local $l23
    get_local $l23
    set_global $g0
    get_local $l21
    return)
  (func $writev_c (type $t4) (param $p0 i32) (param $p1 i32) (param $p2 i32) (result i32)
    (local $l0 i32) (local $l1 i32) (local $l2 i32) (local $l3 i32) (local $l4 i32) (local $l5 i32) (local $l6 i32) (local $l7 i32) (local $l8 i32) (local $l9 i32) (local $l10 i32) (local $l11 i32) (local $l12 i32) (local $l13 i32) (local $l14 i32) (local $l15 i32) (local $l16 i32) (local $l17 i32) (local $l18 i32) (local $l19 i32) (local $l20 i32) (local $l21 i32) (local $l22 i32) (local $l23 i32) (local $l24 i32) (local $l25 i32) (local $l26 i32) (local $l27 i32) (local $l28 i32) (local $l29 i32) (local $l30 i32) (local $l31 i32) (local $l32 i32) (local $l33 i32) (local $l34 i32) (local $l35 i32) (local $l36 i32) (local $l37 i32) (local $l38 i32) (local $l39 i32) (local $l40 i32) (local $l41 i32) (local $l42 i32) (local $l43 i32) (local $l44 i32) (local $l45 i32) (local $l46 i32) (local $l47 i32) (local $l48 i32) (local $l49 i32) (local $l50 i32)
    get_global $g0
    set_local $l0
    i32.const 32
    set_local $l1
    get_local $l0
    get_local $l1
    i32.sub
    set_local $l2
    get_local $l2
    set_global $g0
    i32.const 0
    set_local $l3
    get_local $l2
    get_local $p0
    i32.store offset=28
    get_local $l2
    get_local $p1
    i32.store offset=24
    get_local $l2
    get_local $p2
    i32.store offset=20
    get_local $l2
    get_local $l3
    i32.store offset=16
    get_local $l2
    get_local $l3
    i32.store offset=12
    block $B0
      loop $L1
        get_local $l2
        i32.load offset=12
        set_local $l4
        get_local $l2
        i32.load offset=20
        set_local $l5
        get_local $l4
        set_local $l6
        get_local $l5
        set_local $l7
        get_local $l6
        get_local $l7
        i32.lt_s
        set_local $l8
        get_local $l8
        set_local $l9
        get_local $l9
        i32.eqz
        br_if $B0
        i32.const 0
        set_local $l10
        get_local $l2
        get_local $l10
        i32.store offset=8
        block $B2
          loop $L3
            get_local $l2
            i32.load offset=8
            set_local $l11
            get_local $l2
            i32.load offset=24
            set_local $l12
            get_local $l2
            i32.load offset=12
            set_local $l13
            i32.const 3
            set_local $l14
            get_local $l13
            get_local $l14
            i32.shl
            set_local $l15
            get_local $l12
            get_local $l15
            i32.add
            set_local $l16
            get_local $l16
            i32.load offset=4
            set_local $l17
            get_local $l11
            set_local $l18
            get_local $l17
            set_local $l19
            get_local $l18
            get_local $l19
            i32.lt_u
            set_local $l20
            get_local $l20
            set_local $l21
            get_local $l21
            i32.eqz
            br_if $B2
            get_local $l2
            i32.load offset=24
            set_local $l22
            get_local $l2
            i32.load offset=12
            set_local $l23
            i32.const 3
            set_local $l24
            get_local $l23
            get_local $l24
            i32.shl
            set_local $l25
            get_local $l22
            get_local $l25
            i32.add
            set_local $l26
            get_local $l26
            i32.load
            set_local $l27
            get_local $l2
            i32.load offset=8
            set_local $l28
            get_local $l27
            get_local $l28
            i32.add
            set_local $l29
            get_local $l29
            i32.load8_u
            set_local $l30
            i32.const 24
            set_local $l31
            get_local $l30
            get_local $l31
            i32.shl
            set_local $l32
            get_local $l32
            get_local $l31
            i32.shr_s
            set_local $l33
            get_local $l33
            call $env.putc_js
            get_local $l2
            i32.load offset=8
            set_local $l34
            i32.const 1
            set_local $l35
            get_local $l34
            get_local $l35
            i32.add
            set_local $l36
            get_local $l2
            get_local $l36
            i32.store offset=8
            br $L3
          end
        end
        get_local $l2
        i32.load offset=24
        set_local $l37
        get_local $l2
        i32.load offset=12
        set_local $l38
        i32.const 3
        set_local $l39
        get_local $l38
        get_local $l39
        i32.shl
        set_local $l40
        get_local $l37
        get_local $l40
        i32.add
        set_local $l41
        get_local $l41
        i32.load offset=4
        set_local $l42
        get_local $l2
        i32.load offset=16
        set_local $l43
        get_local $l43
        get_local $l42
        i32.add
        set_local $l44
        get_local $l2
        get_local $l44
        i32.store offset=16
        get_local $l2
        i32.load offset=12
        set_local $l45
        i32.const 1
        set_local $l46
        get_local $l45
        get_local $l46
        i32.add
        set_local $l47
        get_local $l2
        get_local $l47
        i32.store offset=12
        br $L1
      end
    end
    get_local $l2
    i32.load offset=16
    set_local $l48
    i32.const 32
    set_local $l49
    get_local $l2
    get_local $l49
    i32.add
    set_local $l50
    get_local $l50
    set_global $g0
    get_local $l48
    return)
  (table $T0 8 8 anyfunc)
  (memory $memory 2)
  (global $g0 (mut i32) (i32.const 66592))
  (global $__heap_base i32 (i32.const 66592))
  (global $__data_end i32 (i32.const 1052))
  (export "memory" (memory 0))
  (export "__heap_base" (global 1))
  (export "__data_end" (global 2))
  (export "Match" (func $Match))
  (export "writev_c" (func $writev_c))
  (elem (i32.const 1) $f2 $f3 $f4 $f5 $f6 $f7 $f8)
  (data (i32.const 1024) "\01\00\00\00\02\00\00\00\03\00\00\00\04\00\00\00\05\00\00\00\06\00\00\00\07\00\00\00"))

