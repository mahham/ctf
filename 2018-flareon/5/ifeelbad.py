"""
from capstone import *
from decompiler import *
from host import dis
from output import c

# Create a Capstone object, which will be used as disassembler
md = Cs(CS_ARCH_X86, CS_MODE_32)

# Define a bunch of bytes to disassemble
code = "\x55\x89\xe5\x83\xec\x28\xc7\x45\xf4\x00\x00\x00\x00\x8b\x45\xf4\x8b\x00\x83\xf8\x0e\x75\x0c\xc7\x04\x24\x30\x87\x04\x08\xe8\xd3\xfe\xff\xff\xb8\x00\x00\x00\x00\xc9\xc3"

# Create the capstone-specific backend; it will yield expressions that the decompiler is able to use.
disasm = dis.available_disassemblers['capstone'].create(md, code, 0x1000)

# Create the decompiler
dec = decompiler_t(disasm, 0x1000)

# Transform the function until it is decompiled
dec.step_until(step_decompiled)

# Tokenize and output the function as string
print(''.join([str(o) for o in c.tokenizer(dec.function).tokens]))
"""

import sys
sys.path.append('./deco/')

from capstone import *
from decompiler import *
from host import dis
from output import c

# Create a Capstone object, which will be used as disassembler
md = Cs(CS_ARCH_X86, CS_MODE_64)

import struct
f2 = struct.pack ("1517B", *[
0x55,0x48,0x89,0xe5,0x48,0x81,0xec,0xa0,0x00,0x00,0x00,
0x89,0xbd,0x6c,0xff,0xff,0xff,0x89,0xb5,0x68,0xff,0xff,
0xff,0x89,0x95,0x64,0xff,0xff,0xff,0x89,0x8d,0x60,0xff,
0xff,0xff,0xc7,0x85,0x78,0xff,0xff,0xff,0x00,0x00,0x00,
0x00,0xc7,0x85,0x7c,0xff,0xff,0xff,0x00,0x00,0x00,0x00,
0xc7,0x45,0x80,0x00,0x00,0x00,0x00,0xc7,0x45,0x84,0x00,
0x00,0x00,0x00,0xc7,0x45,0x88,0x00,0x00,0x00,0x00,0xc7,
0x45,0x8c,0x00,0x00,0x00,0x00,0xc7,0x45,0x90,0x00,0x00,
0x00,0x00,0xc7,0x45,0x94,0x00,0x00,0x00,0x00,0xc7,0x45,
0x98,0x00,0x00,0x00,0x00,0xc7,0x45,0x9c,0x00,0x00,0x00,
0x00,0xc7,0x45,0xa0,0x00,0x00,0x00,0x00,0xc7,0x45,0xa4,
0x00,0x00,0x00,0x00,0xc7,0x45,0xa8,0x00,0x00,0x00,0x00,
0xc7,0x45,0xac,0x00,0x00,0x00,0x00,0xc7,0x45,0xb0,0x00,
0x00,0x00,0x00,0xc7,0x45,0xb4,0x00,0x00,0x00,0x00,0xc7,
0x45,0xb8,0x00,0x00,0x00,0x00,0xc7,0x45,0xbc,0x00,0x00,
0x00,0x00,0xc7,0x45,0xc0,0x00,0x00,0x00,0x00,0xc7,0x45,
0xc4,0x00,0x00,0x00,0x00,0xc7,0x45,0xc8,0x00,0x00,0x00,
0x00,0xc7,0x45,0xcc,0x00,0x00,0x00,0x00,0xc7,0x45,0xd0,
0x00,0x00,0x00,0x00,0xc7,0x45,0xd4,0x00,0x00,0x00,0x00,
0xc7,0x45,0xd8,0x00,0x00,0x00,0x00,0xc7,0x45,0xdc,0x00,
0x00,0x00,0x00,0xc7,0x45,0xe0,0x00,0x00,0x00,0x00,0xc7,
0x45,0xe4,0x00,0x00,0x00,0x00,0xc7,0x45,0xe8,0x00,0x00,
0x00,0x00,0xc7,0x45,0xec,0x00,0x00,0x00,0x00,0xc7,0x45,
0xf0,0x00,0x00,0x00,0x00,0xc7,0x45,0xf4,0x00,0x00,0x00,
0x00,0x8b,0x05,0x9c,0x74,0x20,0x00,0x83,0xc0,0x01,0x89,
0x05,0x93,0x74,0x20,0x00,0x8b,0x05,0x8d,0x74,0x20,0x00,
0x3d,0xf4,0x01,0x00,0x00,0x76,0x0a,0xbf,0x07,0x00,0x00,
0x00,0xe8,0xc1,0x59,0x00,0x00,0x8b,0x05,0x2e,0x73,0x20,
0x00,0x89,0x45,0xf8,0x8b,0x45,0xf8,0x89,0x85,0x78,0xff,
0xff,0xff,0xc7,0x45,0xf8,0x20,0x00,0x00,0x00,0x8b,0x45,
0xf8,0x89,0x85,0x7c,0xff,0xff,0xff,0x8b,0x85,0x78,0xff,
0xff,0xff,0x89,0x45,0xf8,0x8b,0x85,0x7c,0xff,0xff,0xff,
0x89,0x45,0xfc,0x8b,0x45,0xfc,0x29,0x45,0xf8,0x8b,0x45,
0xf8,0x89,0x45,0x80,0xc7,0x45,0xf8,0x02,0x00,0x00,0x00,
0x8b,0x45,0xf8,0x89,0x45,0x84,0x8b,0x45,0x80,0x89,0x45,
0xf8,0x8b,0x85,0x6c,0xff,0xff,0xff,0x89,0x45,0xfc,0x8b,
0x45,0xf8,0x83,0xc0,0x14,0x89,0xc1,0x8b,0x45,0xfc,0x89,
0xc2,0x48,0x89,0xce,0x48,0x8d,0x3d,0xcd,0x72,0x20,0x00,
0xe8,0x1f,0xfc,0xff,0xff,0x8b,0x45,0x80,0x89,0x45,0xf8,
0x8b,0x85,0x68,0xff,0xff,0xff,0x89,0x45,0xfc,0x8b,0x45,
0xf8,0x83,0xc0,0x10,0x89,0xc1,0x8b,0x45,0xfc,0x89,0xc2,
0x48,0x89,0xce,0x48,0x8d,0x3d,0xa2,0x72,0x20,0x00,0xe8,
0xf4,0xfb,0xff,0xff,0x8b,0x45,0x80,0x89,0x45,0xf8,0x8b,
0x85,0x64,0xff,0xff,0xff,0x89,0x45,0xfc,0x8b,0x45,0xf8,
0x83,0xc0,0x0c,0x89,0xc1,0x8b,0x45,0xfc,0x89,0xc2,0x48,
0x89,0xce,0x48,0x8d,0x3d,0x77,0x72,0x20,0x00,0xe8,0xc9,
0xfb,0xff,0xff,0x8b,0x45,0x80,0x89,0x45,0xf8,0x8b,0x85,
0x60,0xff,0xff,0xff,0x89,0x45,0xfc,0x8b,0x45,0xf8,0x83,
0xc0,0x08,0x89,0xc1,0x8b,0x45,0xfc,0x89,0xc2,0x48,0x89,
0xce,0x48,0x8d,0x3d,0x4c,0x72,0x20,0x00,0xe8,0x9e,0xfb,
0xff,0xff,0x8b,0x45,0x80,0x89,0x45,0xf8,0x8b,0x45,0xf8,
0x83,0xc0,0x10,0x89,0xc0,0x48,0x89,0xc6,0x48,0x8d,0x3d,
0x2f,0x72,0x20,0x00,0xe8,0x94,0xfa,0xff,0xff,0x89,0x45,
0xf8,0x8b,0x45,0xf8,0x89,0x45,0x88,0x8b,0x45,0x84,0x89,
0x45,0xf8,0x8b,0x45,0xf8,0x89,0x45,0x8c,0x8b,0x45,0x88,
0x89,0x45,0xf8,0x8b,0x45,0xf8,0x89,0x45,0x90,0x8b,0x45,
0x8c,0x89,0x45,0xf8,0x8b,0x45,0x90,0x89,0x45,0xfc,0x8b,
0x45,0xf8,0x3b,0x45,0xfc,0x0f,0x97,0xc0,0x0f,0xb6,0xc0,
0x89,0x45,0xf8,0x8b,0x45,0xf8,0x89,0x45,0x94,0x8b,0x45,
0x94,0x89,0x45,0xf8,0x8b,0x45,0xf8,0x89,0x45,0x98,0x8b,
0x45,0x98,0x89,0x45,0xf8,0x83,0x7d,0xf8,0x00,0x0f,0x94,
0xc0,0x0f,0xb6,0xc0,0x89,0x45,0xf8,0x83,0x7d,0xf8,0x00,
0x75,0x3a,0xc7,0x45,0xf8,0x69,0x00,0x00,0x00,0x8b,0x45,
0xf8,0x89,0x45,0x9c,0x8b,0x45,0x80,0x89,0x45,0xf8,0x8b,
0x45,0x9c,0x89,0x45,0xfc,0x8b,0x45,0xf8,0x83,0xc0,0x18,
0x89,0xc1,0x8b,0x45,0xfc,0x89,0xc2,0x48,0x89,0xce,0x48,
0x8d,0x3d,0x93,0x71,0x20,0x00,0xe8,0xe5,0xfa,0xff,0xff,
0xe9,0xc7,0x02,0x00,0x00,0x90,0xc7,0x45,0xf8,0x00,0x00,
0x00,0x00,0x8b,0x45,0xf8,0x89,0x45,0xa0,0x8b,0x45,0x80,
0x89,0x45,0xf8,0x8b,0x45,0xf8,0x83,0xc0,0x14,0x89,0xc0,
0x48,0x89,0xc6,0x48,0x8d,0x3d,0x63,0x71,0x20,0x00,0xe8,
0xc8,0xf9,0xff,0xff,0x89,0x45,0xf8,0x8b,0x45,0xf8,0x89,
0x45,0xa4,0x8b,0x45,0xa4,0x89,0x45,0xf8,0x8b,0x45,0xf8,
0x48,0x89,0xc6,0x48,0x8d,0x3d,0x42,0x71,0x20,0x00,0xe8,
0x1b,0xfa,0xff,0xff,0x89,0x45,0xf8,0x8b,0x45,0xf8,0x89,
0x45,0xa8,0x8b,0x45,0x80,0x89,0x45,0xf8,0x8b,0x45,0xa8,
0x89,0x45,0xfc,0x8b,0x45,0xf8,0x83,0xc0,0x1f,0x89,0xc1,
0x8b,0x45,0xfc,0x89,0xc2,0x48,0x89,0xce,0x48,0x8d,0x3d,
0x11,0x71,0x20,0x00,0xe8,0xde,0xfa,0xff,0xff,0x8b,0x45,
0x80,0x89,0x45,0xf8,0x8b,0x45,0xf8,0x83,0xc0,0x1f,0x89,
0xc0,0x48,0x89,0xc6,0x48,0x8d,0x3d,0xf4,0x70,0x20,0x00,
0xe8,0xcd,0xf9,0xff,0xff,0x89,0x45,0xf8,0x8b,0x45,0xf8,
0x89,0x45,0xac,0xc7,0x45,0xf8,0xff,0x00,0x00,0x00,0x8b,
0x45,0xf8,0x89,0x45,0xb0,0x8b,0x45,0xac,0x89,0x45,0xf8,
0x8b,0x45,0xb0,0x89,0x45,0xfc,0x8b,0x45,0xfc,0x21,0x45,
0xf8,0x8b,0x45,0xf8,0x89,0x45,0xb4,0xc7,0x45,0xf8,0x0f,
0x00,0x00,0x00,0x8b,0x45,0xf8,0x89,0x45,0xb8,0x8b,0x45,
0xb4,0x89,0x45,0xf8,0x8b,0x45,0xb8,0x89,0x45,0xfc,0x8b,
0x45,0xfc,0x21,0x45,0xf8,0x8b,0x45,0xf8,0x89,0x45,0xbc,
0xc7,0x45,0xf8,0xff,0x00,0x00,0x00,0x8b,0x45,0xf8,0x89,
0x45,0xc0,0x8b,0x45,0xbc,0x89,0x45,0xf8,0x8b,0x45,0xc0,
0x89,0x45,0xfc,0x8b,0x45,0xfc,0x21,0x45,0xf8,0x8b,0x45,
0xf8,0x89,0x45,0xc4,0x8b,0x45,0xa0,0x89,0x45,0xf8,0x8b,
0x45,0xf8,0x89,0x45,0xc8,0x8b,0x45,0xc4,0x89,0x45,0xf8,
0x8b,0x45,0xf8,0x89,0x45,0xcc,0x8b,0x45,0xc8,0x89,0x45,
0xf8,0x8b,0x45,0xcc,0x89,0x45,0xfc,0x8b,0x45,0xf8,0x3b,
0x45,0xfc,0x0f,0x95,0xc0,0x0f,0xb6,0xc0,0x89,0x45,0xf8,
0x8b,0x45,0xf8,0x89,0x45,0xd0,0x8b,0x45,0xd0,0x89,0x45,
0xf8,0x8b,0x45,0xf8,0x89,0x45,0xd4,0x8b,0x45,0xd4,0x89,
0x45,0xf8,0x83,0x7d,0xf8,0x00,0x0f,0x94,0xc0,0x0f,0xb6,
0xc0,0x89,0x45,0xf8,0x83,0x7d,0xf8,0x00,0x75,0x3a,0xc7,
0x45,0xf8,0x70,0x00,0x00,0x00,0x8b,0x45,0xf8,0x89,0x45,
0xd8,0x8b,0x45,0x80,0x89,0x45,0xf8,0x8b,0x45,0xd8,0x89,
0x45,0xfc,0x8b,0x45,0xf8,0x83,0xc0,0x18,0x89,0xc1,0x8b,
0x45,0xfc,0x89,0xc2,0x48,0x89,0xce,0x48,0x8d,0x3d,0xe9,
0x6f,0x20,0x00,0xe8,0x3b,0xf9,0xff,0xff,0xe9,0x1d,0x01,
0x00,0x00,0x90,0xc7,0x45,0xf8,0x00,0x00,0x00,0x00,0x8b,
0x45,0xf8,0x89,0x45,0xdc,0xc7,0x45,0xf8,0x02,0x00,0x00,
0x00,0x8b,0x45,0xf8,0x89,0x45,0xe0,0x8b,0x45,0x80,0x89,
0x45,0xf8,0x8b,0x45,0xf8,0x83,0xc0,0x14,0x89,0xc0,0x48,
0x89,0xc6,0x48,0x8d,0x3d,0xac,0x6f,0x20,0x00,0xe8,0x11,
0xf8,0xff,0xff,0x89,0x45,0xf8,0x8b,0x45,0xf8,0x89,0x45,
0xe4,0x8b,0x45,0xe4,0x89,0x45,0xf8,0x8b,0x45,0xf8,0x83,
0xc0,0x01,0x89,0xc0,0x48,0x89,0xc6,0x48,0x8d,0x3d,0x86,
0x6f,0x20,0x00,0xe8,0x5f,0xf8,0xff,0xff,0x89,0x45,0xf8,
0x8b,0x45,0xf8,0x89,0x45,0xe8,0x8b,0x45,0x80,0x89,0x45,
0xf8,0x8b,0x45,0xf8,0x83,0xc0,0x0c,0x89,0xc0,0x48,0x89,
0xc6,0x48,0x8d,0x3d,0x60,0x6f,0x20,0x00,0xe8,0xc5,0xf7,
0xff,0xff,0x89,0x45,0xf8,0x8b,0x45,0xf8,0x89,0x45,0xec,
0x8b,0x45,0xec,0x89,0x45,0xf8,0x8b,0x45,0xe8,0x89,0x45,
0xfc,0x8b,0x45,0xf8,0x8b,0x55,0xfc,0x48,0x89,0xc6,0x48,
0x8d,0x3d,0x36,0x6f,0x20,0x00,0xe8,0x03,0xf9,0xff,0xff,
0x8b,0x45,0x80,0x89,0x45,0xf8,0x8b,0x45,0xf8,0x83,0xc0,
0x08,0x89,0xc0,0x48,0x89,0xc6,0x48,0x8d,0x3d,0x19,0x6f,
0x20,0x00,0xe8,0x7e,0xf7,0xff,0xff,0x89,0x45,0xf8,0x8b,
0x45,0xf8,0x89,0x45,0xf0,0x8b,0x45,0xf0,0x89,0x45,0xf8,
0x8b,0x45,0xe0,0x89,0x45,0xfc,0x8b,0x45,0xf8,0x8b,0x55,
0xfc,0x48,0x89,0xc6,0x48,0x8d,0x3d,0xef,0x6e,0x20,0x00,
0xe8,0x41,0xf8,0xff,0xff,0x8b,0x45,0x80,0x89,0x45,0xf8,
0x8b,0x45,0xdc,0x89,0x45,0xfc,0x8b,0x45,0xf8,0x83,0xc0,
0x18,0x89,0xc1,0x8b,0x45,0xfc,0x89,0xc2,0x48,0x89,0xce,
0x48,0x8d,0x3d,0xc7,0x6e,0x20,0x00,0xe8,0x19,0xf8,0xff,
0xff,0x8b,0x45,0x80,0x89,0x45,0xf8,0x8b,0x45,0xf8,0x83,
0xc0,0x18,0x89,0xc0,0x48,0x89,0xc6,0x48,0x8d,0x3d,0xaa,
0x6e,0x20,0x00,0xe8,0x0f,0xf7,0xff,0xff,0x89,0x45,0xf8,
0x8b,0x45,0xf8,0x89,0x45,0xf4,0x8b,0x45,0xf4,0x89,0x45,
0xf8,0x90,0x8b,0x05,0xcb,0x6f,0x20,0x00,0x83,0xe8,0x01,
0x89,0x05,0xc2,0x6f,0x20,0x00,0x8b,0x45,0xf8,0xc9])

f0 = """55           
4889e5        
8b05dd752000  
83c001        
8905d4752000  
8b05ce752000  
3df4010000    
760a          
bf07000000    
e8025b0000    
8b05b7752000  
83e801        
8905ae752000  
90            
5d            
c3""".replace("\n", "").replace(" ","")
f0x = bytes([int(f0[i:i+1], 16) for i in range(0, len(f0)-2, 2)])

# Create the capstone-specific backend; it will yield expressions that the decompiler is able to use.
disasm = dis.available_disassemblers['capstone'].create(md, f2, 0xbe1)

#disasm.add_name(3830, "func_3830")
disasm.add_name(0x000008ba, "i32_load")
disasm.add_name(0x0000092e, "i32_load8_u")
disasm.add_name(0x000009a7, "i32_store")
disasm.add_name(0x00000a22, "i32_store8")
# Create the decompiler
dec = decompiler_t(disasm, 0xbe1)

# Transform the function until it is decompiled
dec.step_until(step_decompiled)

# Tokenize and output the function as string
print(''.join([str(o) for o in c.tokenizer(dec.function).tokens]))
