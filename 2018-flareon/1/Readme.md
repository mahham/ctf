.jar files are zip archives so we can extract
`MinesweeperChampionshipRegistration.jar` and take a look at the files inside.
The flag is stored in plaintext in `InviteValidator.class`:
`GoldenTicket2018@flare-on.com`