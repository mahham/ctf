import r2pipe as r2
import sys

r = r2.open(
	filename='', flags=['-d', 'rarun2', 'program='+sys.argv[1], 'stdin="AAA\n"'])
r.cmd("doo")			# Currently debugging rarun2, not the binary

print(r.cmd("?E Hello!"))

r.cmd("db 0x012B1274")	# Break a bit before the check
r.cmd("dc")				# Go to the check

# Get correct password address
print(r.cmdj("pdj 1 @ 0x012b1288"))
