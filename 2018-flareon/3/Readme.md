
After running any binary with x64dbg you can see the given user input is checked
to be equal to a 16bit little endian wide char string at address 0x012B4380.
The correct password string is copied before that check from the PE resource
with the name `BRICK`. Taking a look at the file structure again, the correct
password is the last 16bit-little endian string in the file.

Running
`for i in $(ls FLEGGO/*.exe); do strings -e l $i | tail -n 1 | $i; done`
created a bunch of images in the folder and wrote out stuff like:

`19343964.png => o`

All images had a number in the top left corner. Ordering the image-character
pairs in ascending order based on the numbers in the images gets us the flag:
`mor3_awes0m3_th4n_an_awes0me_p0ssum@flare-on.com`

Sorry for the short writeup. I'd love to rewrite it in way more detail sometime.
