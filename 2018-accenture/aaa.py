# -*- coding: utf-8 -*-
"""
    pbkdf2
    ~~~~~~
    This module implements pbkdf2 for Python.  It also has some basic
    tests that ensure that it works.  The implementation is straightforward
    and uses stdlib only stuff and can be easily be copy/pasted into
    your favourite application.
    Use this as replacement for bcrypt that does not need a c implementation
    of a modified blowfish crypto algo.
    Example usage:
    >>> pbkdf2_hex('what i want to hash', 'the random salt')
    'fa7cc8a2b0a932f8e6ea42f9787e9d36e592e0c222ada6a9'
    How to use this:
    1.  Use a constant time string compare function to compare the stored hash
        with the one you're generating::
            def safe_str_cmp(a, b):
                if len(a) != len(b):
                    return False
                rv = 0
                for x, y in izip(a, b):
                    rv |= ord(x) ^ ord(y)
                return rv == 0
    2.  Use `os.urandom` to generate a proper salt of at least 8 byte.
        Use a unique salt per hashed password.
    3.  Store ``algorithm$salt:costfactor$hash`` in the database so that
        you can upgrade later easily to a different algorithm if you need
        one.  For instance ``PBKDF2-256$thesalt:10000$deadbeef...``.
    :copyright: (c) Copyright 2011 by Armin Ronacher.
    :license: BSD, see LICENSE for more details.
"""
import hmac
import hashlib
from struct import Struct
from operator import xor
from itertools import izip, starmap


_pack_int = Struct('>I').pack


def pbkdf2_hex(data, salt, iterations=1000, keylen=24, hashfunc=None):
    """Like :func:`pbkdf2_bin` but returns a hex encoded string."""
    return pbkdf2_bin(data, salt, iterations, keylen, hashfunc).encode('hex')


def pbkdf2_bin(data, salt, iterations=1000, keylen=24, hashfunc=None):
    """Returns a binary digest for the PBKDF2 hash algorithm of `data`
    with the given `salt`.  It iterates `iterations` time and produces a
    key of `keylen` bytes.  By default SHA-1 is used as hash function,
    a different hashlib `hashfunc` can be provided.
    """
    hashfunc = hashfunc or hashlib.sha1
    mac = hmac.new(data, None, hashfunc)
    def _pseudorandom(x, mac=mac):
        h = mac.copy()
        h.update(x)
        return map(ord, h.digest())
    buf = []
    for block in xrange(1, -(-keylen // mac.digest_size) + 1):
        rv = u = _pseudorandom(salt + _pack_int(block))
        for i in xrange(iterations - 1):
            u = _pseudorandom(''.join(map(chr, u)))
            rv = starmap(xor, izip(rv, u))
        buf.extend(rv)
    return ''.join(map(chr, buf))[:keylen]

a = "slsd!#$sa";
b = "#$KD213ks";
c = "@13Ag1S4d3f6g123";


cacat = pbkdf2_bin(a, b, keylen=32)
print cacat.encode("hex")

from Crypto.Cipher import AES 

d = AES.new("643db8d4c84c49bd7784d9f0678648067f922471576be32ecbb01021ca76dd9e".decode("hex"), AES.MODE_CBC, c)

from base64 import *

print d.decrypt(b64decode("SVSeTq9IAz47r9oWZPpNYw=="))


aaaaaaa = ''.join(chr(a) for a in [67,66,68,117,62,112,76,8,109,94,49,16,108,52,110,106,17,103,95,5,109,74,42,18,87,52,114,106,41,89,95,7,109,119,33,91,111,25,64,60,57,100,64,7,65,103,37,84,111,52,110,120])
aaaaaa = ''.join(chr(a) for a in [67,66,68,117,62,112,76,8,109,94,49,16,108,52,110,106,17,103,95,5,109,74,42,18,87,52,114,106,41,89,95,7,109,119,33,91,111,25,64,60,57,100,64,7,65,103,37,84,111,52,110,120])
assert(aaaaaaa == aaaaaa)
#print aaaaaaa

import string
aaaaaa = aaaaaa[:-1]
for k1 in range(256):
    for i in range(0, len(aaaaaa), 3):  
        if chr(ord(aaaaaa[i]) ^ k1) not in string.printable:
            break
    else:
        print "k1 += [{}]".format(k1)
for k2 in range(256):
    for i in range(0, len(aaaaaa), 3):  
        if chr(ord(aaaaaa[i+1]) ^ k2) not in string.printable:
            break
    else:
        print "k2 += [{}]".format(k2)
for k3 in range(256):
    for i in range(0, len(aaaaaa), 3):  
        if chr(ord(aaaaaa[i+2]) ^ k3) not in string.printable:
            break
    else:
        print "k3 += [{}]".format(k3)


e = ""
for j in range(0, len(aaaaaaa), 3):
    try:
        e += chr(ord(aaaaaaa[j]) ^ 1)
        e += chr(ord(aaaaaaa[j+1]) ^ 2)
        e += chr(ord(aaaaaaa[j+2]) ^ 3)
    except:
        pass
print e
print b64encode(e)


q = d.decrypt(e)
p = ""
for j in range(0, len(q), 3):
    try:
        p += chr(ord(q[j]) ^ 0)
        p += chr(ord(q[j+1]) ^ 0)
        p += chr(ord(q[j+2]) ^ 0)
    except:
        pass
print ">>", p

print b64encode(p)






