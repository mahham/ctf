"use strict";

const io = require('socket.io-client');


const rotR = [, 2, 3, 4, 1],
	rotL = [, 4, 1, 2, 3],
	dirS = [, "left", "top", "right", "bottom"];
let forward = 1, right = rotR[forward];

const FSM = {
	state: "start",
	transitions: {
		start: {
			ok: "stepright"
		},
		stepright: {
			ok: "rotateright",
			err: "stepfwd"
		},
		rotateright: {
			ok: "repeat"
		},
		stepfwd: {
			ok: "repeat",
			err: "rotateleft"
		},
		repeat: {
			ok: "stepright"
		},
		rotateleft: {
			ok: "repeat"
		}
	},
	functions: {
		start: ok,
		stepright: function() {
			//console.log("Stepping right");
			sock.emit("action", {type: "move", direction: right});
		},
		rotateright: function() {
			forward = right;
			right = rotR[forward];
			ok();
		},
		stepfwd: function() {
			//console.log("Stepping forward");
			sock.emit("action", {type: "move", direction: forward});
		},
		repeat: ok,
		rotateleft: function() {
			right = forward;
			forward = rotL[right];
			ok();
		}
	}
};

function ok() {
	FSM.state = FSM.transitions[FSM.state].ok;
	FSM.functions[FSM.state]();
}

function err() {
	FSM.state = FSM.transitions[FSM.state].err;
	FSM.functions[FSM.state]();
}

let maze = [];
for(let i=0; i<20; i++) {
	maze[i] = [];
	for(let j=0; j<20; j++)
		maze[i][j] = {pos: {top: false, left: false, right: false, bottom: false}}
}

let laststatus = {pos: {top: true, left: true, right: true, bottom: true}};
let first = true;
function status(status) {
	if(first) {
		console.log("fs")
		first = false;
		ok();
		return;
	}
	let s
	if((typeof status) === "string")
		s = JSON.parse(status);
	else {
		console.log(">>>>>>>>>>>>>>>>>>")
		s = status;
	}

	if(s.level > laststatus.level && s.level > 0) {
		for(let i=0; i<20; i++) {
			maze[i] = [];
			for(let j=0; j<20; j++)
				maze[i][j] = {pos: {top: false, left: false, right: false, bottom: false}}
		}
		console.dir(s);
		///while(1) {
		//	let a =1+3;
		//}
	}
	if(s.level < laststatus.level && laststatus.level > 0) {
		shit();
	}
	laststatus = s
	let x = laststatus.pos.x
	let y = laststatus.pos.y
	let l = laststatus.level

	maze[y][x] = s
	display(maze)

	console.log(x, y, l)
	ok()
}

function shit() {
	const reverse = [, 3, 4, 1, 2];
	sock.emit("action", {type: "move", direction: reverse[forward]});
	right = forward;
	forward = rotL[right];
}

function error_message(error_message) {
	err()
}

function message(message) {
	console.log("[MESSAGE] " + JSON.stringify(message));
	while(1){}
}

function display(maze) {
	let o = ""
	for(let i=0; i<20; i++) {
		for(let j=0; j<20; j++) {
			o += "+"
			if(maze[i][j].pos.top) {
				o += "-";
			} else {
				o += " ";
			}
		}
		o += "\n"
		for(let j=0; j<20; j++) {
			if(maze[i][j].pos.left) {
				o += "|";
			} else {
				o += " ";
			}
			o += " ";
		}
		o += "\n"
	}
	o += "\n\n\n"
	console.log(o)
}

const sock = io.connect("ws://45.76.95.55:8080");

sock.on("status", status);
sock.on("error_message", error_message);
sock.on("message", message);

function interactive() {

}
