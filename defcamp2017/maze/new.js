// New solver

"use strict";

const io = require("socket.io-client");
const sock = io.connect("ws://45.76.95.55:8031");

sock.on("status", handleStatus);
sock.on("error_message", handleError);
sock.on("message", handleFlag);

let dir_fwd = 1;

sock.emit("action", {type: "move", direction: dir_fwd});

const turnL = [, 4, 1, 2, 3],
	turnR = [, 2, 3, 4, 1],
	reverse = [, 3, 4, 1, 2],
	dirS = [, "left", "up", "right", "down"];

let levels = [];
for(let l=0; l<5; l++) {
	levels[l] = [];
	for(let i=0; i<20; i++) {
		levels[l][i] = [];
		for(let j=0; j<20; j++)
			levels[l][i][j] = [, false, false, false, false];
	}
}

let firstStatus = true;
function handleStatus(status) {
	if(firstStatus) {
		firstStatus = false;
		return;
	}
	phase1 = false;
	if(typeof status === "string") {
		status = JSON.parse(status);
	}

	console.dir(status);
	const x = status.pos.x,
		y = status.pos.y,
		level = status.level,
		left = status.pos.left,
		right = status.pos.right,
		up = status.pos.top,
		down = status.pos.bottom,
		stairs = status.pos.stairs;

	if(stairs || level == 1) {
		console.log(">>>> ?? <<<<");
		while(1){}
		sock.emit("action", {type: "move", direction: dir_fwd});
		sock.emit("action", {type: "move", direction: reverse[dir_fwd]});
	}

	const walls = [, left, up, right, down];
	levels[level][y][x] = [, left, up, right, down];
	// Turn right if there's no right wall
	if(!walls[turnR[dir_fwd]]) {
		dir_fwd = turnR[dir_fwd];
	}

	// Turn left on a dead end
	while(walls[dir_fwd]) {
		dir_fwd = turnL[dir_fwd];
	}

	console.log(x, y, level)

	console.log(dirS[dir_fwd])
	console.log(drawMaze(levels[level]))
	sock.emit("action", {type: "move", direction: dir_fwd});
}

let phase1 = true;
function handleError(error) {
	if(phase1) {
		dir_fwd = turnR[dir_fwd];
		sock.emit("action", {type: "move", direction: dir_fwd});
		return;
	} else {
		console.log("[ERROR]");
		console.dir(error);
		console.log(error);
	}
}

function handleFlag(flag) {
	console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	console.dir(flag);
	console.log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	while(1);
}

function drawMaze(level) {
	let r = "";
	for(let i=0; i<20; i++) {
		for(let j=0; j<20; j++) {
			r += "+";
			if(level[i][j][2])
				r += "-";
			else
				r += " ";
		}
		r += "+\n";
		for(let j=0; j<20; j++) {
			if(level[i][j][1])
				r += "|";
			else
				r += " ";
			r += " ";
		}
		if(level[i][19][3])
			r += "|";
		r += "\n";
	}
	for(let j=0; j<20; j++) {
		r += "+";
		if(level[19][j][4])
			r += "-";
		else
			r += " ";
	}
	r += "+"
	return r;
}
