Crypto Luck

simple PoW

```py
import hashlib
from pwn import *

r = remote("37.128.230.46", 50041)
context.log_level = 'debug'

for k in range(10):
	prefix = r.recvline().strip()
	print "prefix:", repr(prefix)
	for i in xrange(256*256*256*256):
		s = str(i)
		if hashlib.sha1(s).hexdigest()[:6] == prefix:
			print "FOUND: ", s, hashlib.sha1(s).hexdigest()[:6]
			r.sendline(s)
			break

r.interactive()
```

Alice

First half - the only way for the attacker to get all 32 nibbles with 16 queries was if they queried each nibble once. Freq and pick the one that only appears once
Second half - no queries here => pick the one that doesn't appear

Online encryption

txtwizard.net/crypto/AES/encrypt/CBC/PKCS5 requests
plainText=you+got+the+ideea&key=asdfsfdbgferwtrwdfg&iv=fASGQFEQRFASDGS&token=03AOLTBL.......


plainText=you+got+the+ideea
plainText=you+got+the+ideea
plainText=you+got+the+ideea
plainText=UlBGUHtxcTU0NX
plainText=NvczEyc3E2MDhx
plainText=bm44cDIwMXM1MH
plainText=M5NXA4NTIwb3Jw
plainText=OXM3NDRuMzU3M2
plainText=8xcXAwb3A1M3By
plainText=MDE5NzI2fQ%3D%3D
plainText=he+he+he+%3A)
plainText=UlBGUHtxcTU0NXNvc
plainText=zEyc3E2MDhxbm44cD
plainText=IwMXM1MHM5NXA4NTI
plainText=wb3JwOXM3NDRuMzU3
plainText=M28xcXAwb3A1M3ByM%0ADE5NzI2fQ%3D%3D

Piet Mondrian

After JPEG file are 3 PNGs that are piet programs and each output
piet1.png
ECSC{e647c19e4fc7838bf7
piet2.png
64abbdcb0c1f08adca163cd
piet3.png
adfb889bee5201fc4397e5d}

Secure Code

Ctrl+U to see the actual morse code (space rendering bleh)
Translates to `well done how about binary stegano 818181`
Looking at the binary there are many seemingly identical for loops with 11, respectly 12 iterations.

Interpreting an 11 for loop as a 1 and a 12 for loop as a 0 we get his binary string:

01000101010000110101001101000011011110110011010000110010001100100011100000110010001101000011000000110010011001100011001000110111011000100110011000111000001100110011011000110101001100010011011100110111001101110011010000110111001101010011000100110000001101000110001101100110001100010011001000110001011001010011011100110011001101110110001000110111001110010110001000110110001110000011001001100100001101110011010000110000001101010011100000111000001100000110000101100100011001100011100000110100001101000011011101100101011001100011001000110010011000100110000101111101

ECSC{42282402f27bf83651777475104cf121e737b79b682d7405880adf8447ef22ba}

solfa sol fa sol

solfa cipher
place in audacity
use the playlist to match the notes
realise rhythm also matters
create rhythm track
count

ciphertext is S1 R3 F1 R3 M3 S1 L3 L3 T1 S4 T1 D3 L3 S3 L3 S1 D1 F1 D3 T1 R3 D2 M1 D1 D1 M3 S1 D4 R1 L1 M1 L3 F1 S1 R3 F1 R3

https://www.wmich.edu/mus-theo/solfa-cipher/secrets/

ECSCHELLOWORLDLETSROCKATTHEFINALSECSC

