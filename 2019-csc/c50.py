import hashlib
from pwn import *

r = remote("37.128.230.46", 50041)
context.log_level = 'debug'

for round in range(10):
    prefix = r.recvline().strip()
    print "prefix:", repr(prefix)
    for i in xrange(256*256*256*256):
        s = str(i)
        if hashlib.sha1(s).hexdigest()[:6] == prefix:
            print "FOUND: ", s, hashlib.sha1(s).hexdigest()[:6]
            r.sendline(s)
            break

print r.recvall(timeout=3)
