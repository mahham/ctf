import os
import marshal
import struct
import zlib

pyver = 36

def _extractPyz(name):
    dirName =  name + '_extracted'
    # Create a directory for the contents of the pyz
    if not os.path.exists(dirName):
        os.mkdir(dirName)

    with open(name, 'rb') as f:
        pyzMagic = f.read(4)
        assert pyzMagic == b'PYZ\0' # Sanity Check

        pycHeader = f.read(4) # Python magic value

        if '' != pycHeader:
            print('[!] Warning: The script is running in a different python version than the one used to build the executable')
            print('    Run this script in Python{0} to prevent extraction errors(if any) during unmarshalling'.format(pyver))

        (tocPosition, ) = struct.unpack('!i', f.read(4))
        f.seek(tocPosition, os.SEEK_SET)

        try:
            toc = marshal.load(f)
        except:
            print('[!] Unmarshalling FAILED. Cannot extract {0}. Extracting remaining files.'.format(name))
            return

        print('[*] Found {0} files in PYZ archive'.format(len(toc)))

        # From pyinstaller 3.1+ toc is a list of tuples
        if type(toc) == list:
            toc = dict(toc)

        for key in toc.keys():
            (ispkg, pos, length) = toc[key]
            f.seek(pos, os.SEEK_SET)
            print("{}: [{}] pkg, pos {}, len {}".format(key, " X"[ispkg], pos, length))

            fileName = key
            try:
                # for Python > 3.3 some keys are bytes object some are str object
                fileName = key.decode('utf-8')
            except:
                pass

            # Make sure destination directory exists, ensuring we keep inside dirName
            destName = os.path.join(dirName, fileName.replace("..", "__"))
            destDirName = os.path.dirname(destName)
            if not os.path.exists(destDirName):
                os.makedirs(destDirName)

           # try:
            data = f.read(length)
            data = zlib.decompress(data)
            #data = marshal.loads(data)
           # except:
           #     print('[!] Error: Failed to decompress {0}, probably encrypted. Extracting as is.'.format(fileName))
           #     open(destName + '.pyc.encrypted', 'wb').write(data)
           #     continue

            with open(destName + '.pyc', 'wb') as pycFile:
                pycFile.write(pycHeader)      # Write pyc magic
                pycFile.write(b'\0' * 4)      # Write timestamp
                if pyver >= 33:
                    pycFile.write(b'\0' * 4)  # Size parameter added in Python 3.3
                pycFile.write(data)

import sys
_extractPyz(sys.argv[1])
