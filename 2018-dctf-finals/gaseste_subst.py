from pwn import *

substitution = {}

for c in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890":
	r = process("./encoder")
	r.sendline("AAAAAA{}A".format(c))
	resp = hex(int(float(r.recvline())))
	print resp
	substitution[c] = chr(int("0x" + resp[2:4], 16))

print substitution

