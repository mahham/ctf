from pwn import *

def test_diff(a, b):
	r.sendline(a)
	ac = int(float(r.recvline()))
	r.sendline(b)
	bc = int(float(r.recvline()))
	return ac ^ bc

thing = "DCTFflag"
"""
for i in range(7):
	for bit in range(7):
		log.log_level = "ERROR"
		r = process("./encoder")
		thingy = [ord(a) for a in thing]
		thingy[i] ^= 1 << bit
		thingy = "".join(chr(a) for a in thingy)
		print thingy
		diff = test_diff(thing, thingy)
		r.close()
		which = bin(diff).count('0')
		print "char {} bit {} -> bit {}".format(i, bit, which)

for last in range(ord('a'), ord('z')):
	r = process("./encoder")
	r.sendline("DCTFfla"+chr(last))
	print last, bin(int(float(r.recvline()))).count('0')
"""

for i in range(-1, 7):
	r = process("./encoder")
	thingy = [ord(a) for a in thing]
	if i > -1:
		thingy[i] = ord("A")
	thingy = "".join(chr(a) for a in thingy)
	r.sendline(thingy)
	print thingy, bin(int(float(r.recvline())))


r = process("./encoder")
r.sendline("ABCDEFGH")
print hex(int(float(r.recvline())))
r.sendline("ABCDEFGG")
print hex(int(float(r.recvline())))
r.sendline("ABCDEGGG")
print hex(int(float(r.recvline())))
r.sendline("IJKLMNOP")
print hex(int(float(r.recvline())))
r.sendline("QRSTUVWX")
print hex(int(float(r.recvline())))
r.sendline("YZABCDEF")
print hex(int(float(r.recvline())))