[bits 32]

extern printf
global main

section .text

main:
	push 0xaee1f319
	push 0xe0911505
	push 0xfac0f685
	call asm3
	push fmt
	call printf
	
	ret

asm3:
	push   	ebp
	mov    	ebp,esp
	mov	eax,0x27
	xor	al,al
	mov	ah, [ebp+0xb]
	sal	ax,0x10
	sub	al, [ebp+0xc]
	add	ah, [ebp+0xf]
	xor	ax, [ebp+0x12]
	mov	esp, ebp
	pop	ebp
	ret

section .data
fmt: db "%d"