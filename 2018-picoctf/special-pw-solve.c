#include <stdio.h>

unsigned int __ROR4__(unsigned int x, int n) {
	return (x >> n) | (x << (32 - n));
}

unsigned short __ROL2__(unsigned short x, int n) {
	return (x << n) | (x >> (16 - n));
}

char ciphered_data[] = {
	123, 24, 166, 54, 218, 59, 43, 166, 254, 203, 130, 174,
	150, 255, 159, 70, 143, 54, 167, 175, 254, 147, 142,
	63, 70, 167, 255, 130, 207, 206, 179, 151, 23, 26, 167,
	54, 239, 43, 138, 237, 0
};

int main() {
	for(int j=36; j>=0; j--) {
		void *start = &ciphered_data[j];

		*(unsigned int *)start = __ROR4__(*(unsigned int *)start, 11);
		*(unsigned short *)start = __ROL2__(*(unsigned short *)start, 5);
		*((char*) start) ^= 0x9D;
	}
	puts(ciphered_data);
	return 0;
}
