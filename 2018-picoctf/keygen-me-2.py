from z3 import *

s = Solver()

flag = [Int('f{}'.format(i)) for i in range(16)]

for f in flag:
	s.add(f >= 0)
	s.add(f < 36)

s.add((flag[0] + flag[1]) % 36 == 14)
s.add((flag[2] + flag[3]) % 36 == 24)
s.add((flag[2] - flag[0]) % 36 == 6)
s.add((flag[1] + flag[3] + flag[5]) % 36 == 4)
s.add((flag[2] + flag[4] + flag[6]) % 36 == 13)
s.add((flag[3] + flag[4] + flag[5]) % 36 == 22)
s.add((flag[6] + flag[8] + flag[10]) % 36 == 31)
s.add((flag[4] + flag[7] + flag[1]) % 36 == 7)
s.add((flag[9] + flag[12] + flag[15]) % 36 == 20)
s.add((flag[13] + flag[14] + flag[15]) % 36 == 12)
s.add((flag[8] + flag[9] + flag[10]) % 36 == 27)
s.add((flag[7] + flag[12] + flag[13]) % 36 == 23)


print s.check()
m = s.model()
print m

print ''.join(["0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"[int(repr(m[f]))] for f in flag])
