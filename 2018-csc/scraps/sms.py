<<<<<<< HEAD
import string, sys

"""
Rezolvarea problemei asteia a fost plina de informatii misleading,
incepand de la definitia vaga si incorecta din enunt si pana la
indicatiile primite de la organizatori:

"de ce y1(0) este 72 adica H, cand de fapt este 4 adica 52?"

Adica initial am crezut ca y-urile sunt hex dar dupa mesajul ala
am zis ca poate e doar o f mare coincidenta ca s-a ajuns la [0-9A-F]
si trebuie sa le tratez ca stringuri de lungime 30 ascii in loc de
stringuri de lungime 15 hex. Aia clar n-a mers ca dupa nu ieseau
w-urile lowercase pentru ca ieseau niste w-uri TOT IN HEXA, as ooposed
to ce scrie in enunt.

F tarziu am mai incercat odata cu ideea ca iau y-urile ca hex
si ca x nu e facut din orice litera mare + cifra, ci doar din
[0-9A-F]. Well guess what? Au inceput sa apara rezultate ok.
Mai jos in fisier am scris um am facut recuperarea flagului
cate trei caractere deodata.

Am fost si tare dezamagit sa caut flag-ul ca cica era undeva pe net si
sa vad ca e copiata mot-a-mot problema de altundeva. Nici macar alte
w-uri, mai? chiar asa..?

"""

y = ["",
	"4840DDEB8C1CBAC721F824949FCF9A",
	"4840D5EA8E1BBACB36F72C9C94D59D",
	"484FD9EB9305A2C63FF92A909DD790",
	"4A4CD3F48018BFCC3BFC208598DE9A",
	"4A4CD0E28808B3C026F9289D98CF90",
	"4A4CCBEA9509A4CB2AF124819DDE9A",
	"4D4ACDE5911CB9C73CE4209F96D790"]

n = len(y[1])

valid_x_chars = "".join([chr(i) for i in range(256)])
valid_w_chars = string.lowercase

possible_x = [valid_x_chars for i in range(len(y[1].decode("hex")))]

# For each ciphertext ct
for i in range(1, 8):
	ct = y[i].decode("hex")
	print ct

	# For each position in the ciphertext
	for j in range(len(ct)):
		bad_values = []
		# Test each possible x value
		for k in range(len(possible_x[j])):
			decrypted = chr(ord(possible_x[j][k]) ^ ord(ct[j]))
			if decrypted not in valid_w_chars:
				bad_values += [possible_x[j][k]]

		# And delete the ones that don't lead to valid decryptions
		for c in bad_values:
			possible_x[j] = possible_x[j].replace(c, "")

print("Possible characters for all positions of x:")
print(possible_x)

"""
Am incercat primele 3 caractere ca sa gasesc cel mai englezesc rezultat.
Ca sa termin mai repede am inceput sa sterg liniile cu diftongi probabil imposibili ('jx', kk', 'kj', 'kg', 'lk')
si liniile care au cuvinte care incep cu 3 consoane /[bcdfghjklmnpqrstvwxyz]{3}/

for x0 in possible_x[0]:
	for x1 in possible_x[1]:
		for x2 in possible_x[2]:
			X = x0+x1+x2
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(3)]) + " ")

Cea mai englezeasca varianta a fost:
2923be -> acc ack alg com con cou dis 

"""




"""
Dupa urmatoarele 3 caractere.
Tot au fost f multe variante asa ca am scos toate cuvintele care contin 4
consoane (in engleza pare sa fie posibil sa fie 3 consecutive dar poate nu la inceput??)

for x3 in possible_x[3]:
	for x4 in possible_x[4]:
		for x5 in possible_x[5]:
			X = "\x29\x23\xbe"+x3+x4+x5
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(6)]) + " ")

# 2923be84e16c -> accomp acknow algori compat confid counte disapp
"""




"""
Si tot asa... Deja acum stiam dupa ce sa ma uit asa ca nu am mai sters

for x6 in possible_x[6]:
	for x7 in possible_x[7]:
		for x8 in possible_x[8]:
			X = "\x29\x23\xbe\x84\xe1\x6c"+x6+x7+x8
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(9)]) + " ")

# 2923be84e16cd6ae52 -> accomplis acknowled algorithm compatibi confident counterex disappoin 
"""




"""
for x9 in possible_x[9]:
	for x10 in possible_x[10]:
		for x11 in possible_x[11]:
			X = "\x29\x23\xbe\x84\xe1\x6c\xd6\xae\x52"+x9+x10+x11
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(12)]) + " ")

# 2923be84e16cd6ae529049f1 -> accomplishme acknowledgem algorithmica compatibilit confidential counterexamp disappointin 
"""



for x12 in possible_x[12]:
	for x13 in possible_x[13]:
		for x14 in possible_x[14]:
			X = "\x29\x23\xbe\x84\xe1\x6c\xd6\xae\x52\x90\x49\xf1"+x12+x13+x14
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(15)]) + " ")

# 2923be84e16cd6ae529049f1f1bbe9 -> accomplishments acknowledgement algorithmically compatibilities confidentiality counterexamples disappointingly 

# Flag is ECSC{2923BE84E16CD6AE529049F1F1BBE9}
# This challenge was terribly misleading
=======
import string, sys

"""
Rezolvarea problemei asteia a fost plina de informatii misleading,
incepand de la definitia vaga si incorecta din enunt si pana la
indicatiile primite de la organizatori:

"de ce y1(0) este 72 adica H, cand de fapt este 4 adica 52?"

Adica initial am crezut ca y-urile sunt hex dar dupa mesajul ala
am zis ca poate e doar o f mare coincidenta ca s-a ajuns la [0-9A-F]
si trebuie sa le tratez ca stringuri de lungime 30 ascii in loc de
stringuri de lungime 15 hex. Aia clar n-a mers ca dupa nu ieseau
w-urile lowercase pentru ca ieseau niste w-uri TOT IN HEXA, as ooposed
to ce scrie in enunt.

F tarziu am mai incercat odata cu ideea ca iau y-urile ca hex
si ca x nu e facut din orice litera mare + cifra, ci doar din
[0-9A-F]. Well guess what? Au inceput sa apara rezultate ok.
Mai jos in fisier am scris um am facut recuperarea flagului
cate trei caractere deodata.

Am fost si tare dezamagit sa caut flag-ul ca cica era undeva pe net si
sa vad ca e copiata mot-a-mot problema de altundeva. Nici macar alte
w-uri, mai? chiar asa..?

"""

y = ["",
	"4840DDEB8C1CBAC721F824949FCF9A",
	"4840D5EA8E1BBACB36F72C9C94D59D",
	"484FD9EB9305A2C63FF92A909DD790",
	"4A4CD3F48018BFCC3BFC208598DE9A",
	"4A4CD0E28808B3C026F9289D98CF90",
	"4A4CCBEA9509A4CB2AF124819DDE9A",
	"4D4ACDE5911CB9C73CE4209F96D790"]

n = len(y[1])

valid_x_chars = "".join([chr(i) for i in range(256)])
valid_w_chars = string.lowercase

possible_x = [valid_x_chars for i in range(len(y[1].decode("hex")))]

# For each ciphertext ct
for i in range(1, 8):
	ct = y[i].decode("hex")
	print ct

	# For each position in the ciphertext
	for j in range(len(ct)):
		bad_values = []
		# Test each possible x value
		for k in range(len(possible_x[j])):
			decrypted = chr(ord(possible_x[j][k]) ^ ord(ct[j]))
			if decrypted not in valid_w_chars:
				bad_values += [possible_x[j][k]]

		# And delete the ones that don't lead to valid decryptions
		for c in bad_values:
			possible_x[j] = possible_x[j].replace(c, "")

print("Possible characters for all positions of x:")
print(possible_x)

"""
Am incercat primele 3 caractere ca sa gasesc cel mai englezesc rezultat.
Ca sa termin mai repede am inceput sa sterg liniile cu diftongi probabil imposibili ('jx', kk', 'kj', 'kg', 'lk')
si liniile care au cuvinte care incep cu 3 consoane /[bcdfghjklmnpqrstvwxyz]{3}/

for x0 in possible_x[0]:
	for x1 in possible_x[1]:
		for x2 in possible_x[2]:
			X = x0+x1+x2
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(3)]) + " ")

Cea mai englezeasca varianta a fost:
2923be -> acc ack alg com con cou dis 

"""




"""
Dupa urmatoarele 3 caractere.
Tot au fost f multe variante asa ca am scos toate cuvintele care contin 4
consoane (in engleza pare sa fie posibil sa fie 3 consecutive dar poate nu la inceput??)

for x3 in possible_x[3]:
	for x4 in possible_x[4]:
		for x5 in possible_x[5]:
			X = "\x29\x23\xbe"+x3+x4+x5
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(6)]) + " ")

# 2923be84e16c -> accomp acknow algori compat confid counte disapp
"""




"""
Si tot asa... Deja acum stiam dupa ce sa ma uit asa ca nu am mai sters

for x6 in possible_x[6]:
	for x7 in possible_x[7]:
		for x8 in possible_x[8]:
			X = "\x29\x23\xbe\x84\xe1\x6c"+x6+x7+x8
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(9)]) + " ")

# 2923be84e16cd6ae52 -> accomplis acknowled algorithm compatibi confident counterex disappoin 
"""




"""
for x9 in possible_x[9]:
	for x10 in possible_x[10]:
		for x11 in possible_x[11]:
			X = "\x29\x23\xbe\x84\xe1\x6c\xd6\xae\x52"+x9+x10+x11
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(12)]) + " ")

# 2923be84e16cd6ae529049f1 -> accomplishme acknowledgem algorithmica compatibilit confidential counterexamp disappointin 
"""



for x12 in possible_x[12]:
	for x13 in possible_x[13]:
		for x14 in possible_x[14]:
			X = "\x29\x23\xbe\x84\xe1\x6c\xd6\xae\x52\x90\x49\xf1"+x12+x13+x14
			print ""
			sys.stdout.write(X.encode("hex") + " -> ")
			for i in range(1, 8):
				ct = y[i].decode("hex")
				sys.stdout.write("".join([chr(ord(X[j]) ^ ord(ct[j])) for j in range(15)]) + " ")

# 2923be84e16cd6ae529049f1f1bbe9 -> accomplishments acknowledgement algorithmically compatibilities confidentiality counterexamples disappointingly 

# Flag is ECSC{2923BE84E16CD6AE529049F1F1BBE9}
# This challenge was terribly misleading
>>>>>>> master
