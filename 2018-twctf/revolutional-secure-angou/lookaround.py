import Crypto.Util.number as N
from math import *

while True:
	p = N.getPrime(1024)
	e = 65537
	q = N.inverse(e, p)
	if N.isPrime(q) == False:
		continue

	t = (p-1) * (q-1) // N.GCD(p-1, q-1)
	d = N.inverse(e, t)

	print("---------- PAIR:")
	print N.GCD(p-1, q-1)
	print "p = " + hex(p)
	print "q = " + hex(q)
	print "d = " + hex(d)
	print "t = " + hex(t)
	print "log t = " + str(log(t))
	print "log n = " + str(log(p*q))