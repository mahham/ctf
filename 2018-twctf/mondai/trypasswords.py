from zipfile import ZipFile
import sys

assert(len(sys.argv) == 3)

passwordList = open(sys.argv[1], "r").read().split("\n")

with ZipFile(sys.argv[2], "r") as myzip:
    zipFilenames = myzip.namelist()
    i = 0
    for password in passwordList:
        password = password
        i += 1
        if i % 100000 == 0:
            print i
        try:
            for filename in zipFilenames:
                myzip.open(filename, mode="r", pwd=password)
        except: # Bad password alltogether
            continue
        try:
            myzip.read(zipFilenames[0], pwd=password)
        except: # Bad CRC
            continue
        print '"{}"'.format(password)


