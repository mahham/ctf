from pwn import *

def request(plaintext):
	r.recvuntil("message: ")
	r.sendline(plaintext)
	r.recvuntil("ciphertext: ")
	return r.recvline()[:-1]

r = remote("crypto.chal.ctf.westerns.tokyo", 14791)
requestcount = 0
r.recvuntil("encrypted flag: ")
encflag = r.recvline().strip()
print("Target flag: \n" + encflag)

alphabet = {}
def alphabet_should_be(letter=None, val=None):
	if letter in alphabet:	# Have we added this letter earlier?
		assert(alphabet[letter] == val)	# Yes: Check if it's the same as previously observed
	else:
		alphabet[letter] = val	# No: Add it

# For the style points
def progressbar(progress, text):
	width = 30
	filled = int(progress*width)
	line = "[" + ("#"*filled) + ("-"*(width-filled)) + "] " + str(text)
	print(line + "\033[F")

print("<SPACE FOR PROGRESSBAR>")

for i in range(57, 120):
	progressbar(float(i-57)/(119-57), i-57)
	alphabet_should_be(letter = request(chr(i))[-1], val = i % 59)
print("\n")

plaintext_as_num = 0
for c in encflag:
	plaintext_as_num = plaintext_as_num * 59 + alphabet[c]

plaintext = ""
while plaintext_as_num > 0:
	plaintext += chr(plaintext_as_num % 256)
	plaintext_as_num //= 256

print(plaintext[::-1])	# Reverse string
