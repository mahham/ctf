from base58 import b58decode, b58encode
from hashlib import sha256
from pwn import *

def POW(nc):
	nc.send("Y\n")
	nc.recvuntil("starts with ")
	prefix = nc.recvuntil(":")[:-1]
	nc.recv()
	print(prefix)
	partial = prefix
	while len(b58decode(partial)) < 25:
		partial += "a"
	extripmd = b58decode(partial)[:21]
	checksum = sha256(sha256(extripmd).digest()).digest()[:4]
	addr = b58encode(extripmd + checksum)
	print(addr)
	nc.sendline(addr)

nc = remote("66.172.27.77", 11019)

POW(nc)

nc.interactive()
