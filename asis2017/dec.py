#!/usr/bin/python

import random

KEY = 'musZTXmxV58UdwiKt8Tp'

def xor_str(x, y):
    if len(x) > len(y):
        return ''.join([chr(ord(z) ^ ord(p)) for (z, p) in zip(x[:len(y)], y)])
    else:
        return ''.join([chr(ord(z) ^ ord(p)) for (z, p) in zip(x, y[:len(x)])])

FLAG = open("flag.enc", "rb").read()

# FLAG.encode('hex')
flag, key = FLAG, KEY.encode('hex')
enc = xor_str(key * (len(flag) // len(key) + 1), flag).encode('hex')

ef = open('flag.txt', 'w')
ef.write(enc.decode('hex').decode('hex'))
ef.close()