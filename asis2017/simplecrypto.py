KEY = 'musZTXmxV58UdwiKt8Tp'

def xor_str(x, y):
    if len(x) > len(y):
        return ''.join([chr(ord(z) ^ ord(p)) for (z, p) in zip(x[:len(y)], y)])
    else:
        return ''.join([chr(ord(z) ^ ord(p)) for (z, p) in zip(x, y[:len(x)])])

encoded_flag = open("flag.enc", "r").read()
key = KEY.encode("hex")

decoded = xor_str(key * (len(encoded_flag) // len(key) + 1), encoded_flag).decode("hex")

df = open("flag.decoded", "w")
df.write(decoded)
df.close()
