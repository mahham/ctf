import random

n = 16490137037403853767897508852305066342580511824398314160223915469571789546851028762684175939606260197963549348930758783554687162186916836289041741437820180152880835047278837660608718533350130769982238498080587182844456607215216035243757999722352488869340324674811287906276649536734908380643537669230925410769

def gcd(a, b):
	while b != 0:
		r = a % b
		a = b
		b = r
	return a

def brent(N):
        if N%2==0:
                return 2
        y,c,m = random.randint(1, N-1),random.randint(1, N-1),random.randint(1, N-1)
        g,r,q = 1,1,1
        while g==1:             
                x = y
                for i in range(r):
                        y = ((y*y)%N+c)%N
                k = 0
                while (k<r and g==1):
                        ys = y
                        for i in range(min(m,r-k)):
                                y = ((y*y)%N+c)%N
                                q = q*(abs(x-y))%N
                        g = gcd(q,N)
                        k = k + m
                r = r*2
        if g==N:
                while True:
                        ys = ((ys*ys)%N+c)%N
                        g = gcd(abs(x-ys),N)
                        if g>1:
                                break
         
        return g    

while n != 1:
	factor = brent(n)
	print(factor)
	n //= factor
