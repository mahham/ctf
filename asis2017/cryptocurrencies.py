from base58 import b58encode, b58decode
from hashlib import sha256
import socket, re
from pwn import *

"""
alphabet = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
def correct(bad_addr):
	eripemd_i = 0
	limit = 256 ** 22p
	for c in bad_addr:
		if eripemd_i >= limit:
			break
		eripemd_i = eripemd_i * 58 + alphabet.index(c)
	eripemd_i = eripemd_i % limit

	s = b""
	while eripemd_i > 0:
		s = chr(eripemd_i % 256) + s
		eripemd_i /= 256

	s += sha256(sha256(s).digest()).digest()[:4]

	return b58encode(s)
nc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
nc.connect(("178.62.22.245", 58901))
nc.recv(4096)
nc.send("Y".encode())
nc.recv(4096)
"""

def POW(nc):
	nc.recv()
	nc.sendline("Y")
	nc.recvuntil("starts with ")
	prefix = nc.recvuntil(":")[:-1]
	partial = prefix
	while len(b58decode(partial)) < 25:
		partial += "a"
	partial = b58encode(b58decode(partial)[:25])
	eripemd = b58decode(partial)[:21]
	checksum = sha256(sha256(eripemd).digest()).digest()[:4]
	addr = b58encode(eripemd + checksum)
	nc.sendline(addr)

def repair(bad):
	extripmd = b58decode(bad)[:21]
	checksum = sha256(sha256(extripmd).digest()).digest()[:4]
	addr = b58encode(extripmd + checksum)
	return addr

nc = remote("178.62.22.245", 58901)
POW(nc)

while True:
	print(nc.recvuntil("address is: "))
	bad_address = nc.recvline(keepends=False)
	print("Bad address:       " + bad_address)
	good_address = repair(bad_address)
	print("Corrected address: " + good_address)
	nc.send(good_address)
	nc.interactive()
