def gcd(x, y):
	while y != 0:
		r = x % y
		x = y
		y = r
	return x

for n in range(1, 500):
	count = 0
	#print(n)
	for x in range(n//2):
		for y in range(x, n//2):
			if gcd(x, y) * n == x * y:
				count += 1

	formula = 1
	divisor = 1
	ncopy = n
	while ncopy != 1:
		divisor += 1
		exponent = 0
		while ncopy % divisor == 0:
			ncopy //= divisor
			exponent += 1
		f = 2 * exponent + 1
		if divisor == 2:
			f -= 1
		formula *= f

	formula = (formula + 1) // 2

	print("{}, {}, {}".format(n, count, formula))
