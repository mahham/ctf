import requests
import time

for vmajor in range(40, 41):
	for vminor in range(100):
		res = requests.get("https://kitkat.dctfq18.def.camp/main-v{}.{}.css.js".format(vmajor, str(vminor+100)[-2:]))
		if res.status_code >= 400 and res.status_code <= 499:
			print vmajor, vminor, res.status_code
			continue
		print res
		time.sleep(0.1)
