from pwn import *

# https://ja.wikipedia.org/wiki/Xorshift
def xorshift32(seed=2463534242):
	y = seed
	while True:
		y = (y ^ (y << 13)) & 0xffffffff
		y = (y ^ (y >> 17)) & 0xffffffff
		y = (y ^ (y << 15)) & 0xffffffff
		yield y

xs = xorshift32()

e = ELF("runme_f3abe874e1d795ffb6a3eed7898ddcbcd929b7be")

x = xor(e.read(e.symbols["flag"], 50), e.read(e.symbols["randval"], 50))
flag = ''
for i in range(50):
	flag += chr(ord(x[i]) ^ (xs.next() % 256))
print flag
