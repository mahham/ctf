from pwn import *
from re import *

r = remote("misc.chal.csaw.io", 9002)

# Skip the banner
r.recvuntil("*********")
r.recvline()

while True:
	task = r.recvline()
	print task
	print r.recvuntil("What does X equal?: ")

	eq1 = task.replace("=","-(")+")"
	c = eval(eq1,{"X":1j})

	result = 0
	if c.imag != 0:
		result = -c.real/c.imag

	r.sendline(str(result))
	print r.recvline()