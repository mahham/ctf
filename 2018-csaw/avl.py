from pwn import *
from pyavltree import AVLTree

r = remote("misc.chal.csaw.io", 9001)
r.recvline()
numbers = [int(x) for x in r.recvline().split(",")]
r.recvline()

tree = AVLTree(numbers)

r.sendline(",".join([str(x) for x in tree.preorder(tree.rootNode)]))
print r.recvall()