def tromino_translate(tromino, ioff, joff):
	return (
		(tromino[0][0] + ioff, tromino[0][1] + joff),
		(tromino[1][0] + ioff, tromino[1][1] + joff),
		(tromino[2][0] + ioff, tromino[2][1] + joff))


def point_rotate(n, pos):
	return (pos[1], n - pos[0] - 1)

def tromino_rotate(tromino, n):
	return (
		point_rotate(n, tromino[0]),
		point_rotate(n, tromino[1]),
		point_rotate(n, tromino[2]))

log2 = {1: 0, 2: 1, 4: 2, 8: 3, 16: 4, 32: 5, 64: 6}
corner_solutions = [[]]

# Precompute 2^i by 2^i squares where the bottom right corner is empty
for i in range(1, 6):
	n = 1 << i
	corner_solutions += [[]]

	# Top-left quadrant identical to n-1 solution
	corner_solutions[i] += corner_solutions[i-1]

	# Bottom-right quadrant identical to n-1 solution
	for tromino in corner_solutions[i-1]:
		corner_solutions[i] += [tromino_translate(tromino, n/2, n/2)]

	# Top-right quadrant is n-1 solution rotated 90deg clockwise
	for tromino in corner_solutions[i-1]:
		tromino = tromino_rotate(tromino, n/2)
		tromino = tromino_translate(tromino, 0, n/2)
		corner_solutions[i] += [tromino]

	# Bottom-left quadrant is n-1 solution rotated 90 deg counterclockwise = 3*90 deg clockwise
	for tromino in corner_solutions[i-1]:
		tromino = tromino_rotate(tromino, n/2)
		tromino = tromino_rotate(tromino, n/2)
		tromino = tromino_rotate(tromino, n/2)
		tromino = tromino_translate(tromino, n/2, 0)
		corner_solutions[i] += [tromino]

	# Middle piece
	corner_solutions[i] += [((n/2-1, n/2-1), (n/2-1, n/2), (n/2, n/2-1))]

def solve(n, i, j):
	if n == 1 and i == 0 and j == 0:
		return []

	quadrants = [
		# top, left, bottom, right, 90 degree rotations, inner corner relative to n/2,n/2
		(0, 0, n/2-1, n/2-1, 0, -1, -1),
		(n/2, 0, n, n/2-1,   3, 0, -1),
		(0, n/2, n/2-1, n,   1, -1, 0),
		(n/2, n/2, n, n,     2, 0, 0)
	]

	sol = []
	middle = ()
	for q in quadrants:
		if i >= q[0] and i <= q[2] and j >= q[1] and j <= q[3]:
			# Quadrant contains the target point
			specialq = solve(n/2, i-q[0], j-q[1])
			for tromino in specialq:
				sol += [tromino_translate(tromino, q[0], q[1])]
		else:
			# Quadrant is one of the other 3
			middle = middle + ((n/2+q[5], n/2+q[6]),)
			for tromino in corner_solutions[log2[n/2]]:
				for k in range(q[4]):
					tromino = tromino_rotate(tromino, n/2)
				tromino = tromino_translate(tromino, q[0], q[1])
				sol += [tromino]

	sol += [middle]

	return sol

from pwn import *
r = remote("misc.chal.csaw.io", 9000)
r.recvuntil("marked block: (")
i = int(r.recvuntil(", ")[:-2])
j = int(r.recvuntil(")")[:-1])

for tromino in solve(64, i, j):
	r.sendline("({},{}),({},{}),({},{})".format(
		tromino[0][0], tromino[0][1],
		tromino[1][0], tromino[1][1],
		tromino[2][0], tromino[2][1]
	))

r.interactive()
# flag{m@n_that_was_sup3r_hard_i_sh0uld_have_just_taken_the_L}
