from z3 import *

s = Solver()

inp = [None] * 70
for i in range(70):
	inp[i] = BitVec("inp[{}]".format(i), 8)
	#s.add(inp[i] >= 32)
	#s.add(inp[i] <= ord('z'))

virginia = [ord(c) for c in "Almost heaven west virginia, blue ridge mountains"]
correct_enc = [ord(c) for c in "\xD6\x4D\x2D\x85\x77\x97\x60\x62\x2B\x88\x86\xCA\x72\x97\xEB\x89\x98\xF3\x78\x26\x83\x29\x5E\x27\x43\xFB\xB8\x17\x7C\xCE\x3A\x73\xCF\xFB\xC7\x9C\x60\xAF\x9C\xC8\x75\xCD\x37\x7B\x3B\x9B\x4E\xC3\xDA\xD8\xCE\x71\x2B\x30\x68\x46\x0B\xFF\x3C\xF1\xF1\x45\xC4\xD0\xC4\xFF\x51\xF1\x88\x51"]

print virginia
print correct_enc


for i in range(10):
	for j in range(7):
		a = 0
		for k in range(7):
			a = (a + inp[i*7+k] * virginia[k*7+j]) % 256
		s.add(a == correct_enc[i*7+j])

print s.check()

m = s.model()
print m
flag = ''
for i in range(70):
	flag += chr(int(str(m[inp[i]])))

print flag
