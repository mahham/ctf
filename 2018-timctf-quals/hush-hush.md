<<<<<<< HEAD
[Go back](.)

### Hush hush - 150 points

This challenge consisted in finding a collision for the given hash algorithm. The algorithm is exploitable because of the way it converts the input strings to numbers, as any null bytes at the beginning are simply ignored:

```py
def my_hash(x):
    global n
    x += "piper"
    x = int(x.encode('hex'), 16) # <-- This will forget about any null bytes at the beginning
    for i in range(32):
        x = pow(x, x, n)
        x +=1
    m = md5.new()
    m.update(hex(x))
    return m.hexdigest()
```

By sending it two strings that are identical except that one has an additional zero byte at its beginning we can make the `my_hash` function return the same hash while still having the string equality check return false, thus obtaining the flag. To do this it is important that we flush the first string before sending the second one so that they do indeed get treated as separate. I did this by adding a `cat -` in between sending the strings to which I sent an EOF using Ctrl-D in order to flush the buffer.

```
$ (printf "whatever"; cat - ; printf "\x00whatever") | nc 89.38.210.129 6665
Hello stranger!
Nobody can break my hash but you are free to try.
First input: Second input: Thix will never be printed anyway, timctf{d0UbT_3verYTH1nG}
```

=======
[Go back](.)

### Hush hush - 150 points

This challenge consisted in finding a collision for the given hash algorithm. The algorithm is exploitable because of the way it converts the input strings to numbers, as any null bytes at the beginning are simply ignored:

```py
def my_hash(x):
    global n
    x += "piper"
    x = int(x.encode('hex'), 16) # <-- This will forget about any null bytes at the beginning
    for i in range(32):
        x = pow(x, x, n)
        x +=1
    m = md5.new()
    m.update(hex(x))
    return m.hexdigest()
```

By sending it two strings that are identical except that one has an additional zero byte at its beginning we can make the `my_hash` function return the same hash while still having the string equality check return false, thus obtaining the flag. To do this it is important that we flush the first string before sending the second one so that they do indeed get treated as separate. I did this by adding a `cat -` in between sending the strings to which I sent an EOF using Ctrl-D in order to flush the buffer.

```
$ (printf "whatever"; cat - ; printf "\x00whatever") | nc 89.38.210.129 6665
Hello stranger!
Nobody can break my hash but you are free to try.
First input: Second input: Thix will never be printed anyway, timctf{d0UbT_3verYTH1nG}
```

>>>>>>> master
