#include<stdio.h>
int main(){
	//3481913753 179825857 1498412593
	//2580122063 3253516042 838225753

	//2663942344 2381490502 951323984
	//3364407454 1185542797 1343075384

	setvbuf(stdout, NULL, _IONBF, NULL);

	unsigned long long k1 = 2580122063;
	unsigned long long k2 = 3253516042;
	unsigned long long k3 = 838225753;

	unsigned long long P = 3525886283;
/*
	for(long long i=0; i<100; i++) {
		printf("%lld ", i * 2115531770 % P);
		printf("%lld\n", i * 1007396081 % P);
	}*/

	printf("35 / 5 = %lld\n", (P+35L) * 2115531770L % P);
	printf("35 / 7 = %lld\n", 35L * 1007396081L % P);

	for(unsigned long long l2 = 0; l2 < P; l2++) {
		if(l2 % 0x10000000 == 0) {
			printf("%lld\n", l2);
		}
		unsigned long long r2 = 0xffffffff & (k2 ^ l2);
		unsigned long long l1 = ((l2 - 11L + P) * 2115531770L) % P;
		unsigned long long r1 = ((r2 - 13L + P) * 1007396081L) % P;

		if ((l1 ^ r1) == k1) {
			printf("%llx %llx %llx %llx\n", l1, r1, l2, r2);
		}
	}
	return 0;
}