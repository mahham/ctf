# Decode custom VM thingy

codef = open("code", "rb")
code = codef.read()
codef.close()

position = 0
while len(code):
	curr_pos = position
	opcode = ord(code[0])
	num_params = ord(code[1])
	params = []
	code = code[2:]
	position += 2
	while num_params > 0:
		num_params -= 1
		param_size = ord(code[0])
		param_value = code[1:param_size+1]
		code = code[param_size+1:]
		position += param_size+1
		params += [param_value]

	if opcode == 0:
		if not params[1].isdigit():
			params[1] = "var_" + params[1]
		print str(curr_pos) + ": " + "var_" + params[0] + " = " + params[1]
	elif opcode == 2:
		if not params[1].isdigit():
			params[1] = "var_" + params[1]
		if not params[2].isdigit():
			params[2] = "var_" + params[2]
		print str(curr_pos) + ": " + "var_" + params[0] + "[" + params[1] + "] = " + params[2]
	elif opcode == 8:
		if not params[1].isdigit():
			params[1] = "var_" + params[1]
		if not params[2].isdigit():
			params[2] = "var_" + params[2]
		print str(curr_pos) + ": " + "if( " + params[1] + " >= " + params[2] + " ) goto " + params[0]
	elif opcode == 11:
		print str(curr_pos) + ": " + "goto " + params[0]
	elif opcode==34:
		print str(curr_pos) + ": " + "var_" + params[0] + " = " + params[1] + "[" + params[2] + "]"
	elif opcode==41:
		print str(curr_pos) + ": " + "flag_is_good = eval('" + params[0] + "')('" + params[1] + "')"
	elif opcode==44:
		print str(curr_pos) + ": " + "flag_is_good = eval('" + params[0] + "')()"
	elif opcode==49:
		if not params[1].isdigit():
			params[1] = "var_" + params[1]
		if not params[2].isdigit():
			params[2] = "var_" + params[2]
		print str(curr_pos) + ": " + "if( " + params[1] + " < " + params[2] + " ) goto " + params[0]
	elif opcode==72:
		print str(curr_pos) + ": " + "var_" + params[0] + " = flag_is_good"
	elif opcode==74:
		print str(curr_pos) + ": " + "return var_" + params[0]
	elif opcode==81:
		if not params[1].isdigit():
			params[1] = "var_" + params[1]
		if not params[2].isdigit():
			params[2] = "var_" + params[2]
		print str(curr_pos) + ": " + "if( " + params[1] + " == " + params[2] + " ) goto " + params[0]
	elif opcode==82:
		print str(curr_pos) + ": " + "var_" + params[0] + " = var_" + params[1]
	elif opcode==91:
		if not params[1].isdigit():
			params[1] = "var_" + params[1]
		if not params[2].isdigit():
			params[2] = "var_" + params[2]
		print str(curr_pos) + ": " + "if( " + params[1] + " != " + params[2] + " ) goto " + params[0]
	elif opcode==99:
		print str(curr_pos) + ": " + "var_" + params[0] + " = '" + params[1] + "'[" + params[2] + ":" + params[3] + "]"
	elif opcode==102:
		if not params[1].isdigit():
			params[1] = "var_" + params[1]
		if not params[2].isdigit():
			params[2] = "var_" + params[2]
		print str(curr_pos) + ": " + "var_" + params[0] + " = " + params[1] + " + " + params[2]
	elif opcode==111:
		if not params[1].isdigit():
			params[1] = "var_" + params[1]
		if not params[2].isdigit():
			params[2] = "var_" + params[2]
		print str(curr_pos) + ": " + "var_" + params[0] + " = " + params[1] + " | " + params[2]
	elif opcode==131:
		if not params[1].isdigit():
			params[1] = "var_" + params[1]
		if not params[2].isdigit():
			params[2] = "var_" + params[2]
		print str(curr_pos) + ": " + "var_" + params[0] + " = " + params[1] + " & " + params[2]
	elif opcode==151:
		if not params[1].isdigit():
			params[1] = "var_" + params[1]
		if not params[2].isdigit():
			params[2] = "var_" + params[2]
		print str(curr_pos) + ": " + "var_" + params[0] + " = " + params[1] + " << " + params[2]
	elif opcode==171:
		if not params[1].isdigit():
			params[1] = "var_" + params[1]
		if not params[2].isdigit():
			params[2] = "var_" + params[2]
		print str(curr_pos) + ": " + "var_" + params[0] + " = " + params[1] + " >> " + params[2]
	elif opcode==186:
		if not params[1].isdigit():
			params[1] = "var_" + params[1]
		if not params[2].isdigit():
			params[2] = "var_" + params[2]
		print str(curr_pos) + ": " + "var_" + params[0] + " = " + params[1] + " ^ " + params[2]
	else:
		print str(curr_pos) + ": " + opcode, params
