#include <string.h>
#include <stdio.h>
// Imi pare rau
char *flag;
int flag_length() {
	return strlen(flag);
}
int main(int argc, char *argv[]) {
	flag = argv[1];

unsigned long long flag_text_to_int, low_4_bytes, hi_4_bytes, least_significant_7_bits;

flag_text_to_int = (unsigned long long)flag[0] << 56 | (unsigned long long)flag[1] << 48 | (unsigned long long)flag[2] << 40 | (unsigned long long)flag[3] << 32 | (unsigned long long)flag[4] << 24 | (unsigned long long)flag[5] << 16 | (unsigned long long)flag[6] << 8 | (unsigned long long)flag[7];

for(int i=0; i< 127; i++) {
	low_4_bytes = flag_text_to_int >> (32);
	low_4_bytes = low_4_bytes ^ 4290952684;
	low_4_bytes = low_4_bytes + 4290952684;
	low_4_bytes = low_4_bytes & (0xffffffff);
	hi_4_bytes = flag_text_to_int & (0xffffffff);
	hi_4_bytes = hi_4_bytes ^ 4290952684;
	hi_4_bytes = hi_4_bytes + 4290952684;
	hi_4_bytes = hi_4_bytes & (0xffffffff);
	flag_text_to_int = hi_4_bytes;
	flag_text_to_int = flag_text_to_int << 32;
	flag_text_to_int = flag_text_to_int | low_4_bytes;
	least_significant_7_bits = flag_text_to_int & (127);
	flag_text_to_int = flag_text_to_int >> 7;
	least_significant_7_bits = least_significant_7_bits << 57;
	flag_text_to_int = flag_text_to_int | least_significant_7_bits;
	flag_text_to_int = flag_text_to_int & 0xffffffffffffffff;
}
printf("%llu %llx\n", flag_text_to_int, flag_text_to_int);
return flag_text_to_int == 15164928151071436234ull;
}
