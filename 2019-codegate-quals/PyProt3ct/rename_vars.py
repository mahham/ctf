import re
cod = open("imsorry.cpp", "r").read()

counter = 0

while True:
	varname = re.search(r"var_[O0]+", cod)
	if varname == None:
		break
	varname = varname.group()
	all = re.findall(varname, cod)
	if len(all) == 2:
		print varname, "apare doar de 2 ori"
		cod = re.sub("^.*" + varname + ".*$", "", cod, flags=re.MULTILINE)

	assignments = re.findall(varname + " = ", cod)
	if len(assignments) == 1:
		value = re.search(varname + " = (.*);", cod).group(1)
		cod = re.sub("^.*" + varname + " = .*$", "", cod, flags=re.MULTILINE)	# sterg assignul
		cod = re.sub(varname, "(" + value + ")", cod, flags = re.MULTILINE) # inlocuiesc celelalte aparitii

	cod = cod.replace(varname, "var_" + str(counter))
	counter += 1

open("imsorry.3.cpp", "w").write(cod)