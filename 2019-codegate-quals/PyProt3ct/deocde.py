# Decode custom VM thingy

codef = open("code", "rb")
code = codef.read()
codef.close()

while len(code):
	opcode = ord(code[0])
	num_params = ord(code[1])
	params = []
	code = code[2:]
	while num_params > 0:
		num_params -= 1
		param_size = ord(code[0])
		param_value = code[1:param_size+1]
		code = code[param_size+1:]
		params += param_value
	print opcode, params
