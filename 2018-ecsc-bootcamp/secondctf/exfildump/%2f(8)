<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<!--[if lt IE 7]> <html lang="en" class="lt-ie9 lt-ie8 lt-ie7" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml"> <![endif]-->
<!--[if IE 7]> <html lang="en" class="lt-ie9 lt-ie8 ie7" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml"> <![endif]-->
<!--[if IE 8]> <html lang="en" class="lt-ie9 ie8" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="lt-ie10 ie9" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml"> <![endif]-->
<!--[if gt IE 9]><!--><html lang="en" class="" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml"><!--<![endif]-->
<head id="Head1"><title>
	AirServer - The Most Advanced AirPlay, Miracast and Google Cast receiver for Mac, PC and Xbox One
</title><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta name="msapplication-config" content="none" />
<link rel="shortcut icon" type="image/ico" href="https://az414406.vo.msecnd.net/img2/Airserver_icon_black_x16.png" />
<link rel="icon" type="image/png" href="https://az414406.vo.msecnd.net/img2/Airserver_icon_black_x16.png" />
<link rel="publisher" href="https://plus.google.com/112548559518400979354" title="AirServer on Google+" />
<link rel="canonical" href="https://www.airserver.com/" />
<link rel="stylesheet" href="https://use.typekit.net/akp4xfy.css" /><meta property="og:site_name" content="AirServer" /><meta property="og:url" content="https://www.airserver.com/" /><meta property="og:type" content="website" />
<meta property="og:image" content="https://az414406.vo.msecnd.net/resources2/AirServer-Connect.png" />
<meta property="og:image" content="https://az414406.vo.msecnd.net/resources2/AirServer-SurfaceHub.png" />
<meta property="og:image" content="https://az414406.vo.msecnd.net/resources2/AirServer-XboxOne.png.png" />
<meta property="og:image" content="https://az414406.vo.msecnd.net/resources2/AirServer-PhilipsTV.png" />
<link rel="video_src" href="https://www.youtube.com/embed/s0sDPy1EsYA" /><meta property="og:video" content="https://www.youtube.com/embed/s0sDPy1EsYA" />
<meta property="fb:app_id" content="266019913492730" /><meta name="description" content="AirServer® is the most advanced screen mirroring software receiver for Mac, PC, Xbox One and Surface Hub. AirServer Universal allows you to receive AirPlay, Miracast and Google Cast streams, similar to an Apple TV or a Chromecast device, so you can stream content, cast or mirror your display from an iOS device, macOS, Windows, Android, Chromebook or any other AirPlay and Google Cast compatible device. AirServer is compatible with Windows 10, iOS 9, iOS 10, macOS Sierra and also runs on the Xbox One." />
<meta property="og:title" content="AirServer® - The Most Advanced AirPlay, Miracast and Google Cast receiver for Mac, PC, Xbox One and Surface Hub." />
<meta property="og:description" content="AirServer® is the most advanced screen mirroring software receiver for Mac, PC, Xbox One and Surface Hub. AirServer Universal allows you to receive AirPlay, Miracast and Google Cast streams, similar to an Apple TV or a Chromecast device, so you can stream content, cast or mirror your display from an iOS device, macOS, Windows, Android, Chromebook or any other AirPlay and Google Cast compatible device. AirServer is compatible with Windows 10, iOS 9, iOS 10, macOS Sierra and also runs on the Xbox One." />
<link type="text/css" rel="stylesheet" href="/cdn2/Style.css" />
<link type="text/css" rel="stylesheet" href="https://az414406.vo.msecnd.net/styles2/Site15.css" />

<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <style>
        .segment.what-you-get .bullets-tab .bullet .img {
            margin: 0 0 0 16px;
        }
        .segment.what-you-get .bullets-tab {
            min-height: 280px;
        }
        h3.h3 {
            line-height: 1.3em;
        }
        .segment .bullet a {
            color: #fff;
        }
    </style>
</head>
<body class="sub"><div id="fb-root"></div>
    <header class="main-header">
	    <div class="limit">
		    <a class="logo" href="/" title="AirServer">
                <img id="AirServerLogo" src="https://az414406.vo.msecnd.net/img2/airserver-logo-dark.svg" alt="AirServer Logo" width="227" height="30" />
		    </a>

  			
<style type="text/css">
#magic-line { display: none; }
</style>


<style type="text/css">
.main-nav ul.dropdown {
    width: 370px
}
.main-nav ul.dropdown a {
    font-size: 1.1em;
    text-align: left;
}

.main-nav ul.dropdown a:hover {
    background-color: #eee;
}

.main-nav ul.dropdown a.new {
    background-image: url(https://az414406.vo.msecnd.net/img2/new-orange.svg);
    background-position-x: 334px;
    background-position-y: center;
    background-repeat: no-repeat;
    background-size: 20px 20px;
}
</style>
<nav class="main-nav">
	<ul>
        
        <li class="trigger selected">
			<a href="/">Products</a>
			<ul class="dropdown">
                <a class="new" href="/Connect">AirServer Hardware Solution</a>
				<a href="/PC">AirServer Universal for PC</a>
				<a href="/WindowsDesktop">AirServer Windows 10 Desktop Edition</a>
				<a href="/Mac">AirServer for Mac</a>
				<a href="/Xbox">AirServer Xbox Edition</a>
				<a href="/SurfaceHub">AirServer Surface Hub Edition</a>
				<a href="/PhilipsTV">AirServer Philips TV Edition</a>
				<a href="/Linux">AirServer for Embedded Linux</a>
				<a class="new" href="/IntelUnite">AirServer Intel Unite&#174; Solution</a>
                
				
			</ul>
		</li>

		<li class="trigger">
			<a href="/Usage/Education">Common Usage</a>
			<ul class="dropdown">
				<a href="/Usage/Education">Education</a>
				<a href="/Usage/Entertainment">Home Entertainment</a>
				<a href="/Usage/Business">Business</a>
				<a href="/Usage/Gaming">Gaming</a>
				<a href="/Usage/Developers">Developers</a>
				<a href="/Usage/Recording">Recording</a>
                <a href="/Usage/LiveStream">Live Stream</a>
			</ul>
		</li> 
		<li ><a href="/Compare">Compare</a></li>
		<li ><a href="/History">History</a></li>
		<li ><a href="http://support.airserver.com/">Support</a></li> 
		<li class="dl"><a href="/Download" style="display:none">Download</a></li>


	</ul>
</nav>
<div class="nav-trigger">
	<img class="menu" src="https://az414406.vo.msecnd.net/img2/menu.svg" alt="menu">
	<img class="close" src="https://az414406.vo.msecnd.net/img2/close.svg" alt="menu">
</div>


	    </div>
    </header>

    <div class="main-wrap">
        <main class="main-content">
            
<div class="segment banner" style="padding: 0; margin: 80px 0 0 0; background: none; background-color: #E16467">
	<div class="limit" style="max-width: 1380px; min-height: 100px; background: url(https://az414406.vo.msecnd.net/img2/banner/AirServer_NEW.png) no-repeat; background-size: auto 40%;">
        
        <img style="vertical-align: bottom;" usemap="#learnmoreMap" src="https://az414406.vo.msecnd.net/img2/device/AirServer_Connect_front_page_2.png" />
        <a href="Connect" style="top: 75%; left: 13%; width: 11%; height: 10%; display: block; position: absolute;"></a> 
	</div>
</div>


<a id="platforms" name="platforms"></a>
<div class="segment what-you-get" style="padding-top: 60px; background-color: #172f3d">
	<div class="limit">

        

		
		<div class="segment-header">
			<h1 class="h1">AirServer - Universal Mirroring Receiver</h1>
            <p style="font-size: 1.3em;">AirPlay + Google&nbsp;Cast + Miracast</p>
			<p style="font-size: 1.1em; text-align: justify;">
                With the help of a PC, or any other supported hardware, AirServer can transform a simple big screen or a projector into a universal screen mirroring receiver.
                It does this by implementing all the major screen&nbsp;mirroring technologies such as AirPlay, Google&nbsp;Cast and Miracast into one universal receiver.
                With AirServer enabled on your big screen, users can use their own devices such as an iPhone, iPad, Mac, Android, Nexus, Pixel, Chromebook, or a Windows&nbsp;10&nbsp;PC to wirelessly mirror their display over to the big screen, instantly turning the room into a collaborative space.</p>
            <p style="font-size: 1.1em; text-align: justify;">Use this technology to screen&nbsp;mirror iPads, Windows laptops and Chromebooks in classrooms, wirelessly project your desktop in meeting rooms, or supercharge your Xbox One at home by turning it into a wireless multimedia hub.</p>

		</div>

		<nav class="bullets-tab-nav">
			<ul>
				<li class="selected" data-id="1">PC</li>
				<li data-id="2">MAC</li>
				<li data-id="3">XBOX ONE</li>
                
                <li data-id="4">SURFACE HUB</li>
                <li data-id="5">PHILIPS TV</li>
			</ul>
		</nav>

		<div class="bullets-tabs">
			<div class="bullets-tab selected" data-id="1">
                

                <div class="grid gutter collapse720">
                    <div class="col s2of3">
                        <p style="font-size: 1.1em; text-align: justify;">AirServer Universal turns your Windows&nbsp;PC into a universal mirroring receiver, allowing you to mirror your device's display using the built-in AirPlay, Google&nbsp;Cast or Miracast based screen&nbsp;projection functionality; one-by-one or simultaneously to AirServer (patent pending).</p>
                        <p style="font-size: 1.1em; text-align: justify;">On a PC, users can mirrror or cast their screen from any AirPlay, Google&nbsp;Cast or Miracast compatible device such as an iPhone, iPad, Mac, Windows&nbsp;10, Android or Chromebook. Windows&nbsp;7 and Linux are also supported using the screen casting sender built into the Google&nbsp;Chrome browser.</p>
                    </div>
                    <div class="col s1of3">
                        <img src="https://az414406.vo.msecnd.net/timeline/may2012.png" width="575" style="margin-top: -60px"  />
                    </div>
                </div>

                <div class="grid gutter collapse720" style="margin-top: 30px">
                    <div class="col s1of1"><a class="button white" href="/WindowsDesktop">Learn more</a></div>
                </div>
			</div>

			<div class="bullets-tab" data-id="2">
               

                <div class="grid gutter collapse720">
                    <div class="col s2of3">
                        <p style="font-size: 1.1em; text-align: justify;">AirServer for Mac turns your Mac into a universal mirroring receiver, with the exception of Miracast, allowing you to mirror your device's display using the built-in AirPlay or Google&nbsp;Cast based screen projection functionality; one by one or simultaneously to AirServer.</p>
                        <p style="font-size: 1.1em; text-align: justify;">On a Mac, users can mirrror or cast their screen from any AirPlay or Google&nbsp;Cast compatible device such as an iPhone, iPad, Mac, Android or Chromebook. Windows and Linux platforms are also supported using the screen casting sender built into the Google&nbsp;Chrome browser.</p>
                    </div>
                    <div class="col s1of3">
                        <img src="https://az414406.vo.msecnd.net/timeline/mar2012.png" width="575" style="margin-top: -60px"  />
                    </div>
                </div>

                <div class="grid gutter collapse720" style="margin-top: 30px">
                    <div class="col s1of1"><a class="button white" href="/Mac">Learn more</a></div>
                </div>
			</div>

			<div class="bullets-tab" data-id="3">
				
                
                <div class="grid gutter collapse720">
                    <div class="col s2of3">
                        <p style="font-size: 1.1em; text-align: justify;">AirServer Xbox Edition transforms your Xbox One into an all-in-one AirPlay + Google&nbsp;Cast + Miracast receiver which can be used to beam over your favorite music from Spotify or Apple&nbsp;Music on your iOS device, into your favorite games. Or simply use AirServer to showcase your mobile gameplay on a bigger screen.</p>
                    </div>
                    <div class="col s1of3">
                        <img src="https://az414406.vo.msecnd.net/timeline/XboxOne.png" width="575" style="margin-top: -60px"  />
                    </div>
                </div>

                <div class="grid gutter collapse720" style="margin-top: 30px">
                    <div class="col s1of1"><a class="button white" href="/Xbox">Learn more</a></div>
                </div>
			</div>

			

			<div class="bullets-tab" data-id="4">
                
                <div class="grid gutter collapse720">
                    <div class="col s2of3">
                        <p style="font-size: 1.1em; text-align: justify;">AirServer Surface&nbsp;Hub Edition supercharges your Surface&nbsp;Hub into an all-in-one AirPlay + Google&nbsp;Cast + Miracast receiver with its own guest access point for mirroring so the guests can connect directly to the Surface&nbsp;Hub without needing access to the corporate network.</p>
                        <p style="font-size: 1.1em; text-align: justify;">AirServer is intrinsically optimized for both 55" and 84" Surface&nbsp;Hubs and fully supports its incredible 120Hz display, and as a result delivers a true end-to-end buttery smooth 60 FPS mirroring experience. There is nothing like it!</p>
                    </div>
                    <div class="col s1of3">
                        <img src="https://az414406.vo.msecnd.net/timeline/SurfaceHub.png" width="575" style="margin-top: -60px"  />
                    </div>
                </div>

				<div class="grid gutter collapse720" style="margin-top: 30px">
                    <div class="col s1of1"><a class="button white" href="/SurfaceHub">Learn more</a></div>
                </div>
			</div>

            <div class="bullets-tab" data-id="5">
				

                <div class="grid gutter collapse720">
                    <div class="col s2of3">
               		    <p style="font-size: 1.1em; text-align: justify;">AirServer Philips&nbsp;TV Edition is a universal screen mirroring receiver which is intrinsically optimized for the Philips&nbsp;TV hardware. It supercharges your Philips&nbsp;TV with content sharing capabilities enabling your guests to project their own content and playlists, making them feel at home.</p>
                    </div>
                    <div class="col s1of3">
                        <img src="https://az414406.vo.msecnd.net/timeline/PhilipsTV.png" width="575" height="217" style="margin-top: -60px"  />
                    </div>
                </div>

                <div class="grid gutter collapse720" style="margin-top: 30px">
                    <div class="col s1of1"><a class="button white" href="/PhilipsTV">Learn more</a></div>
                </div>
			</div>

		</div>

	</div>
</div>



<div class="segment-split">
	<div class="grid collapse960">
		<div class="col s1of2">
			<div class="segment recording">
				<div class="half left">
					<div class="limit">
						<h2 class="h3 short">See AirServer in action</h2>
						<p class="h4 thin">Take a tour of AirServer Universal for PC</p>
						<img class="play" src="https://az414406.vo.msecnd.net/img2/play2.png" alt="Play video" style="width: 100px; height: 100px; margin-left: 236px" alt="" data-youtube="TFEFribMjFg">
					</div>
				</div>
			</div>
		</div>
		<div class="col s1of2">
			<div class="segment universal-receiver">
				<div class="half right">
					<div class="limit">
						<h2 class="h3 short"><strong>B</strong>ring <strong>Y</strong>our <strong>O</strong>wn <strong>D</strong>evice</h2>
						<p class="h4 thin" style="font-size: 1em; line-height: 1.4em;">Now also available on Xbox One, Surface Hub and Philips TV!</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<section class="segment featured">
	<div class="limit">

		<header class="segment-header">
			<h3 class="h5">AirServer has been featured on</h3>
		</header>

		<div class="featured">
			<div class="flexslider">
				<ul class="slides">
					<li>
						<div class="item">
							<div class="logo">
								<a href="http://www.wired.com/2011/05/beam-music-movies-photos-from-ipad-to-mac-with-airserver/" target="_blank"><img src="https://az414406.vo.msecnd.net/img2/review-wired.png" alt="Wired" width="220" /></a>
							</div>
							<blockquote class="quote">
								“AirServer is a Mac app that turns your computer into a receiver for AirPlay. We have seen this kind of thing before, but AirServer works better, and adds functionality.”
							</blockquote>
						</div>
					</li>
					<li>
						<div class="item">
							<div class="logo">
							    <a href="http://online.wsj.com/article/PR-CO-20130610-903784.html" target="_blank"><img src="https://az414406.vo.msecnd.net/img2/review-wsj.png" alt="Wall Street Journal" width="440" /></a>
							</div>
							<blockquote class="quote">
								“Faculty and students also utilize AirServer which enables the display and sharing of iPad content in the classroom.”
							</blockquote>
						</div>
					</li>
					<li>
						<div class="item">
							<div class="logo">
								<a href="http://www.macworld.com/article/1159687/airserver_airplay_mac.html" target="_blank"><img src="https://az414406.vo.msecnd.net/img2/review-macworld.png" alt="Macworld" width="220" /></a>
							</div>
							<blockquote class="quote">
								“AirServer turns your Mac into a AirPlay receiver, letting you stream audio, photos, and even videos to your computer, right over the air.”
							</blockquote>
						</div>
					</li>
					<li>
						<div class="item">
							<div class="logo">
								<a href="http://www.macstories.net/reviews/mirroring-multiple-ios-devices-to-a-mac-comparing-airserver-and-reflection/" target="_blank"><img src="https://az414406.vo.msecnd.net/img2/review-mac_stories.png" alt="MacStories" width="220" /></a>
							</div>
							<blockquote class="quote">
								“AirServer is clearly a more complete solution for all kinds of AirPlay streams with dedicated features for audio, video, and Mirroring.”
							</blockquote>
						</div>
					</li>
					<li>
						<div class="item">
							<div class="logo">
								<a href="http://www.tuaw.com/2013/12/23/best-mac-apps-of-2013-talkcast-recap/" target="_blank"><img src="https://az414406.vo.msecnd.net/img2/review-TUAW.png" alt="TUAW" width="220" /></a>
							</div>
							<blockquote class="quote">
								“Great for demoing iOS apps, and a more professional feature set than...”
							</blockquote>
						</div>
					</li>

   					<li>
						<div class="item">
							<div class="logo">
								<a href="http://www.appletvhacks.net/2013/12/20/new-airserver-5-0-for-mac-with-recording-update/#.UrgJwmRdXVs" target="_blank"><img src="https://az414406.vo.msecnd.net/img2/review-appletvhacks.png" alt="AppleTV Hacks" width="220" /></a>
							</div>
							<blockquote class="quote">
								“AirServer for Mac, one of our favourite AirPlay receivers, features long-awaited HD recording with post processing filters.”
							</blockquote>
						</div>
					</li>

                    


					<li>
						<div class="item">
							<div class="logo">
								<a href="http://www.redmondpie.com/how-to-mirror-your-ios-or-android-device-to-your-pc-mac-for-the-ultimate-big-screen-gaming-experience/" target="_blank"><img src="https://az414406.vo.msecnd.net/img2/review-Redmond_pie.png" alt="Redmond Pie" width="440" /></a>
							</div>
							<blockquote class="quote">
								“What could be better than sending Temple Run or Angry Birds in Space to a large screen, competing side by side with a friend?”
							</blockquote>
						</div>
					</li>
                    <li>
						<div class="item">
							<div class="logo">
								<a href="http://www.addictivetips.com/windows-tips/airserver-comprehensive-mac-pc-airplay-suite-with-screen-mirroring/" target="_blank"><img src="https://az414406.vo.msecnd.net/img2/review-addictive_tips.png" alt="Addictive Tips" width="440" /></a>
							</div>
							<blockquote class="quote">
								“AirServer app clearly blows away all the competition.”
							</blockquote>
						</div>
					</li>
                    <li>
						<div class="item">
							<div class="logo">
								<a href="http://www.pcadvisor.co.uk/downloads/3329070/airserver-465/" target="_blank"><img src="https://az414406.vo.msecnd.net/img2/review-pc_advisor.png" alt="PC Advisor" width="220" /></a>
							</div>
							<blockquote class="quote">
								“With AirServer you can take advantage of the better audio and visuals of your Mac by using the app to transform it into a Apple TV type of device that can be used to stream audio and video.”
							</blockquote>
						</div>
					</li>
                    <li>
						<div class="item">
							<div class="logo">
								<a href="http://www.maclife.com/article/reviews/airserver_review" target="_blank"><img src="https://az414406.vo.msecnd.net/img2/review-mac_life.png" alt="Mac Life" width="320" /></a>
							</div>
							<blockquote class="quote">
								“AirServer is so easy to use, we wonder why Apple didn’t implement it themselves.”
							</blockquote>
						</div>
					</li>
                    <li>
						<div class="item">
							<div class="logo">
								<a href="http://www.cultofmac.com/260272/airserver-adds-screen-recording-via-airplay-media-key-support/" target="_blank"><img src="https://az414406.vo.msecnd.net/img2/review-cult_of_mac.png" alt="Cult of Mac" width="220" /></a>
							</div>
							<blockquote class="quote">
								“With AirServer running, you’ll see your Mac show up in your iPad’s AirPlay menu, and you can just select it to play back video, movies or games on the big screen. It even supports mirroring so you can use it with apps that don;t yet support AirPlay properly.”
							</blockquote>
						</div>
					</li>
				</ul>
			</div>
		</div>

	</div>
</section>
        </main>
    </div>
	
    <div class="footer_container">
       
<footer class="main-footer">
		<div class="limit">
			<div class="info">
				<nav class="footer-nav">
					<ul>
					    <li><a href="/Resources">Press Kit</a></li>
					    
					    <li><a href="/ContactUs">Contact Us</a></li>
					    <li><a href="http://www.appdynamic.com/#jobs">Jobs</a></li>
					    <li><a href="/Privacy">Privacy Policy</a></li>
					    <li><a href="/Terms">Terms &amp; Conditions</a></li>
					</ul>
				</nav>
				<p><small class="legal">© 2011-2018 <a href="http://www.appdynamic.com" target="_blank">App Dynamic ehf.</a> All Rights Reserved. AirPlay, Mac and the Mac logo are trademarks of Apple Inc., registered in the U.S. and other countries.<br />
                    AirServer is a trademark of App Dynamic ehf. and is registered in the U.S..
                    Google Cast and the Google Cast badge are trademarks of Google Inc.
				   </small></p>
			</div>
			<div class="follow">
				<a class="social-link facebook" href="https://www.facebook.com/airserverapp"><i class="icon-facebook"></i></a>
				<a class="social-link twitter" href="https://www.twitter.com/airserver"><i class="icon-twitter"></i></a>

		</div>
</footer>


     </div>
	

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">
    function loadScript(src, callback) {
        var r = false;
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = 1;
        s.src = src;
        if (callback) {
            s.onload = s.onreadystatechange = function () {
                //console.log( this.readyState ); //uncomment this line to see which ready states are called.
                if (!r && (!this.readyState || this.readyState == 'complete')) {
                    r = true;
                    callback();
                }
            };
        }
        var t = document.getElementsByTagName('script')[0];
        t.parentNode.insertBefore(s, t);
    }


    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    
    ga('create', 'UA-22205987-1', 'auto');
    ga('send', 'pageview');
    

    loadScript("https://az414406.vo.msecnd.net/scripts2/jquery.events.js", function () {
        loadScript("https://az414406.vo.msecnd.net/scripts2/waypoints.min.js", function () {
            loadScript("https://az414406.vo.msecnd.net/scripts2/waypoints-sticky.min.js", function () {
                loadScript("https://az414406.vo.msecnd.net/scripts2/jquery.fancybox.pack.js", function () {
                    loadScript("https://az414406.vo.msecnd.net/scripts2/jquery.fancybox-media.js", function () {
                        loadScript("https://az414406.vo.msecnd.net/scripts2/jquery.flexslider-min.js", function () {
                            loadScript("https://az414406.vo.msecnd.net/scripts2/table-shatter.js", function () {
                                loadScript("https://az414406.vo.msecnd.net/scripts2/airserver4.js", function () { });
                            });
                        });
                    });
                });
            });
        });
    });
</script>



</body>
</html>