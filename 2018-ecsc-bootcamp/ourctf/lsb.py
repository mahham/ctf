from sys import argv

with open(argv[1], "rb") as f:
	data = f.read()
	bits = ['1' if ord(x)&1 else '0' for x in data]
	bytes = [(int("".join(bits[i:i+8]), 2)) for i in range(0, len(bits), 8)]
	print b"".join([chr(x) for x in bytes])