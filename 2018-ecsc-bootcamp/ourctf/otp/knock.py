from socket import *
import time
import requests
import re

s = socket(AF_INET, SOCK_STREAM)
print "knocking at TCP port 22..."
s.settimeout(1)
try:
	s.connect(("otp.challs.tux.ro", 22))
except:
	pass
print "22 done"
s.close()

s = socket(AF_INET, SOCK_STREAM)
print "knocking at TCP port 80..."
s.settimeout(1)
s.connect(("otp.challs.tux.ro", 80))
time.sleep(1)
s.close()
print "80 done"

s = socket(AF_INET, SOCK_DGRAM)
print "knocking at UDP port 9090..."
s.connect(("otp.challs.tux.ro", 9090))
s.send("")
time.sleep(1)
print "9090 done"
s.close()

print "\nGetting OTP..."
thing = requests.get("https://otp.challs.tux.ro:65443").text.strip()

print thing

print "\nGetting flag..."
flagpage = requests.post("https://ninja.challs.tux.ro/", data={"code": thing}).text

flag = re.search(r"ECSC\{\w+\}", flagpage).group(0)

print flag
