from PIL import Image
from sys import argv
from sys import stdout
import numpy as np

im = Image.open(argv[1])

pixels = []
for pixel in iter(im.getdata()):
    pixels.append(pixel)

data = b""

for c in pixels:
	data += (chr(c[0]))
	data += (chr(c[1]))
	data += (chr(c[2]))

open("solved.png", "wb").write(data)
