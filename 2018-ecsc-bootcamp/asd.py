from pwn import *

e = ELF("a.out")
content = open("a.out", "rb").read()
s = e.symbols["secret_fun"]
a = content[:s]
b = content[s:s+0x43]
c = content[s+0x43:]

bx = xor(b, 0x56)

contentx = a+bx+c

open("a.out.fixed", "wb").write(contentx)
