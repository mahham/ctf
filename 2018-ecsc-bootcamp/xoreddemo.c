#include <stdio.h>
#include <sys/mman.h>
#include <stdint.h>

void secret_fun() {
	putchar('M');
	putchar('y');
	putchar('F');
	putchar('l');
	putchar('a');
	putchar('g');
	return;
}

int main() {
	mprotect((unsigned long long)secret_fun & 0xfffffffffffff000, 0x1000, 7);

	for(char *p = (char*)secret_fun; (intptr_t) p < (intptr_t)&secret_fun + 0x43; p++) {
		*p = *p ^ 0x56;
	}

	secret_fun();

	return 0;
}
