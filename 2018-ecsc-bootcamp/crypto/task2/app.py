from flask import Flask, send_from_directory, redirect, request
import random

app = Flask(__name__)

sessions = {}

@app.route("/gimme_a_session")
def new_session():
	while True:
		session_id = "".join([random.choice("abcdefghijklmnopqrstuvwxyz") for i in range(5)])
		if session_id not in sessions:
			break
	session_flag = "".join([random.choice("0123456789ABCDEF") for i in range(32)])
	sessions[session_id] = {"id": session_id, "tries": 0, "flag": session_flag}
	return "<h1>Session created!</h1><br>{}".format(session_id)

def repr_session(sid):
	s = sessions[sid]
	return """<br>{} | {} tries <a href='/admin/reset?{}'>(reset)</a> | {}""".format(s["id"], s["tries"], s["id"], s["flag"])

@app.route("/admin")
def admin():
	return "<h1>Admin</h1>" + "".join([repr_session(s) for s in sessions])

@app.route("/admin/reset")
def reset():
	sid = request.args.keys()[0]
	print(sid)
	if sid in sessions:
		sessions[sid]["tries"] = 0
	return redirect("/admin")

@app.route("/")
def index():
    return """
<form method="POST" action="/check">Flag: <input name="flag"><br>User: <input name="sid"><br><button>Submit!</button></form><br><a href="/gimme_a_session">/gimme_a_session</a>
"""

@app.route("/check", methods=["POST"])
def check():
	guess = request.form["flag"]
	sid = request.form["sid"]
	if not guess:
		return "No `flag` field :/"
	if not sid:
		return "No `sid` field :/"
	if sid not in sessions:
		return "That session does not exist :/"
	sessions[sid]["tries"] += 1
	if sessions[sid]["tries"] > 256:
		return "You used up all yout 256 tries :/ Ask the admin to reset your counter or create a new session"
	flagnum = int(sessions[sid]["flag"], 16)
	guessnum = int(guess, 16)
	print(bin(flagnum+0x100000000000000000000000000000000))
	print(bin(guessnum+0x100000000000000000000000000000000))
	x = flagnum ^ guessnum
	numbits = 0
	while x:
		numbits += x & 1
		x >>= 1
	return "{} bits are different".format(numbits)

if __name__ == "__main__":
    app.run()
