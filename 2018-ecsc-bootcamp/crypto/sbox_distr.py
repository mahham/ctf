A = 10
B = 11
C = 12
D = 13
E = 14
F = 15

def table_headers(table, top, left):
	if top == None:
		top = [""]*len(table[0])
	if left == None:
		left = [""]
	assert(len(left) == len(table))
	assert(len(top) == len(table[0]))
	top = list(top)
	table_with_headers = [[" "] + top]
	for row in range(len(table)):
		table_with_headers += [[left[row]] + table[row]]
	return table_with_headers

def distr_from_sbox(sbox):
	distr = [[0 for j in range(16)] for i in range(16)]
	for a2 in range(16):
		for a1 in range(16):
			da = a1 ^ a2
			db = sbox[a1] ^ sbox[a2]
			distr[da][db] += 1
	return distr

def parity(n):
	partiy = 0
	while n > 0:
		partiy ^= n & 1
		n >>= 1
	return partiy

def bias_from_sbox(sbox):
	# a - Sbox input
	# b - Sbox output
	# x - input mask
	# y - output mask
	bias = [[0 for j in range(16)] for i in range(16)]
	for x in range(16):
		for y in range(16):
			for a in range(16):
				b = sbox[a]
				bias[x][y] += (parity(a & x) ^ parity(b & y)) == 0
	for x in range(16):
		for y in range(16):
			bias[x][y] -= 8
	return bias

def pretty_print_table(tbl):
	widths = [0] * len(tbl[0])
	for rowarr in tbl:
		for col in range(len(rowarr)):
			if widths[col] < len(str(rowarr[col])):
				widths[col] = len(str(rowarr[col]))
	for rowarr in tbl:
		line = ""
		for col in range(len(rowarr)):
			s = str(rowarr[col])
			sw = len(s)
			pad = widths[col] - sw
			line += " " + " "*(pad) + s
		print(line)
	print("")

bn = [bin(x+16)[-4:] for x in range(16)]

def pretty_print_sbox(sbox):
	pretty_print_table(["0123456789abcdef", [hex(b)[-1] for b in sbox]])
	distr = distr_from_sbox(sbox)
	pretty_print_table(table_headers(distr, bn, bn))
	d = 0
	for row in range(16):
		for col in range(16):
			if distr[row][col] == 10:
				d += 100
			if distr[row][col] == 8:
				d += 10
			if distr[row][col] == 6:
				d += 1
	print("D = {}\n\n".format(d))
	print("Bias:")
	pretty_print_table(table_headers(bias_from_sbox(sbox), bn, bn))



print("(i)")
pretty_print_sbox([2, C, 4, 1, 7, A, B, 6, 8, 5, 3, F, D, 0, E, 9])
print("(ii)")
pretty_print_sbox([E, B, 2, C, 4, 7, D, 1, 5, 0, F, A, 3, 9, 8, 6])
print("(iii)")
pretty_print_sbox([4, 2, 1, B, A, D, 7, 8, F, 9, C, 5, 6, 3, 0, E])
print("(iv)")
pretty_print_sbox([B, 8, C, 7, 1, E, 2, D, 6, F, 0, 9, A, 4, 5, 3])


print("(v)")
pretty_print_sbox([F, C, 8, 2, 4, 9, 1, 7, 5, B, 3, E, A, 0, 6, D])